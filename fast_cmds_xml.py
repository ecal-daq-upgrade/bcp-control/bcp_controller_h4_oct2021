import xml.etree.ElementTree as ET

class command: 

    def __init__(self, name, code, encoded_code, description): 
        self.name = name
        self.code = code 
        self.encoded_code = encoded_code
        self.description = description


    def __repr__(self): 
        return self.name


def generateCommands(xmlCmdFile='fast_cmds.xml'):
    cmds = {}
    try:
        tree = ET.parse(xmlCmdFile)
        root = tree.getroot() 

        if root.tag != 'fast_commands': 
            print('bad xml file.')
            return cmds

        for cmd in root: 
            if cmd.tag != 'Command':
                print(f'bad tag {cmd.tag}')
                continue 
            name = cmd.find('name')
            code = cmd.find('code')
            encoded_code = cmd.find('encoded_code')
            description = cmd.find('description')
            shortname = cmd.find('shortname')
            if shortname != None:
                cmds[shortname.text] = command(name.text, code.text,
                                encoded_code.text, description.text)
                

    except Exception as e:
        print(e)
    return cmds 


if __name__ == '__main__': 

    cmds = generateCommands()
    print(cmds)
    for k,cmd in cmds.items():
        print(f'{cmd.name}, {cmd.code}, {cmd.encoded_code}')
        print(cmd.description)
