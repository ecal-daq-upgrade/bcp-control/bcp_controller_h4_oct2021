import time
import lpgbt_functions as lpgbt_functions

def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)


    

    try:
        myfile = open("pll_scan.txt", "w")

        print("Reset tclinks")
        tclink_status = lpgbt_functions.reset_tclinks(myBcp)
        if not tclink_status:
            raise Exception('error tclink status')
        time.sleep(1)

        print("Send reset fast command to VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)
        time.sleep(2)

        print("Configure all DTUs")
        lpgbt_functions.configureAllDTUs(myBcp, '2,4')
        time.sleep(1)

        print("Set all DTUs to PLL manual")
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, '2,4', enable=True)

        time.sleep(0.1)

        lpgbt_functions.enableTestMode(myBcp, True)

        pll_conf1_list = [0x31, 0x63] #[0x0, 0x1, 0x3, 0x7, 0x15, 0x31, 0x63]
        pll_conf2_list = range(6,8)      
        #pll_conf1_list = [0x0, 0x1, 0x3, 0x7, 0x15, 0x31, 0x63]
        #pll_conf2_list = range(0,8)
   
        for index1, pll_conf1 in enumerate(pll_conf1_list):
            for index2, pll_conf2 in enumerate(pll_conf2_list):

                pll_conf = (pll_conf1<<3)|pll_conf2

                myfile.write(f"{pll_conf}: ")
                #print(f'pll_conf1 {pll_conf1}, pll_conf2 {pll_conf2}, pll_conf {pll_conf}')
                lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, '2,4', pll_conf=pll_conf)

                print("Zeroing bit shifts")
                lpgbt_functions.remove_all_shifts(myBcp)
        
                time.sleep(0.2)
                print("Acquire data")
                lpgbt_functions.acquireData(myBcp)
        
                print("Acquire data. Why do we need to acquire data a second time?")
                lpgbt_functions.acquireData(myBcp)
        
                time.sleep(0.2) # Don't go below 0.01 seconds, 0.1 it even safer
                print("Align channels")
                lpgbt_functions.align_channels(myBcp)
                #input("Press any key")
                myfile.write(', '.join(str(x) for x in myBcp.swRx.shiftList))
                myfile.write("\n")
                myfile.flush()
 
                print("Acquire data")
                lpgbt_functions.acquireData(myBcp)
        
                print("Acquire data")
                lpgbt_functions.acquireData(myBcp)
                 
        print("Disable test mode")
        lpgbt_functions.enableTestMode(myBcp, False)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
