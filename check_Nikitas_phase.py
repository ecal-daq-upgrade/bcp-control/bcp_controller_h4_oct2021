import time
import lpgbt_functions as lpgbt_functions

def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    try:
        print("Reset tclinks")
        tclink_status = lpgbt_functions.reset_tclinks(myBcp)
        if not tclink_status:
            raise Exception('error tclink status')
        time.sleep(1)
  
#        lpgbt_functions.fast_reset_VFE(myBcp)
        
#        lpgbt_functions.configureAllDTUs(myBcp, '2,4')
#        time.sleep(1)

#        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, '2,4', enable=True)
#        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, '2,4', pll_conf=399)

        lpgbt_functions.setManualPhaseRXChannels(myBcp,0)
        time.sleep(1)       
 
        print("Zeroing bit shifts")
        lpgbt_functions.remove_all_shifts(myBcp)

        print("Enable sync mode")
        lpgbt_functions.enableSyncMode(myBcp, True)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data. Why do we need to acquire data a second time?")
        lpgbt_functions.acquireData(myBcp)

        time.sleep(0.2) # Don't go below 0.01 seconds, 0.1 it even safer
        print("Align channels")
        lpgbt_functions.align_channels(myBcp)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        time.sleep(1)

        print("Disable sync mode")
        lpgbt_functions.enableSyncMode(myBcp, False)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
