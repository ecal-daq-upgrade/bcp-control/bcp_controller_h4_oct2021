import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main():
    
    myBcp = lpgbt_functions.openBCPConnection()
   
    full_ch_list = {} 

    for ch_num in range(0,25):
      full_ch_list[ch_num] = lpgbt_functions.check_ch_errors(myBcp, ch_num=ch_num)
      
    # get reg names from data itself
    reg_name_list = full_ch_list[0].keys()


    for vfe_num in range(0,5):
      print("VFE"+str(vfe_num+1))
      for reg_name in reg_name_list:
        string_line = reg_name.ljust(30)
        for ch_num in range(0,5):
          ch_num_tot = vfe_num*5 + ch_num
          string_line += "{0:#0{1}x}".format(full_ch_list[ch_num_tot][reg_name], 10) + "\t"
        print(string_line)
      print("")

 
 
    myBcp.stop()


if __name__ == "__main__":
    
    main()
