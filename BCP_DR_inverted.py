import threading
import sys
import argparse
import time
from bs4 import BeautifulSoup as Soup
from zmq import *
from zmq import SUB, PUB, SUBSCRIBE, POLLIN
import socket
import traceback



# Add the bcp fw folders
top_level = '/home/gcucciat/bcp_controller_h4_oct2021/bcp-readout/'
#pr.addLibraryPath(top_level+'firmware/submodules/surf/python')
#pr.addLibraryPath(top_level+'firmware/')
sys.path.append(top_level+'firmware/surf/python')
sys.path.append(top_level+'firmware/')
#sys.path.append(top_level+'./firmware/surf/python/surf/devices')

import pyfirmware as fw

class TemplateApp:
  def action_decorator(myfunc):
        def magic( self ) :
            print("{} action begin".format(myfunc.__name__))
            answer = myfunc(self)
            if answer:
              print(f"{self.commandQueue}")
              print(f"Popping {self.commandQueue[0]}")
              self.commandQueue.pop(0)
            print("{} action end".format(myfunc.__name__))
        return magic

  def status_decorator(myfunc):
        def magic( self ) :
            print("{} status begin".format(myfunc.__name__))
            myfunc(self)
            self.check_command_queue()
            print("{} status end".format(myfunc.__name__))
        return magic

  def __init__(self,argv):
    # For the moment we don't include a FSM, even if it is raccomended to 
    # develop one for python custom apps
    #self.final_state_machine = Fsm()
    
    self.rsdictReverse = { #imported from H4DAQ/interface/Command.hpp
      'START': 0,
      'INIT': 1,
      'INITIALIZED': 2,
      'BEGINSPILL': 3,
      'CLEARED': 4,
      'WAITFORREADY': 5,
      'CLEARBUSY': 6,
      'WAITTRIG': 7,
      'READ': 8,
      'ENDSPILL': 9,
      'RECVBUFFER': 10,
      'SENTBUFFER': 11,
      'SPILLCOMPLETED': 12,
      'BYE': 13,
      'ERROR': 14,
      'STEP':15
    }

    self.CMD_t = {
      'NOP': 0,
      'WWE': 1,
      'WE': 2,
      'EE': 3,
      'WBE': 4,
      'BT': 5,
      'WBT': 6,
      'EBT': 7,
      'STARTRUN': 8,
      'SEND': 9,
      'RECV': 10,
      'DATA': 11,
      'STATUS': 12,
      'SPILLCOMPL': 13,
      'ENDRUN': 14,
      'DIE': 15,
      'ERRORCMD': 16,
      'RECONFIG': 17,
      # GUI Commands 100 - 199: Parsed only by the RunControl
      'GUI_STARTRUN': 100, # run# type#(PEDESTAL,LED,PHYSICS) #trig/spill (int)
      'GUI_RESTARTRUN': 101,
      'GUI_STOPRUN': 102,
      'GUI_PAUSERUN': 103,
      'GUI_DIE': 104,
      'GUI_RECONFIG': 105,
       # EB Commands
      'EB_SPILLCOMPLETED': 200,
      'DR_READY': 201, # DR are ready to take data
      'GUI_GDG': 202,
      'GUI_SELFTRIGGER': 203
    }

    self.status = 'INIT'
    self.commandQueue = []

    self.data_port = 0
    self.status_port = 0
    self.cmd_port = 0

    self.sub = None # Receives commands
    self.pub = None # Send status 
    self.pub_cmd = None # Send cmd
    self.context = None
    self.poller = None
    self.sub_urls = []

    self.loopSleepTime = 1

    # Parameters from the configuration file and used by the BCP configuration
    self.bcpip = "192.168.40.192"
    self.selfTriggerEnabled = 0
    self.debugPrint = 0
    self.numReadoutWords = 0
    self.readoutShiftSel = 0
    self.towerDataSel = 0
    self.lastNumTriggersReadout = 0


    # Thread for periodic actions
    self.socketThread = threading.Thread(target=self.socket_thread_function, args=())
    self.status_loop_running = False
    self.socket_loop_running = False
    # Parsing arguments coming from the AppControl
    # We expect -c and the configuration file path
    # and -l with the log file path
    # An error occurs if they are missing 
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--configurefile", type=str, help="Configuration file", required=True)
    parser.add_argument("-l", "--logfile", type=str, help="Log file", required=True)
    args = parser.parse_args()
    self.configure_file_path = args.configurefile
    self.log_file_path = args.logfile

    # Redirecting output to log file
    sys.stdout = open(self.log_file_path, 'w')

    # addin our bcp fw device tree
    #self.bcpip = '192.168.40.192'
    #self.bcp = fw.Root(ip=self.bcpip, serverPort=None)
    self.bcpThread = threading.Thread(target=self.bcp_thread, args=())
    
  def bcp_thread(self):
    print('Starting Rogue/BCP Thread')
    self.bcp.start()

  # Function used by the thread
  def socket_thread_function(self):
    print("Entering in socket loop")
    while self.socket_loop_running:
      try:
        # Limiting the loop rate with 1 sec sleep
        time.sleep(self.loopSleepTime)
        # Sending state
        status_msg = "STATUS statuscode="+str(self.rsdictReverse[self.status])
        print("Sending status msg: ",status_msg)
        self.pub.send_string(status_msg)

        # Polling for messages
        socks = dict(self.poller.poll(1))
        if socks.get(self.sub):
          message = self.sub.recv()
          self.proc_message(message.decode("utf-8"))

        # It seems that python is not flushing "print" each time it is called.
        # Forcing to do it once per cycle.

        sys.stdout.flush()
      except Exception as inst:
        print(inst)
        print("Error in the socket loop. Breaking...")
        self.status == 'ERROR'
        sys.stdout.flush()
        self.close_loops()
    print('End of socket loop')


  def proc_message(self, msg):
    print('Message from subscription "{}" during status {}'.format(msg, self.status))
#    if msg.startswith('RECONFIG'):
#      self.reconfig()
#    if msg.startswith('STARTRUN'):
#      self.startrun()
#    if msg.startswith('DIE'):
#      self.die()
#    if msg.startswith('WWE'):
#      self.wwe()
#    if msg.startswith('WE'):
#      self.we()
#    if msg.startswith('EE'):
#      self.ee()
#    if msg.startswith('ENDRUN'):
#      self.endrun()
#    if msg.startswith('SPILLCOMPL'):
#      self.spillcompl()
    if msg.startswith('RECONFIG'):
      self.commandQueue.append('RECONFIG')
    if msg.startswith('STARTRUN'):
      self.commandQueue.append('STARTRUN')
    if msg.startswith('DIE'):
      self.commandQueue.append('DIE')
    if msg.startswith('WWE'):
      self.commandQueue.append('WWE')
    if msg.startswith('WE'):
      self.commandQueue.append('WE')
    if msg.startswith('EE'):
      self.commandQueue.append('EE')
    if msg.startswith('ENDRUN'):
      self.commandQueue.append('ENDRUN')
    if msg.startswith('SPILLCOMPL'):
      self.commandQueue.append('SPILLCOMPL')

  def check_command_queue(self):
    print(f"Command queue {self.commandQueue}")
    if len(self.commandQueue)==0:
      return
    if self.commandQueue[0] == 'RECONFIG':
      self.reconfig()
    elif self.commandQueue[0] == 'STARTRUN':
      self.startrun()
    elif self.commandQueue[0] == 'DIE':
      self.die()
    elif self.commandQueue[0] == 'WWE':
      self.wwe()
    elif self.commandQueue[0] == 'WE':
      self.we()
    elif self.commandQueue[0] == 'EE':
      self.ee()
    elif self.commandQueue[0] == 'ENDRUN':
      self.endrun()
    elif self.commandQueue[0] == 'SPILLCOMPL':
      self.spillcompl()

  def parse_standard_params(self, soup):
    # We parse the config file to extracts port numbers.
    print("Looking for ports...")
    port_list = []
    for tag in soup.find_all("ListenPort"):
      port_list.append(tag.string)
    self.data_port = port_list[0]
    self.status_port = port_list[1]
    self.cmd_port = port_list[2]
    print("Data port: ",self.data_port," - Status port: ",self.status_port," - Cmd port: ",self.cmd_port)

    # Url list for ZMQ to subscribe to:
    for tag in soup.find_all("ConnectTo"):
      self.sub_urls.append(tag.string)


  # In this function you can extract other parameters from the config file
  def parse_custom_params(self, soup):
    print("Parsing custom params...")
    for tag in soup.find_all("IPAddress"):
      self.bcpip = tag.string
    for tag in soup.find_all("selfTriggerEnabled"):
      self.selfTriggerEnabled = int(tag.string)
    for tag in soup.find_all("debugPrint"):
      self.debugPrint = int(tag.string)
    for tag in soup.find_all("numReadoutWords"):
      self.numReadoutWords = int(tag.string)
    for tag in soup.find_all("readoutShiftSel"):
      self.readoutShiftSel = int(tag.string)
    for tag in soup.find_all("towerDataSel"):
      self.towerDataSel = int(tag.string)

  def parse_configuration_file(self):
    try:
      print("Configure file path sent by AppControl: ",self.configure_file_path)
      with open(self.configure_file_path) as fp:
        soup = Soup(fp, "xml")
        self.parse_standard_params(soup)
        self.parse_custom_params(soup)
      
    except:
      raise Exception("parse_configure_file failed")


  # initialize_zmq_channels comes after parse_configuration_file
  def initialize_zmq_channels(self): 
    print('Start initialize_zmq_channels')
    self.context = Context()
    self.poller = Poller()
    self.sub = self.context.socket(SUB)
    # If the application run in local... do we need to rename the url
    # with localhost instead of machine name? Let's do it...

    # First: subscription to the sockets where the application receives commands
    gui_hostname = socket.gethostname()
    for url in self.sub_urls:
      new_url = "tcp://"
      if gui_hostname in url:
        url = url.replace(gui_hostname,"localhost")
      new_url += url
      self.sub.connect("tcp://"+url)
    self.sub.setsockopt(SUBSCRIBE,b'')
    self.poller.register(self.sub,POLLIN)

    # Second: generate socket to send status
    self.pub = self.context.socket(PUB)
    new_url = "tcp://*:"
    port_index = self.status_port.find(":")
    new_url = new_url + self.status_port[port_index+1:]
    print("Url to publish app status: ", new_url)
    self.pub.bind(new_url)

    # Third: a DR application sends cmd to the RC like DR_READY
    # We need to create also this PUB socket
    self.pub_cmd = self.context.socket(PUB)
    new_url = "tcp://*:"
    port_index = self.cmd_port.find(":")
    new_url = new_url + self.cmd_port[port_index+1:]
    print("Url to publish app cmd: ", new_url)
    self.pub_cmd.bind(new_url)

    self.poller.register(self.pub_cmd,POLLIN)
    self.poller.register(self.pub,POLLIN)

    


  def status_loop(self):
    print("Entering in status loop")
    while self.status_loop_running:
      try:
        print(self.status)
        time.sleep(self.loopSleepTime)
        if self.status == 'START':
          print('Do something when START...')
        if self.status == 'INIT':
          print('Do something when INIT...')
          self.init()
        if self.status == 'INITIALIZED':
          print('Do something when INITIALIZED...')
          self.initialize()
        if self.status == 'BEGINSPILL':
          print('Do something when BEGINSPILL...')
          self.beginspill()
        if self.status == 'CLEARED':
          print('Do something when CLEARED...')
          self.cleared()
        if self.status == 'WAITTRIG':
          print('Do something when WAITTRIG...')
          self.waittrig()
        if self.status == 'READ':
          print('Do something when READ...')
          self.read()
        if self.status == 'CLEARBUSY':
          print('Do something when CLEARBUSY...')
          self.clearbusy()
        if self.status == 'ENDSPILL':
          print('Do something when ENDSPILL...')
          self.endspill()
        if self.status == 'SENTBUFFER':
          print('Do something when SENTBUFFER...')
          self.sentbuffer()
        if self.status == 'BYE':
          self.bye()
        if self.status == 'ERROR':
          self.error()
      except Exception as inst:
        print(traceback.format_exc())
        print("Error somewhere in the status loop. Breaking...")
        self.status == 'ERROR'
        sys.stdout.flush()
        break
    print('End of status loop')

  def close_loops(self):
    print("Closing threads")
    self.socket_loop_running = False            # Closing zmq thread
    self.socketThread.join()
    self.bcp.stop()                            # Closing bcp thread
    self.bcpThread.join()
    self.status_loop_running = False            # Closing state thread

  

  def bcpConfigureAndStart(self):

    # Order to have the self.bcp configured and ready:
    # 1. Extract useful values from config file
    # 2. Define self.bcp
    # 3. Start BCP thread
    # 4. Set the registers

    self.bcp = fw.Root(ip=self.bcpip, serverPort=None)
    self.bcpThread.start()
    print("Configuring the BCP for the acquisition")
    print("In this function we take parameters from the xml config file and we write them in the bcp registers")
    print('Test connection with the board reading Core.AxiVersion.FpgaVersion register: {}'.format(hex(self.bcp.Core.AxiVersion.FpgaVersion.get())))

    print("Setting DebugPrint to {}".format(self.debugPrint))
    self.bcp.SwRx.DebugPrint.set(self.debugPrint)

    print("setTimeStampNow command launched")
    self.bcp.BCP.BCP_Readout.setTimeStampNow()

    print("Setting NumReadoutWords to {}".format(self.numReadoutWords))
    self.bcp.BCP.BCP_Readout.NumReadoutWords.set(self.numReadoutWords)

    print("Setting ReadoutShiftSel to {}".format(self.readoutShiftSel))
    self.bcp.BCP.BCP_Readout.ReadoutShiftSel.set(self.readoutShiftSel)

    print("Setting TowerDataSel to {}".format(self.towerDataSel))
    self.bcp.BCP.BCP_Readout.TowerDataSel.set(self.towerDataSel)


#################################################
#         Other functions
#################################################    


          #####################
#############   Actions   ##############
          #####################

############### Status actions #####

  @status_decorator
  def init(self):
    self.parse_configuration_file()
    self.initialize_zmq_channels() # you can do that only after parse_configuration_file
    self.socket_loop_running = True
    self.socketThread.start() # you can do that only after initialize_zmq_channels
    #self.bcpThread.start()
    self.bcpConfigureAndStart()
    self.status='INITIALIZED'
 
  @status_decorator
  def initialize(self):
    print('Test connection with the board reading Core.AxiVersion.FpgaVersion register: {}'.format(hex(self.bcp.Core.AxiVersion.FpgaVersion.get())))

  @status_decorator
  def beginspill(self):
    return
  
  @status_decorator
  def cleared(self):
    return

  @status_decorator
  def sentbuffer(self):
    return

  @status_decorator
  def bye(self):
    self.close_loops()
  
  @status_decorator
  def error(self):
    self.close_loops()

  @status_decorator
  def waittrig(self):
    if self.selfTriggerEnabled:
      print("Generating a self trigger signal for testing. Normally we should have a trigger from external source")
      print("SWTrigger command launched")
      self.bcp.BCP.BCP_TCDS.SWTrigger()
      self.status="READ"
    else:
      #newNumTriggersReadout = self.bcp.BCP.BCP_Readout.NumTriggersReadout.get()
      #if newNumTriggersReadout > self.lastNumTriggersReadout:
      #  print("BCP has a new trigger!")
      #  self.lastNumTriggersReadout = newNumTriggersReadout
      #  self.status="READ"
      #readoutbusy = self.bcp.BCP.BCP_Readout.Readout_Busy.get()
      #if readoutbusy:
      #  self.status="READ"
      readoutready = self.bcp.BCP.BCP_Readout.Readout_DataReady.get()
      if readoutready:
        self.status="READ"


  @status_decorator
  def read(self):
    value = 1
    print("Setting SendFrame to {}".format(value))
    cnt = self.bcp.SwRx.FrameCount.get()
    self.bcp.App.AppTx.SendFrame.set(value)
    while True:
      newcnt = self.bcp.SwRx.FrameCount.get()
      print(cnt,newcnt)
      if (cnt != newcnt):
        print(self.bcp.SwRx.lastFrame)
        break
    self.status='CLEARBUSY'
  
  @status_decorator
  def clearbusy(self):
    print("ClrBusy command launched")
    self.bcp.BCP.BCP_Readout.ClrBusy()
    self.status='WAITTRIG'

  @status_decorator
  def endspill(self):
    self.status='SENTBUFFER'

############### Command actions #####

  @action_decorator
  def startrun(self):
    if self.status == 'INITIALIZED':
      self.status = 'BEGINSPILL'
      return True
    return False      

  @action_decorator
  def endrun(self):
    good_status = ['BEGINSPILL', 'CLEARED', 'SENTBUFFER']
    if self.status in good_status:
      self.status = 'INITIALIZED'
      return True
    return False

  @action_decorator
  def wwe(self):
    if self.status == 'BEGINSPILL':
      print("ClrBusy command launched")
      self.bcp.BCP.BCP_Readout.ClrBusy()
      self.status = 'CLEARED'
      return True
    return False

  @action_decorator
  def we(self):
    if self.status == 'CLEARED':
      self.pub_cmd.send_string("DR_READY\0") 
      self.status = 'WAITTRIG'
      return True
    return False
  
  @action_decorator
  def ee(self):
    if self.status == 'WAITTRIG':
      self.status = 'ENDSPILL'
      return True
    return False

  @action_decorator
  def spillcompl(self):
    if self.status == 'SENTBUFFER':
      self.status = 'BEGINSPILL'
      return True
    return False

  @action_decorator
  def reconfig(self):
    if self.status == 'INITIALIZED':
      self.status = 'INITIALIZED'
      return True
    return False
  
  @action_decorator
  def die(self):
    good_status = ['INITIALIZED', 'BEGINSPILL', 'CLEARED', 'SENTBUFFER']
    if self.status in good_status:
      self.status = 'BYE'
      return True
    return False

### Main ###

def main(argv):
  print(argv)
  newApp = TemplateApp(argv)

  newApp.status_loop_running = True
  newApp.status_loop()

  print('Status loop terminated. Bye bye!')

if __name__ == '__main__':
  main(sys.argv)
