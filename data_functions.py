import sys
import json
import os

# Add the bcp fw folders
configuration = {}
directory = os.path.dirname(os.path.abspath(__file__))
with open(directory+"/TT_config.json") as conf_file:
  configuration = json.load(conf_file)
  conf_file.close()
top_level = directory + "/" + configuration["bcp-rudp-readout"]
print(top_level)
sys.path.append(top_level+'firmware/surf/python')
sys.path.append(top_level+'firmware/')

import pyfirmware as fw
import pyrogue as pr
import pyrogue.pydm
import numpy as np
import pandas as pd
import struct
from pyfirmware._CRC16_8005_D64b_crcgen import *

def hexMSBFirst(vals):
    "Reverse bytes so that the MSB is printed first"
    rev = bytearray(len(vals))
    #for cnt, val in vals:
    #    rev[-1*(cnt+1)] = val
    for idx in range(len(vals)):
        rev[-1*(idx+1)] = vals[idx]
    return rev.hex()




class FrameStruct(object):
    def __init__(self):
        #############################
        # Header
        #############################
        #@@@self.header = None
        #############################
        # Payload
        #############################
        #@@@self.wrdData = None

        self.preamble = None
        self.towerDataFull = None
        self.towerData = [None for i in range(4)]
        self.timeStampLHC = None
        self.trailer = None

class fakeFrame:
    def __init__(self, header, data):
        self.header = header
        self.data = data

    def getPayload(self):
        return len(self.data)

    def read(self, data, offset):
        for i in range(0, len(data)):
            data[i] = self.data[i+offset]
        return data

class Receiver(pr.DataReceiver):
    def __init__( self,**kwargs):
        super().__init__(**kwargs)

        self.frameCnt = 0
        self.lastCRCFrame = 0
        self.numSubFramesGood = 0
        self.numSubFramesBad = 0
        self.TTeventList = []
        self.unexpectedHeader = 0

    def parseTimeStamp(self, val):
        seconds = int(val / 40078900)
        return str(datetime.timedelta(seconds=seconds))

    def process(self, frame, raw_data=False):
        #print(f'\nFrame #{self.frameCnt}')
        eventFrames, badSubFrames = self.ParseFrame(frame)

        if (badSubFrames != 0):
            # Be sure to increment Bad subframe count if have
            # trouble parsing subframes altough may not be an
            # accurate count of errors.
            self.numSubFramesBad += badSubFrames
      
        for idx, eventFrame in enumerate(eventFrames):
            # we skip the last frame because we cannot get its next
            if idx+1 == len(eventFrames):
              continue
            frametime = eventFrame.timeStampLHC

            nextEventFrameVFEdata = eventFrames[idx+1].VFEdata

            for cnt, vfe in enumerate(eventFrame.VFEdata):
                count = 40
                revVfe = hexMSBFirst(vfe)
                revVfeNext = hexMSBFirst(nextEventFrameVFEdata[cnt])

                for i in range (0, 5):
                    #if cnt == 3 and i == 3:
                    # self.frameCnt += 1
                    #ch_num = cnt*5 + i
                    word_as_str_first = revVfe[count-8:count]
                    word_as_str_next = revVfeNext[count-8:count] 
                    word_as_int_first = int(word_as_str_first,16)
                    word_as_int_next = int(word_as_str_next,16)
                    word_as_bin_first = format(word_as_int_first,"032b")
                    word_as_bin_next = format(word_as_int_next,"032b")

                    bit_shift = 32
                    word_as_int = word_as_bin_first[bit_shift:] + word_as_bin_next[0:bit_shift]
                    word_as_int = int(word_as_int,2)

                    #word_as_str = revVfe[count-8:count]
                    #word_as_int = int(word_as_str,16)

                    if raw_data == True:
                        new_point = {'value':word_as_int, 'vfe':cnt, 'ch':i}
                        self.TTeventList.append(new_point)
                        continue

                    header_short = word_as_int >> 30
                    header_long = word_as_int >> 26
                    header_lim = word_as_int >> 28


                    if header_short == 1 or header_short == 2:
                        samples_number = 5
                        if header_short == 2:
                            samples_number = (word_as_int >> 24) & 0x3F
                            if samples_number > 4:
                                #print(f"VFE {cnt+1}, Ch {i+1}, num of samples = {samples_number} > 4 in baseline dataformat 1. Error!")
                                samples_number = 4
                                continue
                        for sample_index in range(samples_number):
                            value = word_as_int & 0x3F
                            new_point = {'value':value, 'time':frametime, 'vfe':cnt, 'ch':i, 'type': header_short, 'hg':1}
                            #if sample_index%2 == 1:
                            self.TTeventList.append(new_point)
                            word_as_int = word_as_int >> 6
                        
                    elif header_long == 10 or header_long == 11: # as 001010 or 001011
                        value1 = word_as_int & 0x1FFF
                        value2 = (word_as_int >> 13) & 0x1FFF
                        if header_long == 11 and (value2 != 0b1111000001111 or value2 != 0b0101010101010):
                            continue
                        bit1 = value1 & 0x1000
                        bit2 = value2 & 0x1000
                        value1 = value1 & 0xFFF
                        value2 = value2 & 0xFFF
                        
                        new_point_1 = {
                            #'value':value1 if bit1 else int(value1/10), 
                            'value':value1,
                            'hg':0 if bit1 else 1, 
                            'time':1, #timeframe 
                            'vfe':cnt, 
                            'ch':i, 
                            'type': header_long}
                        self.TTeventList.append(new_point_1)
                        if header_long == 10:
                            new_point_2 = {
                                #'value':value2 if bit2 else int(value2/10),
                                'value':value2,
                                'hg':0 if bit2 else 1, 
                                'time':0, #timeframe
                                'vfe':cnt, 
                                'ch':i,
                                'type': header_long}
                            self.TTeventList.append(new_point_2)
                    #Looking for frame delimiter packet
                    elif header_lim == 13 and cnt == 1 and i == 3:
                      #pass
                      print("frame Num", word_as_int & 0xFF)
                      print("Num of samples", (word_as_int>>20) & 0xFF) 

                    elif header_lim == 3:
                        look_9 = (word_as_int >> 12) & 0xF
                        if look_9 == 9:
                            value1 = word_as_int & 0xFFF
                            value2 = (word_as_int >> 16) & 0xFFF

                            new_point_1 = {
                            #'value':value1 if bit1 else int(value1/10), 
                             'value':value1,
                             'hg': 1,
                             'time':frametime,
                             'vfe':cnt,
                             'ch':i,
                             'type': header_lim}
                            self.TTeventList.append(new_point_1)

                            new_point_2 = {
                            #'value':value2 if bit1 else int(value1/10), 
                                'value':value2,
                                'hg': 1,
                                'time':frametime,
                                'vfe':cnt,
                                'ch':i,
                                'type': header_lim}
                            self.TTeventList.append(new_point_2)

                    else:
                        self.unexpectedHeader += 1

                    count -= 8

   # Parse the received Frame
    def ParseFrame(self, frame):
        # Count number of bad subframes
        bad = 0

        # Next we can get the size of the frame payload
        size = frame.getPayload()

        # Calculate the number of 64-bit words
        num64bWords = (size>>3)

        # There should be 16 64-bit words per subframe so process data 128
        # bytes (16*8) at a time until it is depleted.
        subframeSize = 16*8
        numSubFrames = size // subframeSize


        # Create the event holders
        eventFrames = [FrameStruct() for frm in range(numSubFrames)]

        ## Divide data into subFrames
        for idx, subframe in enumerate(range(0, size, subframeSize)):

            availableData = size - (subframeSize*idx)

            if (availableData < subframeSize):
                # Not a full frame in this pass so read data and print it
                data = bytearray(availableData)
                frame.read(data,subframe)

                bad += 1
                print(f'ERROR: Incomplete subframe! Expected {16*8} bytes but only got {len(data)}:')
                for cnt, word64 in enumerate(range(0,len(data),8)):
                    print(f'   word #{cnt:02d}  = 0x{hexMSBFirst(data[word64+0:word64+8])}') #@@@  ({self.crcmod}/0x{hexMSBFirst(self.crccalc)})')
                    (self.crcmod, self.crccalc) = crcgen(data[word64+0:word64+8],self.crcmod,self.crccalc)

                    if (self.crcmod == 0):
                        # self.crcmod will be 0 again when enough data has passed
                        # through crcgen to add up to a full subFrame. Use
                        # this to check CRC mainly for testing purposes. The
                        # bad count has been incremented already due to data
                        # not being a full subFrame but a CRC check is useful
                        # for testing firmware.
                        if (self.crccalc == data[word64+0:word64+2]):
                            print(f'GOOD NEWS: CRC in badly ordered data matches:  0x{hexMSBFirst(data[word64+0:word64+2])} == 0x{hexMSBFirst(self.crccalc)}')
                        else:
                            print(f' CRC in badly ordered data mismatches:  0x{hexMSBFirst(data[word64+0:word64+2])} != 0x{hexMSBFirst(self.crccalc)}')

            else:
                # reset self.crcmod in case previous data was not a full
                # subFrame so that CRC calculation gets reset on any
                # future non-full subFrame data
                self.crcmod = 0

                # To access the subframe data we need to create a byte array to hold the subframe data
                subframeData = bytearray(subframeSize)

                # Next we read the subframe data into the byte array, from offset subframe
                frame.read(subframeData,subframe)

                a = 0
                #@@@print(f'a = {a}')
                #@@@eventFrame.header = fullData[a+0:a+8]
                #@@@a += len(eventFrame.header)
                #@@@print(f'a = {a}')
                eventFrames[idx].preamble = subframeData[a+0:a+12]
                a += len(eventFrames[idx].preamble)
                eventFrames[idx].VFEdata = [subframeData[a+(x*20):a+((x+1)*20)] for x in range(5)]
                a += len(eventFrames[idx].VFEdata[0]*len(eventFrames[idx].VFEdata))
                eventFrames[idx].timeStampLHC = struct.unpack('<Q',subframeData[a+0:a+8])[0]
                a += len(subframeData[a+0:a+8])
                eventFrames[idx].trailer = subframeData[a+0:a+8]
                # Compute CRC over full sized subFrame. In this case, return mod can be ignored.
                (_mod, eventFrames[idx].crccalc) = crcgen(subframeData)

        # Return the results
        return (eventFrames, bad)


def crcgen(subFrame, mod=0, crc=b'\xff\xff'):
    # like a constant.
    wordsPerCRC = 16

    #@@@#print(f'1: mod={mod}, crc={crc}')
    # convert to 16-bit unsigned integer
    (crc, ) = struct.unpack('H', crc)
    if (mod == 0):
        # re-init crc
        crc = 0xffff

    #@@@#print(f'2: mod={mod}, crc={crc}')
    # calculate CRC over the 64-bit words in subFrame. If subFrame
    # contains a number of bytes not evenly divisible by 8, the CRC
    # may be calculated incorrectly.
    for words in struct.iter_unpack('Q', subFrame):
        word64 = words[0]       # words is a tuple even though a single value in tuple
        if (mod == (wordsPerCRC-1)):
            # If last 64-bit word, clear out the 16-bit CRC since it
            # was 0's in these bits when computing the CRC
            word64 = word64 & 0xffffffffffff0000
        #@@@print(f'crc = 0x{crc:04X} word64 = 0x{word64:016X}')
        crc = CRC16_8005_D64b_crcgen(crc,word64)
        mod = (mod+1) % wordsPerCRC

    # return updated mod count along with new crc. return in same
    # order as function arguments for clarity.
    return (mod, struct.pack('H', crc))

def getListFromFile(inputfile, raw_data = False, getDataframe=False):

    rec = Receiver()
    fread = pyrogue.utilities.fileio.FileReader(files=inputfile, configChan = None)

    for header, data in fread.records():
        arr = np.array(data, dtype=np.uint8)
        fframe = fakeFrame(header, arr)
        rec.process(fframe, raw_data = raw_data)

    if getDataframe:
      return pd.DataFrame(rec.TTeventList)

    return rec.TTeventList

def getHeaderList(df):

  for vfe in range(5):
    for ch in range(5):
      header_1 = df[df.type.eq(1) & df.vfe.eq(vfe) & df.ch.eq(ch)].count()
      print(f"{vfe} {ch} {header_1}")
