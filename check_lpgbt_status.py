import lpgbt_functions as lpgbt_functions
import time
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
# Script to test the slow communication between the BCP and the LpGBTs on the FE

def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    mlpgbt_device_add       = 113
    slpgbt_device_add_list  = [114, 115, 116]
    register_add            = 0x004
    data                    = [0x11,0x12]

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    slpgbt_device_add_list = lpgbt_functions.find_slave_lpgbts(myBcp)

    status = True

    #lpgbt_functions.mlpgbt_config_done(myBcp, 113, pll_config_done=True, dll_config_done=True)

    try: 
        for i in range(10):
            value_master = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, 0x1d9, 1)
            value_list = []
            for slpgbt_device_add in slpgbt_device_add_list:
                value_slave = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, 0x1d9, 1)
                value_list.append(value_slave)    
            print(f"{value_master}, {value_list}")
            time.sleep(1)
        
            if not (value_master==19 and len(set(value_list))==1 and value_list[0]==19):
              status = False

    except Exception as ex:
        print(ex)
        status = False
    finally:
        myBcp.stop()
        return status

if __name__ == "__main__":
   
    main() 
