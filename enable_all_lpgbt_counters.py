import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main():
   
    myBcp = lpgbt_functions.openBCPConnection()
   
    lpgbt_functions.reset_tclinks(myBcp) 
    lpgbt_functions.setTestModeLpgbt(myBcp, lpgbt_list=[113,114,115,116])

    myBcp.stop()


if __name__ == "__main__":


    
    main()
