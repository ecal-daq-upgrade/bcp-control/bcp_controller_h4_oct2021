import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import lpgbt_functions as lpgbt_functions
import math

def main(args):

    global ax
    global fig
    global plot1

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) - 1 for i in active_VFEs.split(',')]
    
    point_list = []

    for scan_num in range(6):
      for phase_val in range(16):
        file_name = f"data/pll_scan_fixed_phase/pll_scan_phase_{phase_val}_{scan_num}.txt"
        with open(file_name,"r") as f:
          counter = 1
          for line in f:
            result = line.find(":")
            if result != -1:
              pll_value = line[:3]
              pll_index = counter
              counter += 1
              for index,val in enumerate(line[result+1:].split(",")):
                if int(val.strip()) == -1: # No sync pattern at all
                  val = "-2"
                if int(val.strip()) == -9: # Different bit shifts between frames
                  val = "-3"
                point = {"ch":index, "PLL":int(pll_value), "PLL_index":pll_index, "shift":int(val.strip()), "phase":int(phase_val)}
                point_list.append(point)
    
    df = pd.DataFrame(point_list)

    groups = df.groupby(['ch'])
    good_shift = [[] for _ in range(25)]
    for ch, group in groups:
      good_index = -1
      shift_val, pll_val = group["shift"].to_numpy(), group['PLL'].to_numpy()
      for i in range(len(shift_val)-1): 
        if (shift_val[i]==shift_val[i+1]) and (shift_val[i]>0):
          good_shift[ch].append(pll_val[i])
          good_index = i
        else:
          continue
      good_shift[ch].append(pll_val[good_index+1])
    
    #might not for multiple files
    good_shift = [ sorted(_)[int(len(_)/2)] for _ in good_shift]
    print(good_shift)

    for vfe in which:
      for ch in range(1):
        for phase in range(16):
          if phase % 4 == 0:
            fig, ax = plt.subplots(4,1)
            fig.canvas.manager.set_window_title('VFE {} ch {}'.format(str(vfe+1), (str(ch))))
          plt_num = phase % 4 #int(math.modf(phase/4)[1])
          sub_df = df[df.ch.eq((vfe)*5+ch) & df.phase.eq(phase)]
          ax[plt_num].plot(sub_df["PLL_index"], sub_df['shift'], 'o', ms='3', label=f'DTU PLL, phase {phase}')
          ax[plt_num].axhline(y=0.0, color='r', linestyle='--')
          ax[plt_num].set_xlabel('DTU PLL sequence')
          ax[plt_num].set_ylabel('Bitshift / LpGBT PLL')
          ax[plt_num].legend()
          ax[plt_num].set_title(f"Channel{ch}", fontstyle='italic', y=1.0, pad=-14)
            
    for vfe in which:
      fig, ax = plt.subplots(5,1)
      fig.canvas.manager.set_window_title('VFE {} '.format(str(vfe+1), (str(ch))))
      for ch in range(5):
        sub_df = df[df.ch.eq((vfe)*5+ch) & df.PLL.eq(57)]
        ax[ch].plot(sub_df["phase"], sub_df['shift'], 'o', ms='3')
        ax[ch].axhline(y=0.0, color='r', linestyle='--')
        ax[ch].set_xlabel('LpGBT phase')
        ax[ch].set_ylabel('Bitshift')
        ax[ch].legend()


#   Showing results

    plt.show()
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-r", "--reset", help="Do the files you want to pass include VFE resets (y) or not (n), default is (n)", default="n")

    args = parser.parse_args()

    main(args)

