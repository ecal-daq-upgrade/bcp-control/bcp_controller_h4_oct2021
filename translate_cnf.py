my_conf = []
with open("slave.cnf", "r") as f:
  for line in f:
    if line[0] == '#':
      my_conf.append(line.strip())
    else:
      line = line.strip()
      reg = int(line.split()[0])
      val = int(line.split()[1], 16)
      newline = f"{reg:#05x}" + " " + f"{val:#04x}"
      my_conf.append(newline)

with open("hexconf.cnf", "w") as f:
  for line in my_conf:
    f.write(line+'\n')

    

