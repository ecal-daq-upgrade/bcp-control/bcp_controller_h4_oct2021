import sys
import argparse
import time
import os
import math

import lpgbt_functions as lpgbt_functions

def main():
   
    myBcp = lpgbt_functions.openBCPConnection()

    vtrx_device = lpgbt_functions.FindVTRx(myBcp)


    revid, chipid = lpgbt_functions.getVTRxVersion(myBcp, vtrx_device)
    print("VTRx revid ", revid)
    print("VTRx chipid ", chipid)

    #data = lpgbt_functions.ReadVTRx(myBcp, vtrx_device, 0x0)
    #print("VTRx register 0, enable channels:", data)
    #print("Disabling channel 4 (Slave 3 uplink)") 

    lpgbt_functions.WriteVTRx(myBcp, vtrx_device, 0x0, 0xF)
    data = lpgbt_functions.ReadVTRx(myBcp, vtrx_device, 0x0)
    print("VTRx Reg 0", data)

    myBcp.stop()


if __name__ == "__main__":
    
    main()
