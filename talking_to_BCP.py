import threading
import sys
import argparse
import time
from zmq import *
import socket
import argparse
import os

import lpgbt_functions as lpgbt_functions

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 0, TestMode = False, debugPrint = False)

    print("Reset Tclinks")
    links_ok = lpgbt_functions.reset_tclinks(myBcp)
    if links_ok:
        print("TC links ok")
    else:
        print("TC links bad")

    myBcp.stop()
