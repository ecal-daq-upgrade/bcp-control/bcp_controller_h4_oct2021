from zmq import *
import threading
import sys
class ZmqServer:

    def __init__(self,argv):
        self.context = Context()
        self.poller = Poller()
        self.bcpStatusSocketAddress = "tcp://localhost:7002"
        self.commandSocketAddress = "tcp://*:5566" # address of GUI PUB socket
        self.statusSocket = None    
        self.commandSocket = None

        self.action_loop_running = False
        self.lastMessage = ""

        # Socket to listen to the status from the bcp controller
        self.statusSocket = self.context.socket(SUB)
        self.statusSocket.connect(self.bcpStatusSocketAddress)
        self.statusSocket.setsockopt(SUBSCRIBE,b'')
        self.poller.register(self.statusSocket,POLLIN)

        # Socket to send commands
        self.commandSocket = self.context.socket(PUB)
        self.commandSocket.bind(self.commandSocketAddress)

        # Is there a better way to handle these threads?
        self.poll_sockets_running = True
        self.pool_socket_thread = threading.Thread(target = self.poll_sockets)
        self.pool_socket_thread.start()

    def poll_sockets(self):
        while self.poll_sockets_running:
            socks = dict(self.poller.poll(1))
            if (socks.get(self.statusSocket)):
                message = self.statusSocket.recv()
                self.lastMessage = message.decode("utf-8")
                #print('Message from app:{}'.format(self.lastMessage))
    
    def send_message(self,msg,param='',forcereturn=None):
        mymsg=msg
        print('Contents of param: ', str(param))
        if not param=='':
            mymsg=str().join([str(mymsg),' ',str(param)])
        
        print("Sending message:",mymsg)
        if self.commandSocket != None:
          self.commandSocket.send_string(mymsg)
        else:
          print("Trying to send a message to the applications with no pub defined, why?")

    def action_loop(self):
        print("Entering action loop")
        while self.action_loop_running:
            value = input("Do you want to see last message? [Y/n]\n")
            if value == "y" or value == "Y" or value == "":
                print(self.lastMessage)
            
            value = input("Do you want to send a message? [Y/n]\n")
            if value == "y" or value == "Y" or value == "":
                message = input("Write your message and press enter:\n")
                self.send_message(message)
            
            value = input("Do you want to continue? [Y/n]\n")
            if value == "n" or value == "N":
                self.poll_sockets_running = False
                self.action_loop_running = False

### Main ###

def main(argv):
  print(argv)
  zmqServer = ZmqServer(argv)

  zmqServer.action_loop_running = True
  zmqServer.action_loop()

if __name__ == '__main__':
  main(sys.argv)
