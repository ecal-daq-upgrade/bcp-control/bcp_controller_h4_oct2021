import sys
import argparse
import time
import os
import random 

import lpgbt_functions as lpgbt_functions

def main():
    

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    master_num = 2 #for VFE4 (SCA iic master)
    vfe_num = 4
    VFE_device_add = 10 #8:ADC0 
    ## Write ADC register wiht lpgbt
    #start_time = time.time()
    #WriteVFEDeviceWithRetry
    #elapsed_time_lpgbt = time.time() - start_time

    ## Write ADC register with SCA
    lpgbt_functions.EnableScaI2C(myBcp, 1) 
    lpgbt_functions.EnableScaI2C(myBcp, 2)
    lpgbt_functions.EnableScaI2C(myBcp, 3)
    lpgbt_functions.EnableScaI2C(myBcp, 5)
    lpgbt_functions.EnableScaI2C(myBcp, 7)

    rep = 1 #repetitions
    err_counter = 0
    elapsed_time = 0
    nbytes = 15 ## MAX:15
    reg_add = 0x00


    for master_num in range(16):
      print(f"master_num {master_num}")
      lpgbt_functions.EnableScaI2C(myBcp, master_num)
      result = lpgbt_functions.readMultiScaI2C(myBcp, master_num, device_add=VFE_device_add, reg_add=0x00, nbytes=1)
      print(hex(result))



    for i in range(rep):
      start_time = time.time()
      data = [random.randrange(256) for i in  range(nbytes)]
      if len(data)>4:
        data[4] = 0x00 # some values cause reset of the DTU
      lpgbt_functions.writeMultiScaI2C(myBcp, master_num, device_add=VFE_device_add, reg_add=reg_add, data=data)
      result = lpgbt_functions.readMultiScaI2C(myBcp, master_num, device_add=VFE_device_add, reg_add=reg_add, nbytes=nbytes) 
      #print([hex(val) for val in data])
      #print([hex(val) for val in result])
      for index, value in enumerate(data):
        if (reg_add + index) not in [9,10]: # 0x09 and 0x0a read only in DTU
          if value != result[index]: 
            print(value, result[index])
            print("WARNING: error detected in reg{:02x}! Total errors: {}".format(index, err_counter))
            err_counter += 1
      elapsed_time += time.time() - start_time
    print(f"\n==== Report after {rep} repetitions ====")
    print("Total errors: {}".format(err_counter))
    print("Total elapsed time: {:.4f} s\n".format(elapsed_time/rep))



    myBcp.stop() 

if __name__ == "__main__":
    main()














