import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main():
    
    myBcp = lpgbt_functions.openBCPConnection()
   
    lpgbt_functions.start_error_counters(myBcp)

    print("To read the results, use: \npython resync_status.py")    
    myBcp.stop()


if __name__ == "__main__":
    
    main()
