import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
from operator import add


def main():

    start_time = time.time()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]


    try:
        myfile = open("pll_scan.txt", "w")

        print("Send reset fast command to VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)
        time.sleep(0.1)

        print("Configure all DTUs")
        lpgbt_functions.configureAllDTUs(myBcp)
        time.sleep(0.1)

        print("Set all DTUs to PLL manual")
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, active_VFEs, enable=True)

        time.sleep(0.1)

        print("Enable sync mode")
        lpgbt_functions.enableSyncMode(myBcp, True)

        #pll_conf1_list = [0x31, 0x63] #[0x0, 0x1, 0x3, 0x7, 0x15, 0x31, 0x63]
        #pll_conf2_list = range(6,8)      
        pll_conf1_list = [0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F]
        #pll_conf1_list = [0x1, 0x3, 0x7, 0xF, 0x1F] ## [1, 11, 111, 1111, 11111] thermometric
        pll_conf2_list = range(0,8)                 ## binary
   
        for index1, pll_conf1 in enumerate(pll_conf1_list):
            for index2, pll_conf2 in enumerate(pll_conf2_list):

                pll_conf = (pll_conf1<<3)|pll_conf2
               
                myfile.write(f"{str(pll_conf).zfill(3)}, ")
                #print(f'pll_conf1 {pll_conf1}, pll_conf2 {pll_conf2}, pll_conf {pll_conf}')
               
                print('Before set PLL phase') 
                lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, active_VFEs, pll_conf=pll_conf)
               
                print('Before Re initialize') 
                lpgbt_functions.ReinitLpgbtTrain(myBcp)
                time.sleep(0.1)

                pll_gpio_list = lpgbt_functions.getGPIO_PLLLock(myBcp, repetition = 10)
                myfile.write(', '.join(str(x).zfill(2) for x in pll_gpio_list))  
                myfile.write("\n")

                myfile.flush()
        
        print("Disable sync mode")
        #lpgbt_functions.enableTestMode(myBcp, False)
        lpgbt_functions.enableSyncMode(myBcp, False)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
        print("Total elapsed time: {:.2f} min".format((time.time() - start_time)/60))

if __name__ == "__main__":
   
    main() 
