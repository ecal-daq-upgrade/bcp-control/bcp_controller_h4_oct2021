import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
from operator import add


def main():

    start_time = time.time()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    lpgbt_map = pd.read_csv("lpgbt_map.csv",index_col='channel')

    try:

        print("Send reset fast command to VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)
        time.sleep(0.1)

        print("Configure all DTUs")
        lpgbt_functions.configureAllDTUs(myBcp)
        time.sleep(0.1)

        print("Set all DTUs to PLL manual")
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, active_VFEs, enable=True)

        time.sleep(0.1)

        print("Enable sync mode")
        lpgbt_functions.enableSyncMode(myBcp, True)

        #pll_conf1_list = [0x31, 0x63] #[0x0, 0x1, 0x3, 0x7, 0x15, 0x31, 0x63]
        #pll_conf2_list = range(6,8)      
        pll_conf1_list = [0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F]
        #pll_conf1_list = [0x1, 0x3, 0x7, 0xF, 0x1F] ## [1, 11, 111, 1111, 11111] thermometric
        pll_conf2_list = range(0,8)                 ## binary
   
        which = [int(i) for i in active_VFEs.split(',')]
        DTU_list_pll = []
        for vfe_num in range(1,6):
          for ch_num in range(1,6):
            if vfe_num in which: 
              DTU_list_pll.append([vfe_num,ch_num])
            else:
              DTU_list_pll.append([])

        for index1, pll_conf1 in enumerate(pll_conf1_list):
            for index2, pll_conf2 in enumerate(pll_conf2_list):

                pll_conf = (pll_conf1<<3)|pll_conf2
               
                print('Before set PLL phase')
                for index, DTU in enumerate(DTU_list_pll):
                  if len(DTU) == 2:
                    lpgbt_functions.setPLLphase(myBcp, DTU[0], DTU[1], pll_conf=pll_conf)
               
                print('Before Re initialize') 
                lpgbt_functions.ReinitLpgbtTrain(myBcp)
                time.sleep(0.1)

                pll_gpio_list = lpgbt_functions.getGPIO_PLLLock(myBcp, repetition = 10)
                for index, value in enumerate(pll_gpio_list):
                  print(index, value)
                  if value and len(DTU_list_pll[index]) == 2:
                      DTU_list_pll[index].append(pll_conf)
                print(DTU_list_pll)             
 
                continueScan = False
                for DTU in DTU_list_pll:
                  if len(DTU) == 2:
                    continueScan = True
                if not continueScan:
                  break
 
        print("Disable sync mode")
        #lpgbt_functions.enableTestMode(myBcp, False)
        lpgbt_functions.enableSyncMode(myBcp, False)
        
        new_list = []
        for DTU in DTU_list_pll:
          if len(DTU) == 3:
            new_list.append(DTU[2])
          else:
            new_list.append(0)
        print(new_list)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
        print("Total elapsed time: {:.2f} min".format((time.time() - start_time)/60))

if __name__ == "__main__":
   
    main() 
