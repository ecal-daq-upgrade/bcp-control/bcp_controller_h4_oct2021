import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
import time

def apply_mask(x):
    x = hex(x)
    return x[2] == '3' and x[6] == '9'

def main(args):
    global ax
    global fig
    global plot1
    TTeventList = getListFromFile(args.inputfile, raw_data=0)
    print(len(TTeventList))

    df = pd.DataFrame(TTeventList)
#    df.at[100, 'value'] = 1091670289
    for vfe in range(5):
        for ch in range(5): 
            print(f'We are in vfe {vfe} ch {ch}')
            all_vals_df = df[df.vfe.eq(vfe) & df.ch.eq(ch) & df.type.eq(-999)]
            #all_vals_df['value'] = all_vals_df['value'].apply(apply_mask)
            #print( all_vals_df['type'] )
            all_vals_df.loc[:,'value'] = all_vals_df['value'].apply(lambda x: hex(x))
            print(all_vals_df['value'].value_counts())

if __name__ == "__main__":
    
        # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="continuous.dat")
    parser.add_argument("-of", "--outputfile", help="Output csv filename path", default="my_archive.csv")
    parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true') #type=bool, default=False)

    args = parser.parse_args()

    main(args) 
