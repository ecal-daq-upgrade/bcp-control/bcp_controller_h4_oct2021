import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the LpGBTs on the FE


def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try: 
        for i in range(32):
          try:
            value = lpgbt_functions.ReadScaAdcChannel(myBcp, i)
            print (f'Channel {i} \t{hex(value)} \t{value/4095:.5f}')
          except TimeoutError as ex:
            print(ex)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()


if __name__ == "__main__":
   
    main() 
