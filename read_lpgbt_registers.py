import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    mlpgbt_device_add       = 113
    slpgbt_device_add_list  = [114, 115, 116]
    register_add            = 0x0c8

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try: 
        print(f"Reading master lpgbt")
        value_old = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, register_add, 7)
        for cnt, value in enumerate(value_old):
            print("Register {}: {}".format(hex(register_add+cnt),hex(value)))
        for key, slpgbt_device_add in enumerate(slpgbt_device_add_list):
            print(f"Reading slave lpgbt {slpgbt_device_add}")
            value_old = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, register_add, nb_bytes=7)
            for cnt, value in enumerate(value_old):
                print("Register {}: {}".format(hex(register_add+cnt),hex(value)))
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()


if __name__ == "__main__":
   
    main() 
