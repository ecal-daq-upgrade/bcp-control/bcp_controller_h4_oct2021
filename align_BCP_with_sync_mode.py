import time
import lpgbt_functions as lpgbt_functions

def main():

    startTime = time.time()
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    try:
        bitshift = lpgbt_functions.align_BCP_with_sync_mode(myBcp)
        print(bitshift)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
    endTime = time.time()
    print(f"Total time {endTime - startTime}")

if __name__ == "__main__":
   
    main() 
