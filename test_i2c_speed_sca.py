import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main():
    

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    master_num = 2 #for VFE4 (SCA iic master)
    vfe_num = 4
    VFE_device_add = 8 #ADC0 
    '''
      def WriteVFEDeviceWithRetry(
          myBcp,
          VFE_num,
          VFE_device_add,
          VFE_reg_add,
          VFE_reg_width,
          data
      )
    '''
    ## Write ADC register wiht lpgbt
    #start_time = time.time()
    #WriteVFEDeviceWithRetry
    #elapsed_time_lpgbt = time.time() - start_time

    ## Write ADC register with SCA
    lpgbt_functions.EnableScaI2C(myBcp, master_num) 

    rep = 100 #repetitions

    elapsed_time = 0
    for i in range(rep):
      start_time = time.time()
      lpgbt_functions.writeMultiScaI2C(myBcp, master_num, device_add=8, reg_add=0x01, data=[0xa0])
      elapsed_time += time.time() - start_time
    print("Write 1 byte:\t\t{:.4f} s".format(elapsed_time/rep))
    #lpgbt_functions.readMultiScaI2C(myBcp, master_num, device_add, reg_add=0x00, nbytes=16) 


    elapsed_time = 0
    for i in range(rep):
      start_time = time.time()
      lpgbt_functions.writeMultiScaI2C(myBcp, master_num, device_add=8, reg_add=0x01, data=[0xa0 for i in range(15)])
      elapsed_time += time.time() - start_time
    print("Write 15 bytes:\t\t{:.4f} s".format(elapsed_time/rep))


    elapsed_time = 0
    for i in range(rep):
      start_time = time.time()
      lpgbt_functions.readMultiScaI2C(myBcp, master_num, device_add=8, reg_add=0x00, nbytes=1) 
      elapsed_time += time.time() - start_time
    print("Read 1 byte:\t\t{:.4f} s".format(elapsed_time/rep))

    elapsed_time = 0
    for i in range(rep):
      start_time = time.time()
      lpgbt_functions.readMultiScaI2C(myBcp, master_num, device_add=8, reg_add=0x00, nbytes=16) 
      elapsed_time += time.time() - start_time
    print("Read 15 bytes:\t\t{:.4f} s".format(elapsed_time/rep))

    myBcp.stop() 

if __name__ == "__main__":
    main()














