import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-cd", "--confDTUs", help="if 1 configure DTUs before acquisition", type=int, default=0)
    parser.add_argument("-cc", "--confCATIAs", help="if 1 configure CATIAs before acquisition", type=int, default=0)
    parser.add_argument("-ca", "--confADCs", help="if 1 configure ADCs before acquisition", type=int, default=0)
    parser.add_argument("-vref", help="bits for the Vref_reg", type=int, default=6)

    args = parser.parse_args()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 2, TestMode = False, debugPrint=False)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    if args.confDTUs:
        print("Configure all DTUs")
        lpgbt_functions.configureAllDTUs(myBcp)
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, active_VFEs, enable=True)
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, active_VFEs, pll_conf=399)
  
 
    if args.confCATIAs:    
        print("Configure all CATIAs")
        lpgbt_functions.configureAllCATIAs(myBcp)

    if args.confADCs:
        print("Configure all ADCs")
        lpgbt_functions.configureAllADCs(myBcp)

    for i in range(12):
      filename = f"continuous_short_{i}.dat"
      print(f"File name {filename}")
      lpgbt_functions.acquire_continuous(myBcp, duration=10, filename=filename)
    f_final = open(f"continuous_short_{i+1}.dat", 'w')
    f_final.close()
    myBcp.stop()
