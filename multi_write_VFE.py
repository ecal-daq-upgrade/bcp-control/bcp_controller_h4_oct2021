import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
import matplotlib.pyplot as plt
import random
def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)


    

    try:
        if True: 
            result_list = []
            for cnt in range(1):
                if cnt % 100 == 0:
                    print(cnt)
                start_time = time.time()

                data = []
                for i in range(5):
                  data.append(random.randrange(255) )


                print(data)
                lpgbt_functions.WriteVFEDevice(myBcp,4,0x12,0x00,1,data)
                data_res = lpgbt_functions.ReadVFEDevice(myBcp,4,0x12,0x00,1,len(data))
                if (data_res != data):
                  print(cnt, data_res, data)

                result_list.append(time.time() - start_time)

    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
