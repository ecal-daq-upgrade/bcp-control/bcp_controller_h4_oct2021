import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main():
    
    myBcp = lpgbt_functions.openBCPConnection()
    tclink_status = lpgbt_functions.get_tclink_status(myBcp, hr=True)
    print("TCLink_status")
    print(tclink_status)


    lpgbts_status = lpgbt_functions.get_lpgbts_status(myBcp, hr=True)
    print("\nLpGBT status")
    print(lpgbts_status)
  
    #lpgbts_status = lpgbt_functions.get_lpgbts_status(myBcp, hr=True)
    #print("\nLpGBT PLL phase")
    #print(lpgbts_status)

    uplink_status = lpgbt_functions.get_uplink_alignment(myBcp)
    for lpgbt_map in uplink_status:
      print(lpgbt_map)
      
 
    myBcp.stop()


if __name__ == "__main__":
    
    main()
