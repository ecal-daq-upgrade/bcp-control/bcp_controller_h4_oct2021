import sys
import time
import os

import lpgbt_functions as lpgbt_functions

def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try:
        print("Configure lpgbt master")
        lpgbt_functions.configure_mlpgbt_from_file(myBcp, 113, 'master_rom_ec.cnf')
        lpgbt_functions.mlpgbt_config_done(myBcp, 113, pll_config_done=True, dll_config_done=True)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
    


if __name__ == "__main__":
    
    main()
