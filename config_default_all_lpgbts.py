import sys
import time
import os

import lpgbt_functions as lpgbt_functions

def main():

    startTime = time.time()
    myBcp = lpgbt_functions.bcpConfigureAndStart()
    status = True

    try:
      lpgbt_functions.configure_default_all_lpgbts(myBcp) 
       # print("Reset bitshift in BCP")
       # lpgbt_functions.remove_all_shifts(myBcp)
 
       # print("Configure lpgbt master")
       # lpgbt_functions.configure_mlpgbt_from_file(myBcp, 113, 'master_rom_ec.cnf')
       # lpgbt_functions.mlpgbt_config_done(myBcp, 113, pll_config_done=1, dll_config_done=1)
       # time.sleep(1)

       # print("Reset master tclink")
       # lpgbt_functions.reset_tclinks(myBcp, master_only=True)
       # time.sleep(1)

       # print("Enable VTRx channels if recent version")
       # lpgbt_functions.enableVTRxChannels(myBcp, vtrx_device=0x50)

       # print("Reset slave lpgbts")
       # lpgbt_functions.reset_slave_lpgbts(myBcp)
       # time.sleep(1)

       # slpgbt_list = lpgbt_functions.find_slave_lpgbts(myBcp)

       # for addr in slpgbt_list:
       #     print(f"Configure slave lpgbt {addr}")
       #     lpgbt_functions.configure_slpgbt_from_file(myBcp, addr, 'slave.cnf')
       #     lpgbt_functions.slpgbt_config_done(myBcp, addr)

       # print("Reset all tclink")
       # lpgbt_functions.reset_tclinks(myBcp)
       # time.sleep(1)

       # print("Reset VFEs")
       # #lpgbt_functions.gpio_reset_VFE(myBcp, 115)
       # lpgbt_functions.fast_reset_VFE(myBcp)

    except Exception as ex:
        print(ex)
        status = False
    finally:
        myBcp.stop()
    endTime = time.time()
    print(f"Total time {endTime-startTime}")
    return status
    

if __name__ == "__main__":
    
    main()
