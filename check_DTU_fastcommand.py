import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart()
 
    DTU_init_value = {}
    DTU_new_value = {}
    errorCounter = 0

    try: 
      conf = lpgbt_functions.getConfig()
      active_VFEs = conf["active_VFEs"]


      for i in range(1000):
        print("Fast command reset VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)

        which = [int(i) for i in active_VFEs.split(',')]
        for vfe_num in which:
          for dtu_num in range(1,6):
            dtu_device_add = 8+2+8*(dtu_num-1)
            ch_num = (vfe_num - 1)*5 + dtu_num
            initial_val  = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, dtu_device_add, 0x01, 1, nb_bytes=1)
            DTU_init_value[ch_num] = initial_val
      
        print(DTU_init_value)
        print("Checking that they are all 0s")
        for key in DTU_init_value.keys():
          if DTU_init_value[key] != 0:
            print("Found value different from 0")
            errorCounter = errorCounter + 1

        print("Now we write something")
        for vfe_num in which:
          for dtu_num in range(1,6):
            dtu_device_add = 8+2+8*(dtu_num-1)
            ch_num = (vfe_num - 1)*5 + dtu_num
            initial_val  = lpgbt_functions.WriteVFEDevice(myBcp, vfe_num, dtu_device_add, 0x01, 1, 1)
      
        print("Now we read again")
        for vfe_num in which:
          for dtu_num in range(1,6):
            dtu_device_add = 8+2+8*(dtu_num-1)
            ch_num = (vfe_num - 1)*5 + dtu_num
            new_val  = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, dtu_device_add, 0x01, 1, nb_bytes=1)
            DTU_new_value[ch_num] = new_val

        print(DTU_new_value)
        print("Checking that they are all 1s")
        for key in DTU_new_value.keys():
          if DTU_new_value[key] != 1:
            print("Found value different from 1")
            errorCounter = errorCounter + 1

    except Exception as ex:
      print(ex)
    finally:
      print("errorCounter ",errorCounter) 
      myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    main(args)
