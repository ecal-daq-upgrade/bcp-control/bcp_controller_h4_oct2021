import sys
import argparse
import time
import os

# Add the bcp fw folders
import lpgbt_functions as lpgbt_functions

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = args.OutputType, TestMode = args.TestMode, debugPrint = False)
    
    print("Reset tclinks")
    tclink_status = lpgbt_functions.reset_tclinks(myBcp)
    if not tclink_status:
        raise Exception('error tclink status')
    time.sleep(1)

    # DTU flush (the DTU output FIFO is flushed)
    lpgbt_functions.fast_cmd_VFE(myBcp, mode=7)

    #lpgbt_functions.remove_all_shifts(myBcp)
    lpgbt_functions.fast_cmd_VFE(myBcp, mode=6)

    #myBcp.App.AppTx.SendFrame.set(1)
    myBcp.BCP.BCP_Readout.ClrBusy()
    old_trigger = 0

#    filename = f"continuous_trigger.dat"
#    open(filename, 'w').close()
#    myBcp.dataWriter.DataFile.set(filename)
#    myBcp.dataWriter.Open()
    frameGot = myBcp.App.AppTx.SendFrame.set(1)
    clrBusyDone = myBcp.BCP.BCP_Readout.ClrBusy() 

    try:
      while True:
        data_ready = myBcp.BCP.BCP_Readout.Readout_DataReady.get()
        #print(data_ready)
        if data_ready:

#        new_trigger = myBcp.BCP.BCP_Readout.NumTriggersReadout.get()
#        print(new_trigger)
#        
#        if new_trigger>old_trigger:
          frameGot = myBcp.App.AppTx.SendFrame.set(1)
          clrBusyDone = myBcp.BCP.BCP_Readout.ClrBusy() 
#          old_trigger = new_trigger

          
    except KeyboardInterrupt:
      print("CONTROL-C")
      myBcp.stop()
#      myBcp.dataWriter.Close()
    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    main(args)
