import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

file_name = "Catia1CalibData_gain1.csv"
df = pd.read_csv(file_name)
ax = plt.figure()
ax1 = ax.add_subplot(111)

#x = df.xI.apply(int, base=16)
#x = x/0xFF
x = df.Temp
#y = df.Vt
y = df.ADC_Count
#x = np.log(x)
#y = 1/y
plt.scatter(x, y)
#plt.plot(x, y, 'o', label='getI')
m,q = np.polyfit(x,y,1)

#print('I_bias from the slope is ', m/(1000+(0.003925*1000*31.9)))
#print('Temperature from the intercept is ', q/(m*0.003925))
#print(q)

ax1.set_xlabel("Temperature (Degrees Celcius)")
ax1.set_ylabel("ADC Count")
plt.plot(x, m*x + q, label=f'{m:.5f}x+{q:.5f}')
#plt.plot(x,x,label = "1:1 accuracy line")
#plt.scatter(x_processed,y_processed,label='processed')
print(m, q)
#plt.scatter(df.True_Voltage, df.ADC_slave_1, label='slave1')
#plt.scatter(df.True_Voltage, df.ADC_slave_2, label='slave2')
#plt.scatter(df.True_Voltage, df.ADC_slave_3, label='slave3')

ax.legend()

plt.show()

