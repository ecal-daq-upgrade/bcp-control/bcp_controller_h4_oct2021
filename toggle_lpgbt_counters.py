import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):
   
  myBcp = lpgbt_functions.openBCPConnection()
   
  countersEnable = lpgbt_functions.getLpgbtCounterEnable(myBcp, args.LpgbtAddress)
  lpgbt_functions.setTestModeLpgbt(myBcp, lpgbt_list=[args.LpgbtAddress], enable=(not countersEnable) )

  myBcp.stop()


if __name__ == "__main__":

  # Defining option list
  parser = argparse.ArgumentParser(description='Command line options parser')
  parser.add_argument("-a", "--LpgbtAddress", help="Lpgbt address where to toggle counters", type=int, default=113)
  args = parser.parse_args()

  main(args)
    
