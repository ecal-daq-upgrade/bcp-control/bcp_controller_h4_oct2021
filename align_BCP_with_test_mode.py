import time
import lpgbt_functions as lpgbt_functions

def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)


    

    try:
        #print("Send reset fast command to VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)
        time.sleep(2)

        #print("Configure all DTUs")
        lpgbt_functions.configureAllDTUs(myBcp)
        #time.sleep(1)
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, '2,4', enable=True)
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, '2,4', pll_conf=399)
        #print("Configure all CATIAs")
        #lpgbt_functions.configureAllCATIAs(myBcp, '2,3,4,5')
        #time.sleep(2)

        print("Zeroing bit shifts")
        lpgbt_functions.remove_all_shifts(myBcp)

        print("Enable test mode")
        lpgbt_functions.enableTestMode(myBcp, True)

        time.sleep(4)
        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data. Why do we need to acquire data a second time?")
        lpgbt_functions.acquireData(myBcp)

        time.sleep(0.2) # Don't go below 0.01 seconds, 0.1 it even safer
        print("Align channels")
        lpgbt_functions.align_channels(myBcp)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Disable test mode")
        lpgbt_functions.enableTestMode(myBcp, False)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
