import sys
import argparse
import time


import lpgbt_functions as lpgbt_functions
from data_functions import *
import pandas as pd

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 2, TestMode = False, debugPrint=False)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]
    
    print("Calibrating ADCs")
    lpgbt_functions.calibADCs(myBcp, calib_with_fast = True)

    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    main(args)
