To launch these scripts you need also to:
```
git submodule update --init --recursive
```
which will load the repo
```
https://gitlab.cern.ch/ecal-daq-upgrade/bcp-control/bcp-readout
```
taken from the original Thomas project
```
https://gitlab.cern.ch/tanderso/bcp-rudp-readout/-/tree/bcphw/
```

You will need also Rogue libraries plus other python libraries like beautifulsoup4 and lxml

Tested and working initially with:

- BCP_controller_h4_oct2021 revision 75a41c1
- bcp-rudp-readout revision dbc8b88
- Stephen's firmware 0x0004020A
- ndpc3 and 192.168.40.192

Then evolved...

You have to adapt BCP_DR_inverted.py line 10 to your path (top_level variable)

To load (or reload) the FPGA firmware tag 0.4.2, login to the ELM 
```
ssh root@elm10032
```
and then execute:
```
cd /mnt/persistent/bcp_user/
./configure_bcpd.sh -f bcpd_test_beams-0x0004020A-20210914200853-sdg-b59acf0.bit
```



FSM based on DataReadoutFSM in H4DAQ project
```
https://gitlab.cern.ch/ecal-daq-upgrade/H4DAQ
```
More or less shown by this picture

<img src="Readout_FSM.png" alt="Open data file" width="600" />

How to test:

1. In a first terminal launch command
```
python BCP_DR_inverted.py -c /home/gcucciat/bcp_controller_h4_oct2021/bcp_simple_config.xml -l /home/gcucciat/bcp_controller_h4_oct2021/log_bcp.log
```
2. In a second terminal
```
python zmq_server.py
```

