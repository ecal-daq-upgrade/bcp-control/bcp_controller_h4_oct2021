file1 = open("pll_scan_v1.txt", "r")
lines = file1.readlines()
#pll_value_list = [[0]*2] *25
#prev_list = [0, ['00']*25]

prev_list = [0, ['00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00']]
pll_value_list = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
pll_value_list_def = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]
pll_available_values = []
pll_chosen = []

for line in lines:
  channel_lock_list = line.split(": GPIO_PLL")
  channel_lock_list[1] = (channel_lock_list[1].strip()).split(", ")
  channel_lock_list[0] = int((channel_lock_list[0])[:3])
  pll_available_values.append(channel_lock_list[0]) 

  for col_num in range(25):
    
    if prev_list[1][col_num] == '00' and channel_lock_list[1][col_num] == '01':
        pll_value_list[col_num][1] = 1
        pll_value_list[col_num][0] = channel_lock_list[0]
    
    if prev_list[1][col_num] == '01' and channel_lock_list[1][col_num] == '01': 
      pll_value_list[col_num][1] = pll_value_list[col_num][1]+1

    if prev_list[1][col_num] == '01' and channel_lock_list[1][col_num] == '00' and pll_value_list[col_num][1] > 1:
      pll_value_list_def[col_num][1] = pll_value_list[col_num][1]
      pll_value_list_def[col_num][0] = pll_value_list[col_num][0]

  prev_list = channel_lock_list

print(pll_value_list_def)
# unpacking the list
for index in range(25):
  print(pll_value_list_def[index][0] + int(pll_value_list_def[index][1]/2))
  pll_chosen.append(pll_available_values[pll_value_list_def[index][0] + int(pll_value_list_def[index][1]/2)])
