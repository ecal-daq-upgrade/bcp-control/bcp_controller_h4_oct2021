import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
import time
import lpgbt_functions as lpgbt_functions
ax = None
plot1 = None
fig = None

total_hg_6bits = []
total_hg_12bits = []
total_lg = []

conf = lpgbt_functions.getConfig()
active_VFEs = conf["active_VFEs"]
active_VFEs = conf["active_VFEs"]
which = [int(i)-1 for i in active_VFEs.split(',')]

def Mouse(val):
    print('HERE')
    global ax
    global fig
    global plot1
    for ch in range(len(ax)):
      ax[ch].semilogy()
    fig.canvas.draw()
    fig.canvas.flush_events()

def DF_count(args):
    TTeventList = getListFromFile(args.inputfile)
#    print(len(TTeventList))

    df = pd.DataFrame(TTeventList)
    df.fillna(0, inplace=True)

    for vfe in range(5):
      for ch in range(5):
          ch_num = vfe*5+ch
          #print(f"Channel number {ch_num}")
          all_vals_df = df[df.vfe.eq(vfe) & df.ch.eq(ch)]
          
          single_ch_hg_df = df[df.vfe.eq(vfe) & df.ch.eq(ch) & df.hg.eq(1)]
         
          single_ch_hg_6bits_df = single_ch_hg_df[single_ch_hg_df.type.eq(1) | single_ch_hg_df.type.eq(2)]['value'].value_counts()
          #ADC[ch_num][0].append(single_ch_hg_6bits_df.index.tolist())
          #weight[ch_num][0].append(single_ch_hg_6bits_df.tolist())
          if len(total_hg_6bits) <= ch_num:
            total_hg_6bits.append(single_ch_hg_6bits_df)
          else:
            #print('hello adding')
            total_hg_6bits[ch_num] = total_hg_6bits[ch_num].add(single_ch_hg_6bits_df,fill_value=0)

#          if vfe==1 and ch==0:
#            for iidex, ii in enumerate(total_hg_6bits):
#              print(iidex)
#              print(ii)
#            print(single_ch_hg_6bits_df)
            #print(total_hg_6bits[ch_num], ch_num)                  

          single_ch_hg_12bits_df = single_ch_hg_df[single_ch_hg_df.type.eq(10) | single_ch_hg_df.type.eq(11)]['value'].value_counts()
          #ADC[ch_num][1].append(single_ch_hg_12bits_df.index.tolist())
          #weight[ch_num][1].append(single_ch_hg_12bits_df.tolist())
          if len(total_hg_12bits) <= ch_num:
            total_hg_12bits.append(single_ch_hg_12bits_df)
          else:
            total_hg_12bits[ch_num] = total_hg_12bits[ch_num].add(single_ch_hg_12bits_df,fill_value=0)

          single_ch_lg_df = df[df.vfe.eq(vfe) & df.ch.eq(ch) & df.hg.eq(0)]['value'].value_counts()
          #ADC[ch_num][2].append(single_ch_lg_df.index.tolist())
          #weight[ch_num][2].append(single_ch_lg_df.tolist())
          if len(total_lg) <= ch_num:
            total_lg.append(single_ch_lg_df)
          else:
            total_lg[ch_num] = total_lg[ch_num].add(single_ch_lg_df,fill_value=0)
 
def plot():
    global ax
    global fig
    global plot1

    fig = None
    ax = None
    for vfe in which:
        fig, ax = plt.subplots(5)
      
        fig.canvas.manager.set_window_title('VFE {}'.format(str(vfe+1)))
        #fig.set_size_inches(1200/fig.dpi,900/fig.dpi)
        for ch in range(5): 
            
          ch_num = vfe*5+ch
        
          min_hg_6b, min_hg_12b, min_lg = 0, 0, 0
          max_hg_6b, max_hg_12b, max_lg = 0, 0, 0

          if len(total_hg_6bits[ch_num]) != 0:
            min_hg_6b = min(total_hg_6bits[ch_num].keys())
            max_hg_6b = max(total_hg_6bits[ch_num].keys())
          if len(total_hg_12bits[ch_num]) != 0:
            min_hg_12b = min(total_hg_12bits[ch_num].keys())
            max_hg_12b = max(total_hg_12bits[ch_num].keys())
          if len(total_lg[ch_num]) != 0:
            min_lg = min(total_lg[ch_num].keys())
            max_lg = max(total_lg[ch_num].keys())

          min_val = min(min_hg_6b, min_hg_12b, min_lg)
          max_val = max(max_hg_6b, max_hg_12b, max_lg)

          x = []
          y = []

          for index, value in total_hg_6bits[ch_num].items():
            x.append(index)
            y.append(value)
#          ax[ch].scatter(x,y)
#          ax[ch].set_xlim(min_val-50,max_val+50)
          

          multi_histo = []
          multi_label = []           
 
          bins = np.arange(max(min_val-50-0.5, 0-0.5), max_val+50-0.5)
          one_histo = [x+0.5 for x in bins]
          multi_histo.append(one_histo)
          multi_histo.append(one_histo)
          multi_histo.append(one_histo)

          #multi_label.append(f'hg 6 - mean {mean:.2f} - std {stdev:.2f}')
          #multi_label.append(f'hg 12 - mean {mean:.2f} - std {stdev:.2f}')
          #multi_label.append('lg')
          multi_label.append(f'hg 6')
          multi_label.append(f'hg 12')
          multi_label.append('lg')

          weights = [[],[],[]]
          for center in one_histo:
            try:
              weights[0].append(total_hg_6bits[ch_num][int(center)])
            except KeyError:
              weights[0].append(0)
            try:
              weights[1].append(total_hg_12bits[ch_num][int(center)])
            except KeyError:
              weights[1].append(0)
            try:
              weights[2].append(total_lg[ch_num][int(center)])
            except KeyError:
              weights[2].append(0)
          ax[ch].hist(multi_histo, bins=bins, label=multi_label, weights=weights, histtype='step', fill=False)
          ax[ch].legend()

          #if len(total_lg[ch_num]) > 1:
          #  multi_histo.append(single_ch_lg_df['value'])
           #   multi_label.append('lg')            

           # if len(single_ch_hg_6bits_df) > 1:
           #   multi_histo.append(single_ch_hg_6bits_df['value'])
           #   mean, stdev = single_ch_hg_6bits_df['value'].mean(), single_ch_hg_6bits_df['value'].std()
           #   multi_label.append(f'hg 6 - mean {mean:.2f} - std {stdev:.2f}')

           # multi_histo.append(single_ch_hg_12bits_df['value'])
           # mean, stdev = single_ch_hg_12bits_df['value'].mean(), single_ch_hg_12bits_df['value'].std()
           # multi_label.append(f'hg 12 - mean {mean:.2f} - std {stdev:.2f}')

           # multi_histo.append(single_ch_hg_12bits_df_odd['value'])
           # mean, stdev = single_ch_hg_12bits_df_odd['value'].mean(), single_ch_hg_12bits_df_odd['value'].std()
           # multi_label.append(f'hg 12 odd - mean {mean:.2f} - std {stdev:.2f}')

           # multi_histo.append(single_ch_hg_12bits_df_even['value'])
           # mean, stdev = single_ch_hg_12bits_df_even['value'].mean(), single_ch_hg_12bits_df_even['value'].std()
           # multi_label.append(f'hg 12 even - mean {mean:.2f} - std {stdev:.2f}')
           #
           # if (min_val==0 and max_val==0) or (np.isnan(min_val) and np.isnan(max_val)):
           #     continue
           # bins = np.arange(max(min_val-50-0.5, 0-0.5), max_val+50-0.5)
           # ax[ch].hist(multi_histo, bins=bins, label=multi_label, histtype='step', fill=False)#, stacked=True, fill=False)
           # #ax[ch].semilogy()
           # ax[ch].legend()
           # #ax[ch].scatter([x for x in range(len(single_ch_hg_12bits_df['value']))], single_ch_hg_12bits_df['value'])
            

        fig.canvas.mpl_connect('button_press_event', Mouse)

    plt.show()

if __name__ == "__main__":
    
        # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="continuous_no_adc.dat")
    parser.add_argument("-of", "--outputfile", help="Output csv filename path", default="my_archive.csv")
    parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true') #type=bool, default=False)
    parser.add_argument("-b", "--bitshift", help="Apply bit shift to ADC", default=0, type=int)

    args = parser.parse_args()

    i = 0
    numberofFiles = 11
    while i < numberofFiles:
      filename = f'continuous_short_{i}.dat'
      filename_next = f'continuous_short_{i+1}.dat'
      try:
        f=open(filename_next)
        f.close()
        print(f"\n\n\n ################# {filename} ############### \n\n\n")
        args.inputfile = filename
        DF_count(args)
        i+=1  
      except FileNotFoundError:
        print(f"File {filename_next} not found")
        time.sleep(3)
        pass
#    for index, value in total_hg_6bits[5].items():
#      print(f"Index : {index}, Value : {value}")

    print(f"total_hg_6bits {len(total_hg_6bits)}")
    print(f"total_hg_12bits {len(total_hg_12bits)}")
    print(f"total_lg {len(total_lg)}")
    plot()
