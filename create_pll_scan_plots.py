import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import lpgbt_functions as lpgbt_functions

def main(args):

    global ax
    global fig
    global plot1

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) - 1 for i in active_VFEs.split(',')]
    
    point_list = []
    point_list_phase = []
    point_list_gpio = []      

    
    for file_number in range(1,11):
      if args.reset == "n":
        #file_name = f"data/pll_scan_no_reset/pll_scan{file_number}.txt"
        file_name = "pll_scan.txt"
      elif args.reset =="y":
        file_name = f"data/pll_scan_with_reset/pll_scan{file_number}.txt"
      with open(file_name,"r") as f:
        counter = 1
        for line in f:
          result = line.find(":")
          if result != -1:
            pll_value = line[:3]
            pll_index = counter
            counter += 1
            for index,val in enumerate(line[result+1:].split(",")):
              theValue = int(val.strip())
              if theValue == -1: 
                theValue = -5
              point = {"ch":index, "PLL":int(pll_value), "PLL_index":pll_index, "shift":theValue}
              point_list.append(point)
          else:
            if line.startswith("Phase"):
              for index,val in enumerate(line[10:].split(",")):
                point = {"ch":index, "PLL":int(pll_value), "PLL_index":pll_index, "value":int(val.strip())}
                point_list_phase.append(point)
            elif line.startswith("GPIO"):
              for index,val in enumerate(line[10:].split(",")):
                if  int(val.strip()) == 1:
                  point = {"ch":index, "PLL":int(pll_value), "PLL_index":pll_index, "value":10}
                else:
                  point = {"ch":index, "PLL":int(pll_value), "PLL_index":pll_index, "value":0}
                point_list_gpio.append(point)
    
    df = pd.DataFrame(point_list)
    df_phase = pd.DataFrame(point_list_phase)
    df_gpio = pd.DataFrame(point_list_gpio)

    groups = df.groupby(['ch'])
    good_shift = [[] for _ in range(25)]
    for ch, group in groups:
      good_index = -1
      shift_val, pll_val = group["shift"].to_numpy(), group['PLL'].to_numpy()
      for i in range(len(shift_val)-1): 
        if (shift_val[i]==shift_val[i+1]) and (shift_val[i]>0):
          good_shift[ch].append(pll_val[i])
          good_index = i
        else:
          continue
      good_shift[ch].append(pll_val[good_index+1])
    
    #might not for multiple files
    good_shift = [ sorted(_)[int(len(_)/2)] for _ in good_shift]
    print(good_shift)

    for vfe in which:
      fig, ax = plt.subplots(5,1)
      fig.canvas.manager.set_window_title('VFE {}'.format(str(vfe+1)))
      for ch in range(5):
        sub_df = df[df.ch.eq((vfe)*5+ch)]    
        ax[ch].plot(sub_df["PLL_index"], sub_df['shift'], 'o', ms='3', label='DTU PLL, bitshift')
        #sub_df_phase = df_phase[df.ch.eq((vfe)*5+ch)]
        #ax[ch].plot(sub_df_phase["PLL_index"], sub_df_phase["value"], 'o', ms='3', label='LpGBT PLL Initial Training')
        sub_df_gpio = df_gpio[df.ch.eq((vfe)*5+ch)]
        ax[ch].plot(sub_df["PLL_index"], sub_df_gpio['value'], 'o', ms='3', label='GPIO ')
        ax[ch].axhline(y=0.0, color='r', linestyle='--')
        ax[ch].set_xlabel('DTU PLL sequence')
        ax[ch].set_ylabel('Bitshift / LpGBT PLL')
        ax[ch].legend()
        ax[ch].set_title(f"Channel{ch}", fontstyle='italic', y=1.0, pad=-14)
            

#   Showing results

    plt.show()
        
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-r", "--reset", help="Do the files you want to pass include VFE resets (y) or not (n), default is (n)", default="n")
    parser.add_argument("-if", "--inputfile", help="Path of the input file", default="")

    args = parser.parse_args()

    main(args)

