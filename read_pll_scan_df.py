import pandas as pd
import numpy as np

df = pd.read_csv("pll_scan.txt", sep=",")
try:
  df.columns = np.arange(0,26)
except:
  print("The pll_scan.txt file should be created by PLL_scan_sync_mode.py. Otherwise it doesn't work")
  exit()
pll_value = []

# making it generic: all non-zero numbers to 1
df.iloc[:, 0] = df.iloc[:, 0].astype(int)
#df.iloc[:, 1:] = df.iloc[:, 1:].mask(df.iloc[:, 1:]!='00', '01')
#print(df)

for index_col in range(1,26):
    # selecting the consecutive pattern 
    df['consecutive' + str(index_col)] = df[index_col].groupby((df[index_col] != df[index_col].shift()).cumsum()).transform('size') * df[index_col]
    # masking the lorger pattern of consecutive rows 
    pll_value_df = df[0].loc[df['consecutive'+str(index_col)] == df[('consecutive'+str(index_col))].max()]
    pll_value_df = pll_value_df.reset_index(drop=True)
    #selecting the pll value in the center of the range
    pll_value.append(pll_value_df.iloc[int(pll_value_df.shape[0]/2)])

print('The PLL values chosen for each channel are ', pll_value)
