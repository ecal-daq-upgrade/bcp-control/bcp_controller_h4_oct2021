import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import lpgbt_functions as lpgbt_functions

def main(args):

    global ax
    global fig
    global plot1

    TTeventList = getListFromFile(args.inputfile)

    av_list= []
    np_array_list = []

    df = pd.DataFrame(TTeventList)
  
    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) - 1 for i in active_VFEs.split(',')]
    

    for vfe in which:
        fig, ax = plt.subplots(5,2)
        fig.canvas.manager.set_window_title('VFE {}'.format(str(vfe+1)))
        #for row in ax:
          #for axScatter in row:
            #ch = 4
        for ch in range(5):
           
            single_ch_df = df[df.vfe.eq(vfe) & df.ch.eq(ch)]
            single_ch_df.index = range(len(single_ch_df.index))
            single_ch_hg_df_index = single_ch_df.index[single_ch_df.hg.eq(1)]
            single_ch_lg_df_index = single_ch_df.index[single_ch_df.hg.eq(0)]
 
            single_ch_hg_df = single_ch_df[single_ch_df.hg.eq(1)]
            single_ch_lg_df = single_ch_df[single_ch_df.hg.eq(0)]

            multi_histo = []
            multi_label = []

            min_val = 9999
            max_val = 0
            if len(single_ch_lg_df) != 0:
                multi_histo.append(single_ch_lg_df['value'])
                max_val = single_ch_lg_df['value'].max()
                min_val = single_ch_lg_df['value'].min()
            else:
                multi_histo.append([])
            multi_label.append('lg')


            if len(single_ch_hg_df) != 0:
                multi_histo.append(single_ch_hg_df['value'])
                max_val_hg = single_ch_hg_df['value'].max()
                min_val_hg = single_ch_hg_df['value'].min()
                max_val = max_val_hg if max_val_hg > max_val else max_val
                min_val = min_val_hg if min_val_hg < min_val else min_val
            else:
                multi_histo.append([])
            multi_label.append('hg')

            if min_val == 9999 and max_val == 0:
                continue
            bins = np.arange(min_val-50, max_val+50)
            ax[ch,0].hist(multi_histo, label=multi_label, bins=bins, histtype='step', stacked=True, fill=False)
            ax[ch,0].semilogy()
            ax[ch,0].legend()

            ax[ch,1].scatter(single_ch_lg_df_index, single_ch_lg_df['value'], label=f"lg max:{single_ch_lg_df['value'].max()}", s=10)
            ax[ch,1].scatter(single_ch_hg_df_index, single_ch_hg_df['value'], label=f"hg max:{single_ch_hg_df['value'].max()}", s=10)
            #ax[ch,1].plot(multi_index, multi_plot, 'o', label=multi_label)
            ax[ch,1].legend()
            

#   Showing results

    plt.show()
        
if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    #parser.add_argument("-if", "--inputfile", help="Input filename path", default="../bcpd_test_beams_sw/software/scripts/data_20220202_104537.dat")
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="continuous.dat")
    parser.add_argument("-of", "--outputfile", help="Output csv filename path", default="my_archive.csv")
    parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true') #type=bool, default=False)

    args = parser.parse_args()

    print("TODO https://matplotlib.org/stable/gallery/lines_bars_and_markers/scatter_hist.html")

    main(args) 
