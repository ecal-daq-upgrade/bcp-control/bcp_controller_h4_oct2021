import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 2, TestMode = False, debugPrint=False)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    lpgbt_functions.genericAllDTUs(myBcp, halfSampleMode, active_VFEs)

    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')

    args = parser.parse_args()

    main(args)
