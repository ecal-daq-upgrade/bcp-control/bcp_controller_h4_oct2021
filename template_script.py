import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = args.OutputType, TestMode = args.TestMode, debugPrint = True)
    
    value = lpgbt_functions.ReadMasterLpgbt(myBcp, 113, 0x004, number_of_bytes=1)
    print(f"Our value {hex(value)}")
    lpgbt_functions.WriteMasterLpgbt(myBcp, 113, 0x004, 0x12)
    value = lpgbt_functions.ReadMasterLpgbt(myBcp, 113, 0x004, number_of_bytes=1)
    print(f"Our value {hex(value)}")
    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    main(args)
