import threading
import sys
import argparse
import time
from zmq import *
import socket
import argparse
import os

import lpgbt_functions as lpgbt_functions

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Command line options parser')

    args = parser.parse_args()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 0, TestMode = False, debugPrint = False)

    # Initialize decompression monitoring system
    lpgbt_functions.decomp_ctrl(myBcp)

    myBcp.stop()
