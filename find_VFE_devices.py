import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the VFE devices via LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    VFE_num                 = 2
    VFE_device_add          = 0x22 # one of the DTU
    register_add            = 0x0
    data                    = 0x01 #[0x00, 0x00, 0x88, 0x44, 0x55, 0x55, 0x00, 0x3c, 0x32, 0x88, 0x8f, 0x00]

    myBcp = lpgbt_functions.bcpConfigureAndStart()
  
    VFE_first_add = 8
    VFE_ch_step = 8
 
    for vfe_num in range(1,6):
      print(f"\nCheck VFE {vfe_num}")
      for ch_num in range(1,6):
        dtu_add = VFE_first_add+2+VFE_ch_step*(ch_num-1)
        catia_add = VFE_first_add+3+VFE_ch_step*(ch_num-1)
        #print(dtu_add, catia_add)
        try:
          value_old = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, dtu_add, 0, 1, 1)
          print(f"DTU {dtu_add} \t\tFOUND")
        except Exception as ex:
          print(f"DTU {dtu_add} \t\tnot found")
        try:
          value_old = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, catia_add, 0, 1, 1)
          print(f"CATIA {catia_add} \tFOUND")
        except Exception as ex:
          print(f"CATIA {catia_add} \tnot found")
		
        
#    try: 
#        print(f"\n\nChecking if we can multi read/write on {hex(VFE_device_add)} VFE device of VFE {VFE_num}:\n")
#        print(f"Reading reg {hex(register_add)} and {hex(register_add+1)}")
#        value_old = lpgbt_functions.ReadVFEDevice(myBcp, VFE_num, VFE_device_add, register_add, 1, 1)
#        print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")
#        print(f"Writing {data} on reg {hex(register_add)} and {hex(register_add+1)}")
#        lpgbt_functions.WriteVFEDevice(myBcp, VFE_num, VFE_device_add, register_add, 1, data)
#        print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
#        value_new = lpgbt_functions.ReadVFEDevice(myBcp, VFE_num, VFE_device_add, register_add, 1, 12)
#        print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_new}{END}")    
#        print(f"Writing original value on reg {hex(register_add)} and {hex(register_add+1)}")
#        lpgbt_functions.WriteVFEDevice(myBcp, VFE_num, VFE_device_add, register_add, 1, value_old)
#        print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
#        value_old = lpgbt_functions.ReadVFEDevice(myBcp, VFE_num, VFE_device_add, register_add, 1, 12)
#        print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")
#
#    except Exception as ex:
#        print(ex)
#    finally:
    myBcp.stop()


if __name__ == "__main__":
   
    main() 
