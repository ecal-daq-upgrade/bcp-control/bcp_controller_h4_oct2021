import lpgbt_functions as lpgbt_functions
import argparse

def main(args):

    VFE_first_add = 8
    VFE_ch_step = 8

    register_add = 0x1
    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try:
      for vfe_num in [int(i) for i in args.vfe.split(",")]:
        for vfe_ch in [int(i) for i in args.ch.split(",")]:
          
          vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
          value = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, register_add, 1)
          if args.half:
            lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, register_add, 1, value | 0x20)
            print(f'Writing vfe {vfe_num} ch {vfe_ch} reg {register_add}:', value | 0x20)
          else:
            lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, register_add, 1, value & 0xdf)
            print(f'Writing vfe {vfe_num} ch {vfe_ch} reg {register_add}:', value | 0x20)
 
          value = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, register_add, 1)
          print(f'Reading vfe {vfe_num} ch {vfe_ch} reg {register_add}:', value)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Turn on/off half-sampling rate')
    parser.add_argument("-half", help="turn on the half sampling rate", action='store_true') 
    parser.add_argument("-ch", help="list of chs to run, separated by comma", type=str, default="2")
    parser.add_argument("-vfe", help="list of VFEs to run, separated by comma", type=str, default="5")

    args = parser.parse_args()

    main(args) 
