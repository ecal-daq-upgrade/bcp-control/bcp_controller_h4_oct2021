import threading
import sys
import argparse
import time
from zmq import *
import socket
import argparse
import json
import os 


directory = os.path.dirname(os.path.abspath(__file__))


top_level = directory + "/" + 'bcp-readout/'
print(top_level)
sys.path.append(top_level+'firmware/surf/python')
sys.path.append(top_level+'firmware/')
import pyfirmware as fw






def bcpWorker(tid): 
    bcp = fw.Root(ip='192.168.40.192', serverPort=None)
    print(f'Starting BCP Root Node {tid}')
    bcp.start()
    print('sleep time')
    time.sleep(3)
    print(f'Stopping BCP Root Node {tid}')
    bcp.stop() 


#if __name__ == '__main__':



w1 = threading.Thread(target=bcpWorker, name='Worker-1', args=(1,))
w2 = threading.Thread(target=bcpWorker, name='Worker-2',args=(2,))

w1.start()
w2.start()
