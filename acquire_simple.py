import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):
    
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = args.OutputType, TestMode = args.TestMode, debugPrint = True)
    
    #print("Disable sync mode")
    #lpgbt_functions.enableSyncMode(myBcp, False)
    #lpgbt_functions.enableSyncMode(myBcp, True)

    ## Disabling sync mode on single VFEs
    #for i in range(1,6):
      #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=6, vfe_num=i)
      #time.sleep(2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=2, vfe_num=2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=3, vfe_num=2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=4, vfe_num=2)


    print("Enable sync mode")
    lpgbt_functions.enableSyncMode(myBcp, args.SyncMode)
    # DTU flush (the DTU output FIFO is flushed)
    #lpgbt_functions.fast_cmd_VFE(myBcp, mode=7)
    #lpgbt_functions.setHGDAC(myBcp, 4, 1, 171)

    #lpgbt_functions.remove_all_shifts(myBcp)
    #lpgbt_functions.fast_cmd_VFE(myBcp, mode=6)
    print("Acquire data")
    lpgbt_functions.acquireData(myBcp)
    #time.sleep(20)
    #print(lpgbt_functions.getGPIOList())

    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", action='store_true')
    parser.add_argument("-sm", "--SyncMode", help="Enable Sync Mode", action='store_true')

    args = parser.parse_args()

    main(args)
