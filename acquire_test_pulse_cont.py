import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions

def main(args):

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = args.OutputType, TestMode = args.TestMode, debugPrint = True)


#    lpgbt_functions.fast_cmd_VFE(myBcp, mode=7)
#    lpgbt_functions.fast_cmd_VFE(myBcp, mode=6)

    print("Acquire data")
#    lpgbt_functions.acquire_test_pulse(myBcp, int(args.Delay))
    lpgbt_functions.acquire_test_pulse_cont(myBcp, int(args.Delay))

    myBcp.stop()


if __name__ == "__main__":

    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-d", "--Delay", help="BX delay", type=int, default=1)
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)

    args = parser.parse_args()

    main(args)
