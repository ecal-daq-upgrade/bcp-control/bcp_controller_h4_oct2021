import sys
import argparse
import time
import os

import lpgbt_functions as lpgbt_functions
from lpgbt_functions import WriteVFEDeviceWithRetry

def main(args):
    
    #myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = args.OutputType, TestMode = args.TestMode, debugPrint = True)
    myBcp = lpgbt_functions.bcpConfigureAndStart()

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    vfe_list = [int(x) for x in active_VFEs.split(",")]
    VFE_first_add = 8
    VFE_ch_step = 8
 
    for vfe_num in [4]:
      for ch in [1]:
        adc0_add = VFE_first_add+0+VFE_ch_step*(ch-1)

        cycles = 100

        elapsedTime = 0
        for i in range(cycles):
          startTime = time.time()     
          lpgbt_functions.WriteVFEDeviceViaLpgbt(myBcp, vfe_num, adc0_add, 0x00, 1, 0x1)
          elapsedTime += (time.time() - startTime)
        print("Write 1 byte:\t\t{:.4f}".format(elapsedTime/cycles))
         
        elapsedTime = 0
        for i in range(cycles):
          startTime = time.time()     
          lpgbt_functions.WriteVFEDeviceViaLpgbt(myBcp, vfe_num, adc0_add, 0x00, 1, [0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xb,0xc,0xd])
          elapsedTime += (time.time() - startTime)
        print("Write 14 bytes:\t\t{:.4f}".format(elapsedTime/cycles))

        elapsedTime = 0
        for i in range(cycles):
          startTime = time.time()     
          lpgbt_functions.ReadVFEDeviceViaLpgbt(myBcp, vfe_num, adc0_add, 0x00, 1, 1)
          elapsedTime += (time.time() - startTime)
        print("Read 1 bytes:\t\t{:.4f}".format(elapsedTime/cycles))
        
        elapsedTime = 0
        for i in range(cycles):
          startTime = time.time()     
          lpgbt_functions.ReadVFEDeviceViaLpgbt(myBcp, vfe_num, adc0_add, 0x00, 1, 14)
          elapsedTime += (time.time() - startTime)
        print("Read 14 bytes:\t\t{:.4f}".format(elapsedTime/cycles))



    #print("Disable sync mode")
    #lpgbt_functions.enableSyncMode(myBcp, False)
    #lpgbt_functions.enableSyncMode(myBcp, True)

    ## Disabling sync mode on single VFEs
    #for i in range(1,6):
      #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=6, vfe_num=i)
      #time.sleep(2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=2, vfe_num=2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=3, vfe_num=2)
    #lpgbt_functions.fast_cmd_single_VFE(myBcp, mode=4, vfe_num=2)

    #lpgbt_functions.getAllADCValuesFromDTUs(myBcp)

    #print("Enable sync mode")
    #lpgbt_functions.enableSyncMode(myBcp, args.SyncMode)
    # DTU flush (the DTU output FIFO is flushed)
    #lpgbt_functions.fast_cmd_VFE(myBcp, mode=7)
    #lpgbt_functions.setHGDAC(myBcp, 4, 1, 171)

    #lpgbt_functions.remove_all_shifts(myBcp)
    #lpgbt_functions.fast_cmd_VFE(myBcp, mode=6)
    #print("Acquire data")
    #lpgbt_functions.acquireData(myBcp)
    #time.sleep(20)
    #print(lpgbt_functions.getGPIOList())

    #lpgbt_functions.test_full_config(myBcp)

    myBcp.stop()


if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", action='store_true')
    parser.add_argument("-sm", "--SyncMode", help="Enable Sync Mode", action='store_true')

    args = parser.parse_args()

    main(args)
