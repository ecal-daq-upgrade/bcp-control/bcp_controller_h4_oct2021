import time
import lpgbt_functions as lpgbt_functions
import argparse

def main(args):

    start_time = time.time()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    try:

        print("Set DTUs to PLL manual")
        lpgbt_functions.togglePLLForce(myBcp, 5, 3, enable=True)
        
        time.sleep(0.1)

        #[8, 9, 10, 11, 12, 13, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31, 56, 57, 58, 59, 60, 61, 62, 63, 120, 121, 122, 123, 124, 125, 126, 127, 248, 249, 250, 251, 252, 253, 254, 255, 504, 505, 506, 507, 508, 509, 510, 511]
        pll_conf = args.pll_value
        print(f"Set PLL value {pll_conf}")
        lpgbt_functions.setPLLphase(myBcp, 5, 3, pll_conf=pll_conf)

        time.sleep(0.1)
      
        pll_gpio_list = lpgbt_functions.getGPIO_PLLLock(myBcp)
        print(pll_gpio_list)

    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
        print("Total elapsed time: {:.2f} min".format((time.time() - start_time)/60))

if __name__ == "__main__":
  
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-pll", "--pll_value", help="PLL value to set to the DTU", type=int, default=57)
 
    args = parser.parse_args()
    main(args) 
