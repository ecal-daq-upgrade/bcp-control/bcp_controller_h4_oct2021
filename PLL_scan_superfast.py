import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
from operator import add
import collections

def main():

    start_time = time.time()

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]


    try:
        myfile = open("pll_scan.txt", "w")

        print("Set all DTUs to PLL manual")
        lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, active_VFEs, enable=True)

        time.sleep(0.1)

        #pll_conf1_list = [0x31, 0x63] #[0x0, 0x1, 0x3, 0x7, 0x15, 0x31, 0x63]
        #pll_conf2_list = range(6,8)      
        #pll_conf1_list = [0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F]
        pll_conf1_list = [0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F]
        #pll_conf1_list = [0x1, 0x3, 0x7, 0xF, 0x1F] ## [1, 11, 111, 1111, 11111] thermometric
        pll_conf2_list = range(0,8)                 ## binary
        full_list = []
        short_list = []   

        for pll_conf1 in pll_conf1_list:
            for pll_conf2 in pll_conf2_list:
                pll_conf = (pll_conf1<<3)|pll_conf2
                full_list.append(pll_conf)
        
        start_range = 0
        end_range = len(full_list)
	#Do it for one channel
        for dir_ in [1,-1]:
          ngood = 0
          first_good_pll_conf = -1
          for k in range(start_range, end_range, dir_):
                  pll_conf = full_list[k]
                  print('Before set PLL phase') 
                  lpgbt_functions.setPLLphase(myBcp, int(active_VFEs[0]), 1, pll_conf=pll_conf)
                 
                  print('Before Re initialize') 
                  lpgbt_functions.ReinitLpgbtTrain(myBcp)
                  time.sleep(0.1)
  
                  pll_gpio_list = lpgbt_functions.getGPIO_PLLLock(myBcp, repetition = 5)
  
                  if (ngood != 0) and (pll_gpio_list[0] != 1):
                    ngood = 0
                  elif pll_gpio_list[0] == 1:
                    if ngood == 0: first_good_pll_conf = pll_conf
                    ngood += 1 
                  if ngood >= 3:
                    short_list.append(first_good_pll_conf)
                    start_range = min(full_list.index(first_good_pll_conf)+12, len(full_list))
                    end_range = 0
                    break

        idx_lo = max(full_list.index(short_list[0])-5, 0)
        idx_hi = min(full_list.index(short_list[-1])+5, len(full_list)-1)
        print(full_list[idx_lo:idx_hi])

        early_stop_pattern = [1,1,1,0]
        pattern_len = len(early_stop_pattern)
        patterns = collections.deque(pattern_len*[], pattern_len)
        early_stop_ch = []

	#Do it for all channels
        for idx, pll_conf in enumerate(full_list[idx_lo:idx_hi]):
            
            myfile.write(f"{str(pll_conf).zfill(3)}, ")
            
            print('Before set PLL phase') 
            for vfe in [int(i) for i in active_VFEs.split(',')]:
              for ch in range(1,6):
                if ((vfe-1)*5+ch-1) in early_stop_ch: continue
                lpgbt_functions.setPLLphase(myBcp, vfe, ch, pll_conf=pll_conf)
            
            print('Before Re initialize') 
            lpgbt_functions.ReinitLpgbtTrain(myBcp)
            time.sleep(0.1)

            pll_gpio_list = lpgbt_functions.getGPIO_PLLLock(myBcp, repetition = 5)
            patterns.append(pll_gpio_list)
            if idx > (pattern_len-1):
              for ch in range(len(pll_gpio_list)):
                if early_stop_pattern == [patterns[i][ch] for i in range(pattern_len)]:
                  early_stop_ch.append(ch)

            myfile.write(', '.join(str(x).zfill(2) for x in pll_gpio_list))  
            myfile.write("\n")
            myfile.flush()

            if (sum(pll_gpio_list) == 0) and (idx > (idx_hi-idx_lo)/2): break
        
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()
        print("Total elapsed time: {:.2f} min".format((time.time() - start_time)/60))

if __name__ == "__main__":
   
    main() 
