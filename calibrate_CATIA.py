import threading
import sys
import argparse
import time
from zmq import *
import socket
import argparse

import lpgbt_functions as lpgbt_functions
import read_data_for_catia as catia_reader
if __name__ == "__main__":
    

    filename = 'catia_file.dat'

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    # Force High Gain Lite_DTU
    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    lpgbt_functions.genericAllDTUs(myBcp, forceHighGainAllDTUs, which=active_VFEs)

    list_CATIA_cycle = {}
    list_CATIA_calibrated = {}
    for i in range(25):
        list_CATIA_cycle[i] = 0x00

    for ped in range(0, 0x2F):
        for key, value in list_CATIA_cycle.items():
            vfenumber = int(key / 5) + 1
            slpgbt_device_add, slpgbt_i2c_ch = lpgbt_functions.fromVFEnumToMap(vfenumber) 
            vfe_device_add = (key % 5 + 1)*16 + 3 

            # write list_CATIA_cycle[i] in CATIA i-esimo
            lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, 2, 113, slpgbt_i2c_ch, slpgbt_device_add, vfe_device_add, 1, 0x03, 0x00)
            val_to_write = ped << 3 | 0x7
            lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, 2, 113, slpgbt_i2c_ch, slpgbt_device_add, vfe_device_add, 1, 0x03, val_to_write & 0xFF) 

        # Open file for data
        open(filename, 'w').close()
        myBcp.dataWriter.DataFile.set(filename)
        myBcp.dataWriter.Open()

        # read data
        print("Acquire data")
        lpgbt_functions.acquireData(myBcp)

        print("Acquire data. Why do we need to acquire data a second time?")
        lpgbt_functions.acquireData(myBcp)
    
        # Close file
        myBcp.dataWriter.Close()

        # extract value from data per channel
        rec = catia_reader.Rec()
        fread = pyrogue.utilities.fileio.FileReader(files=filename, configChan = None)
        for header, data in fread.records():
            arr = np.array(data, dtype=np.uint8)
            fframe = catia_reader.fakeFrame(header, arr)
            rec.process(fframe)
            
        # We can do an average and check if it is between 10 and 30
        # This check can be performed only for the channels still in list_CATIA_cycle
        for key, value in list_CATIA_cycle.items():
            vfenumber = int(key / 5) + 1
            chnumber = key % 5 + 1
            ch_average = 0
            for val in rec.TTeventList[vfenumber][chnumber]:
                ch_average += val

            ch_average = ch_average/len(rec.TTeventList[vfe_string][ch_string])
            if ch_average > 10 and ch_average < 30:
                # if yes remove pair from list_CATIA_cycle and save pair in list_CATIA_calibrated
                list_CATIA_calibrated[key] = value
                del list_CATIA_cycle[key]
            else:
                # if not increase val by 1
                list_CATIA_cycle[key] = list_CATIA_cycle[key] + 1
        # if list_CATIA_cycle empty stop cycle
        if len(list_CATIA_cycle) == 0:
            break

    # if some pairs still in list_CATIA_cycle at the end of the cycle copy them in list_CATIA_calibrated
    if len(list_CATIA_cycle) != 0:
        for key, value in list_CATIA_cycle.items():
            list_CATIA_calibrated[key] = value
    
    for key, value in list_CATIA_calibrated.items():
        print("Channel {}, using CATIA pedestals {}".format(str(key), str(value)))


    # Closing everything
    myBcp.stop()
