import argparse
from data_functions import *
import matplotlib.pyplot as plt
import time
import lpgbt_functions as lpgbt_functions



ax = None
plot1 = None
fig = None

def Mouse(val):
    print('HERE')
    global ax
    global fig
    global plot1
    for ch in range(len(ax)):
      ax[ch].semilogy()
    fig.canvas.draw()
    fig.canvas.flush_events()


def main(args):
    global ax
    global fig
    global plot1
   
    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) - 1 for i in active_VFEs.split(',')]    

 
    df = getListFromFile(args.inputfile, getDataframe=True)
    df.fillna(0, inplace=True)

    #getHeaderList(df)

    print(df.shape)
    for vfe in which:
        fig, ax = plt.subplots(5)
      
        fig.canvas.manager.set_window_title('VFE {}'.format(str(vfe+1)))
        #fig.set_size_inches(1200/fig.dpi,900/fig.dpi)
        fig.set_size_inches(800/fig.dpi,600/fig.dpi)
        for ch in range(5):
            if len(df) == 0:
              continue 
            all_vals_df = df[df.vfe.eq(vfe) & df.ch.eq(ch)]
            min_val = all_vals_df['value'].min()
            max_val = all_vals_df['value'].max()

            single_ch_hg_df = df[df.vfe.eq(vfe) & df.ch.eq(ch) & df.hg.eq(1)]
            single_ch_lg_df = df[df.vfe.eq(vfe) & df.ch.eq(ch) & df.hg.eq(0)]
           
            single_ch_hg_6bits_df = single_ch_hg_df[single_ch_hg_df.type.eq(1) | single_ch_hg_df.type.eq(2)]
            single_ch_hg_12bits_df = single_ch_hg_df[single_ch_hg_df.type.eq(10) | single_ch_hg_df.type.eq(11)]

            single_ch_hg_12bits_df_odd = single_ch_hg_df[ (single_ch_hg_df.type.eq(10) | single_ch_hg_df.type.eq(11) ) & single_ch_hg_12bits_df.time.eq(1)]
            single_ch_hg_12bits_df_even = single_ch_hg_df[( single_ch_hg_df.type.eq(10) | single_ch_hg_df.type.eq(11) ) & single_ch_hg_12bits_df.time.eq(0)]

            multi_histo = []
            multi_label = []           
 
            if len(single_ch_lg_df) > 1:
              multi_histo.append(single_ch_lg_df['value'])
              mean, stdev = single_ch_lg_df['value'].mean(), single_ch_lg_df['value'].std()
              multi_label.append(f'lg - mean {mean:.2f} - std {stdev:.2f}')

            if len(single_ch_hg_6bits_df) > 1:
              multi_histo.append(single_ch_hg_6bits_df['value'])
              mean, stdev = single_ch_hg_6bits_df['value'].mean(), single_ch_hg_6bits_df['value'].std()
              #multi_label.append(f'hg 6 - mean {mean:.2f} - std {stdev:.2f}')
              multi_label.append(f'mean {mean:.2f} - std {stdev:.2f}')

            if len(single_ch_hg_12bits_df) > 1:
              multi_histo.append(single_ch_hg_12bits_df['value'])
              mean, stdev = single_ch_hg_12bits_df['value'].mean(), single_ch_hg_12bits_df['value'].std()
              #multi_label.append(f'hg 12 - mean {mean:.2f} - std {stdev:.2f}')
              multi_label.append(f'ADC1 + ADC2 - mean {mean:.1f} - std {stdev:.1f}')


            #multi_histo.append(single_ch_hg_12bits_df_odd['value'])
            mean, stdev = single_ch_hg_12bits_df_odd['value'].mean(), single_ch_hg_12bits_df_odd['value'].std()
            #multi_label.append(f'hg 12 odd - mean {mean:.2f} - std {stdev:.2f}')
            #multi_label.append(f'ADC odd - mean {mean:.1f} - std {stdev:.1f}')

            #multi_histo.append(single_ch_hg_12bits_df_even['value'])
            mean, stdev = single_ch_hg_12bits_df_even['value'].mean(), single_ch_hg_12bits_df_even['value'].std()
            #multi_label.append(f'hg 12 even - mean {mean:.2f} - std {stdev:.2f}')
            #multi_label.append(f'ADC even - mean {mean:.1f} - std {stdev:.1f}')
           
            if (min_val==0 and max_val==0) or (np.isnan(min_val) and np.isnan(max_val)):
                continue
            bins = np.arange(max(min_val-50-0.5, 0-0.5), max_val+50-0.5)
            ax[ch].hist(multi_histo, bins=bins, label=multi_label, histtype='step', fill=False)#, stacked=True, fill=False)
            ax[ch].semilogy()
            ax[ch].legend()
            #ax[ch].scatter([x for x in range(len(single_ch_hg_12bits_df['value']))], single_ch_hg_12bits_df['value'])
            

        fig.canvas.mpl_connect('button_press_event', Mouse)

    plt.show()

if __name__ == "__main__":
    
        # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="continuous_no_adc.dat")
    parser.add_argument("-of", "--outputfile", help="Output csv filename path", default="my_archive.csv")
    parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true') #type=bool, default=False)
    parser.add_argument("-b", "--bitshift", help="Apply bit shift to ADC", default=0, type=int)

    args = parser.parse_args()

    main(args) 
