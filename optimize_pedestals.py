import sys
import argparse
import time


import lpgbt_functions as lpgbt_functions
from data_functions import *
import pandas as pd

def main(args):
   
    startTime = time.time() 
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = args.NumReadoutWords, OutputType = 2, TestMode = False, debugPrint=False)

    if args.calibADC:
        lpgbt_functions.calibADCs(myBcp, calib_with_fast = True)

    mean_map, dacval_map = lpgbt_functions.optimize_pedestals_bisec(myBcp, lowGain = False)
    lpgbt_functions.optimize_pedestals_bisec(myBcp, lowGain = True, hg_dacval_map=dacval_map)

    lpgbt_functions.extract_pedestal_config(myBcp)
    lpgbt_functions.getAllADCValuesFromDTUs(myBcp)
    myBcp.stop()

    endTime = time.time()
    print(f"Total time {endTime - startTime}")

if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=10)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)
    parser.add_argument("-cal", "--calibADC", help="calibrate the ADC on Lite-DTU", action='store_true')
    
    args = parser.parse_args()

    main(args)
