import threading
import sys
import argparse
import time
import zmq
import socket
import argparse
import json
import os 
import traceback
import math
import copy
# Add the bcp fw folders
configuration = {}
GPIO_config = []
directory = os.path.dirname(os.path.abspath(__file__))
with open(directory+"/TT_config.json") as conf_file:
  configuration = json.load(conf_file)
  conf_file.close()
with open(directory+"/GPIO_list.json") as conf_file:
  GPIO_config = json.load(conf_file)["GPIO_list"]
  conf_file.close()
with open(directory+"/ADC_list.json") as conf_file:
  ADC_config = json.load(conf_file)["ADC_list"]
  conf_file.close()
top_level = directory + "/" + configuration["bcp-rudp-readout"]
viaSca = configuration["viaSca"]
print(top_level)
sys.path.append(top_level+'firmware/surf/python')
sys.path.append(top_level+'firmware/')
import pyfirmware as fw
from data_functions import *
import pandas as pd

from lpgbt_register_map_v1 import LpgbtRegisterMapV1 as lpgbtConstants
from lpgbt_v1_enums import LpgbtEnumsV1 

VFE_first_add = 8
VFE_ch_step = 8

def getGPIOList():
    global GPIO_config
    return GPIO_config

def getADCList():
    global ADC_config
    return ADC_config

def fromGPIOListToMap():
    global GPIO_config
    my_map = {}
    for el in GPIO_config:
        if el[0] not in my_map:
            my_map[el[0]] = []
        my_map[el[0]].append(el[2])
    return my_map

def getConfig():
  global configuration
  return configuration

def checkZmqServerRunning():
    try:
        ctx = zmq.Context() 
        sub = ctx.socket(zmq.SUB)
        value = sub.connect('tcp://127.0.0.1:9099')
        print('sub.connect',value)
        value = sub.setsockopt(zmq.SUBSCRIBE,"".encode('utf-8'))
        print('sub.setsockopt',value)

        socket = ctx.socket(zmq.REQ)
        print('socket = ctx.socket',socket)

        value = socket.connect('tcp://127.0.0.1:9100')
        print('socket.connect',value)
    except Exception as ex:
        print(ex)

def openBCPConnection(ip=configuration["bcp_ip"]):
    # checkZmqServerRunning()
    bcp = fw.Root(ip=ip, serverPort=None)
    bcp.start()

    return bcp

def bcpConfigureAndStart(NumReadoutWords = 10, OutputType = 0, TestMode = False, debugPrint = True, ip=configuration["bcp_ip"]):
    # checkZmqServerRunning()
    FrameSize_val = 0xFFFFFFFF
    numReadoutWords_val = NumReadoutWords
    readoutShiftSel_val = 0
    towerDataSel_val = 0    # towerDataSel_Enum 
                            # 0:'Tower Data'
                            # 1:'Incr Counter'
                            # 2:'PRBS'

    

    bcp = fw.Root(ip=ip, serverPort=None)
    bcp.SwRx.outputType = OutputType
    bcp.SwRx.TestMode = TestMode
    bcp.start()
    print("Configuring the BCP for the acquisition")
    print('Test connection with the board reading Core.AxiVersion.FpgaVersion register: {}'.format(hex(bcp.Core.AxiVersion.FpgaVersion.get())))

    print("Setting DebugPrint to {}".format(debugPrint))
    bcp.SwRx.DebugPrint.set(debugPrint)

    print("setTimeStampNow command launched")
    #bcp.BCP.BCP_Readout.setTimeStampNow()

    print("Setting FrameSize to {}".format(FrameSize_val))
    bcp.App.AppTx.FrameSize.set(FrameSize_val)

    print("Setting NumReadoutWords to {}".format(numReadoutWords_val))
    bcp.BCP.BCP_Readout.NumReadoutWords.set(numReadoutWords_val)
    print("Setting ReadoutShiftSel to {}".format(readoutShiftSel_val))
    bcp.BCP.BCP_Readout.ReadoutShiftSel.set(readoutShiftSel_val)

    print("Setting TowerDataSel to {}".format(towerDataSel_val))
    bcp.BCP.BCP_Readout.TowerDataSel.set(towerDataSel_val)
    bcp.SwRx.DataCheckType.set(towerDataSel_val)

    #print("Clear busy")
    #bcp.BCP.BCP_Readout.ClrBusy()

    return bcp

def SendFramesAsync(bcpObj, nFrame=1):
    cnt = bcpObj.SwRx.ProcessCounter.get()
    #print(f"SendFramesAsync bcpObj.SwRx.ProcessCounter {cnt}, expecting next val {cnt+1}")
    bcpObj.App.AppTx.SendFrame.set(nFrame)
    #print(f"SendFramesAsync bcpObj.SwRx.SendFrame {bcpObj.App.AppTx.SendFrame.get()}")
    while(True):
      newcnt = bcpObj.SwRx.ProcessCounter.get()
      #print(cnt, newcnt)
      if (cnt != newcnt):
        break

def clearBusyRepeat(bcpObj):
    while bcpObj.BCP.BCP_Readout.Readout_Busy.get():
      print("############################################ IN CLEARBUSY LOOP #####################################")
      bcpObj.BCP.BCP_Readout.ClrBusy()

def acquireData(bcpObj):
    print("Clear busy")
    bcpObj.BCP.BCP_Readout.ClrBusy()
    clearBusyRepeat(bcpObj)
    #print(f"bcpObj.SwRx.SendFrame {bcpObj.App.AppTx.SendFrame.get()}, bcpObj.BCP.BCP_Readout.NumTriggersSeen {bcpObj.BCP.BCP_Readout.NumTriggersSeen.get()}, bcpObj.BCP.BCP_Readout.NumTriggersReadout {bcpObj.BCP.BCP_Readout.NumTriggersReadout.get()}, bcpObj.BCP.BCP_Readout.Readout_Busy {bcpObj.BCP.BCP_Readout.Readout_Busy.get()}")
    #print("Create software trigger")
    bcpObj.BCP.BCP_TCDS.SWTrigger()
    #print(f"bcpObj.SwRx.SendFrame {bcpObj.App.AppTx.SendFrame.get()}, bcpObj.BCP.BCP_Readout.NumTriggersSeen {bcpObj.BCP.BCP_Readout.NumTriggersSeen.get()}, bcpObj.BCP.BCP_Readout.NumTriggersReadout {bcpObj.BCP.BCP_Readout.NumTriggersReadout.get()}, bcpObj.BCP.BCP_Readout.Readout_Busy {bcpObj.BCP.BCP_Readout.Readout_Busy.get()}")
    #print("Requesting frames")
    SendFramesAsync(bcpObj)
    #print(f"bcpObj.SwRx.SendFrame {bcpObj.App.AppTx.SendFrame.get()}, bcpObj.BCP.BCP_Readout.NumTriggersSeen {bcpObj.BCP.BCP_Readout.NumTriggersSeen.get()}, bcpObj.BCP.BCP_Readout.NumTriggersReadout {bcpObj.BCP.BCP_Readout.NumTriggersReadout.get()}, bcpObj.BCP.BCP_Readout.Readout_Busy {bcpObj.BCP.BCP_Readout.Readout_Busy.get()}")

def getAllADCValuesFromDTUs(myBcp):
  conf = getConfig()
  active_VFEs = conf["active_VFEs"]
  which = [int(i) for i in active_VFEs.split(',')]

  folder = "data/"
  if not os.path.exists(folder): os.makedirs(folder)
  base_file_name = 'ADC_config_file_'
  end_file_name = '.dat'

  for vfe_num in which:
      file_path = folder + base_file_name + 'VFE' + str(vfe_num) + end_file_name
      with open(file_path, 'w') as f:
        for vfechannel in range(1,6):
          print(f"VFE {vfe_num}, channel {vfechannel}")
          vfe_device_add = VFE_first_add+0+VFE_ch_step*(vfechannel-1)
          val_list_0, val_list_1 = getADCvaluesFromDTU(myBcp,vfe_num,vfe_device_add)
          for val1, val2 in zip(val_list_0, val_list_1):
            f.write(str(val1) + " " + str(val2)+"\n")
            
    
def getADCvaluesFromDTU(myBcp,vfe_num,vfe_device_add):
  nbytes = 15
  val_list_0 = []
  val_list_1 = []
  for i in range(0,75,nbytes):
    val_list_0.extend(ReadVFEDevice(myBcp, vfe_num, vfe_device_add, i, 1, nb_bytes=nbytes))
    val_list_1.extend(ReadVFEDevice(myBcp, vfe_num, vfe_device_add+1, i, 1, nb_bytes=nbytes))
  return val_list_0, val_list_1
     
 
        
def configureAllADCs(myBcp):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]

    which = [int(i) for i in active_VFEs.split(',')]
    for i in which:
        configureAllADCsInVfe(myBcp, i, f"data/ADC_config_file_VFE{i}.dat")

def configureAllADCsInVfe(myBcp, vfe, file_path):
    print("Configuring ADCs in VFE {}".format(vfe))
    registerADC0 = []
    registerADC1 = []
    
    with open(file_path, 'r') as f:
        for line in f:
            l = line.strip()
            if l.startswith("#"):
               continue
            ll = l.split()
            if len(ll) >= 2:
                registerADC0.append(int(ll[0]))
                registerADC1.append(int(ll[1]))

    for i in range(5):
        print(f"writing channel {i} ADC0")
        tempList = registerADC0[75*i:75*(i+1)]
        configureADC(myBcp, vfe, i+1, tempList, 0)

        print(f"writing channel {i} ADC1")
        tempList = registerADC1[75*i:75*(i+1)]
        configureADC(myBcp, vfe, i+1, tempList, 1)


def configureADC(myBcp, vfenumber, vfechannel, registerList, adc_number):
    vfe_device_add = VFE_first_add+adc_number+VFE_ch_step*(vfechannel-1)
    
    step = 14
    for x in range(0, len(registerList), step):
        WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, x, 1, registerList[x:x+step])

def configureDTU(myBcp, vfenumber, vfechannel):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfechannel-1)

    bulk_data = [0x81, 0x03, 0x04, 0x20, 0x00, 0x00, 0x00, 0xff, 0x0f]
    if vfechannel == 4 and vfenumber==4:
      bulk_data = [0x81, 0x03, 0x04, 0x20, 0x00, 0x00, 0x00, 0xff, 0x0f]
    #if vfechannel == 5:
    #  bulk_data = [0x9f, 0x03, 0x04, 0x20, 0x04, 0x00, 0x00, 0xff, 0x0f]   
    #print("Before write ", vfenumber, vfechannel, ReadVFEDevice(myBcp, vfenumber, vfe_device_add, 0x00, 1, 9)) 
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x00, 1, bulk_data)
    #print("After write", vfenumber, vfechannel, ReadVFEDevice(myBcp, vfenumber, vfe_device_add, 0x00, 1, 9))
 
    bulk_data = [0x00, 0x00, 0x88, 0x44, 0x55, 0x55, 0x00, 0x3c, 0x32, 0x88, 0x8f, 0x00] #regs 09 0a are READ-ONLY
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x09, 1, bulk_data)
    
    #bulk_data = [0x0c, 0xcc, 0xcc, 0xcf, 0x00]
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x15, 1, bulk_data)

    print("Configured DTU {} in VFE {}".format(vfechannel, vfenumber))

def configureAllDTUs(myBcp):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]

    which = [int(i) for i in active_VFEs.split(',')]

    for vfe_num in which:
        for dtu_num in range(1,6):
            configureDTU(myBcp, vfe_num, dtu_num)


def forceHighGainDTU(myBcp, vfenumber, vfechannel):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfechannel-1)
    val_reg1 = ReadVFEDevice(myBcp, vfenumber, vfe_device_add, 0x01, 1, nb_bytes=1)
    val_reg1 = val_reg1 & 0x3F
    val_reg1 = val_reg1 | 0x40
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, val_reg1)

def forceLowGainDTU(myBcp, vfenumber, vfechannel):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfechannel-1)
    val_reg1 = ReadVFEDevice(myBcp, vfenumber, vfe_device_add, 0x01, 1, nb_bytes=1)
    val_reg1 = val_reg1 & 0x3F
    val_reg1 = val_reg1 | 0xC0
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, val_reg1)

def setFreeGainDTU(myBcp, vfenumber, vfechannel):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfechannel-1)
    val_reg1 = ReadVFEDevice(myBcp, vfenumber, vfe_device_add, 0x01, 1, nb_bytes=1)
    val_reg1 = val_reg1 & 0x3F
    val_reg1 = val_reg1 | 0x80
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, val_reg1)

def calibADCs(myBcp, calib_with_fast = True):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]

    which = [int(i) for i in active_VFEs.split(',')]

    if not calib_with_fast:
      for vfe_num in which:
        for ch in range(1,6):
          dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
          catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)

          # step 1: ADC mode off, CATIA sending normal signal
          val_reg1 = ReadVFEDevice(myBcp, vfe_num, dtu_add, 0x01, 1, nb_bytes=1)
          WriteVFEDeviceWithRetry(myBcp, vfe_num, dtu_add, 0x01, 1, val_reg1&0xFC) 
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x00)
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x00)

          # step 1.5: CATIA setting Vref 0.989V:  
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x06, 1, 0x0F+(vref<<4))

          # step 2: ADC mode off, CATIA sending calibration voltage
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x30)
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x00)

          # step 3: ADC active mode, CATIA sending calibration voltage
          val_reg1 = ReadVFEDevice(myBcp, vfe_num, dtu_add, 0x01, 1, nb_bytes=1)
          WriteVFEDeviceWithRetry(myBcp, vfe_num, dtu_add, 0x01, 1, val_reg1 | 0x03)


    else:
      for vfe_num in which:
        for ch in range(1,6):
          dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
          catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)

           # step 1.5: CATIA setting calibration Vref:  
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x06, 1, 0x0F+(vref<<4))

           # step 2: ADC mode off, CATIA sending calibration voltage
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x30)
      #     WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x00)
        fast_cmd_single_VFE(myBcp, mode=0x8, vfe_num=vfe_num)
        time.sleep(0.5)
        fast_cmd_single_VFE(myBcp, mode=0xA, vfe_num=vfe_num)
        time.sleep(0.5)
        fast_cmd_single_VFE(myBcp, mode=0x9, vfe_num=vfe_num)
        time.sleep(0.5)
        fast_cmd_single_VFE(myBcp, mode=0xB, vfe_num=vfe_num)          


    # ADCs are now calibrating.. sleeping for a while
    busy_list = []
    for repeat in range(5):
      for vfe_num in which:
        for ch in range(1,6):
          dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
          myval = ReadVFEDevice(myBcp, vfe_num, dtu_add, 0x09, 1, nb_bytes=1)
          busy_list.append(myval&0xa)
      busy_list.append('space')
    print(busy_list)

    time.sleep(1)   
    
    enableCATIAactiveMode(myBcp, active_VFEs)

def enableCATIAactiveMode(myBcp, which='1,2,3,4,5'):
  vfe_list = [int(x) for x in which.split(",")]
  for vfe_num in vfe_list:
        for ch in range(1,6):
          catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)

          # step 4: ADC active mode, CATIA sending normal signal
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0x0F)
          WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x05, 1, 0xFF)
        
def setHGbaseline(myBcp, vfenumber, vfechannel, value):
  dtu_add = VFE_first_add+2+VFE_ch_step*(vfechannel-1)
  WriteVFEDeviceWithRetry(myBcp, vfenumber, dtu_add, 0x05, 1, value)

def configureCATIA(myBcp, vfenumber, vfechannel):
    
    vfe_device_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1)
    # correction to bug in VFE early version

    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x02)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x03, 1, CATIAMap(vfenumber, vfechannel, 1))
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x03, 1, CATIAMap(vfenumber, vfechannel, 2))
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x04, 1, 0x1f) # no differences touching this parameter?
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x04, 1, 0xff)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x00)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x00)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x06, 1, 0x09)        

    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x02)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x03, 1, 0x10)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x03, 1, 0x87)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x04, 1, 0x9F) # no differences touching this parameter?
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x04, 1, 0xFF)
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0xCF)
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0xFF)
    #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x06, 1, 0x2B)

    print("Configured CATIA {} in VFE {}".format(vfechannel, vfenumber))

def CATIAMap(vfe_num, ch_num, val_num):
    catia_map = {}
    catia_map[1] = {
        '1a': 0x18,
        '1b': 0x0f,
        '2a': 0x1b,
        '2b': 0x27,
        '3a': 0x1a,
        '3b': 0x17,
        '4a': 0x17,
        '4b': 0x17,
        '5a': 0x18,
        '5b': 0x0f,
    }
    catia_map[2] = {
        '1a': 0x1f,
        '1b': 0x2f,
        '2a': 0x1c,
        '2b': 0x37,
        '3a': 0x1c,
        '3b': 0x3f,
        '4a': 0x1b,
        '4b': 0x27,
        '5a': 0x1f,
        '5b': 0x2f,
    }
    catia_map[3] = {
        '1a': 0x1f,
        '1b': 0x6f,
        '2a': 0x1f,
        '2b': 0x97,
        '3a': 0x1f,
        '3b': 0x97,
        '4a': 0x1f,
        '4b': 0x77,
        '5a': 0x1f,
        '5b': 0x6f,
    }
    catia_map[4] = {
        '1a': 0x1f,
        '1b': 0x5f,
        '2a': 0x1f,
        '2b': 0x67,
        '3a': 0x1f,
        '3b': 0x6f,
        '4a': 0x1a,
        '4b': 0x47,
        '5a': 0x1f,
        '5b': 0x5f,
    }
    catia_map[5] = {
        '1a': 0x1f,
        '1b': 0x77,
        '2a': 0x1f,
        '2b': 0x6f,
        '3a': 0x1a,
        '3b': 0x3f,
        '4a': 0x1c,
        '4b': 0x37,
        '5a': 0x1f,
        '5b': 0x77,
    }
    val1 = str(ch_num)+'a'
    val2 = str(ch_num)+'b'
    if val_num == 1:
        return catia_map[vfe_num][val1]
    elif val_num == 2:
        return catia_map[vfe_num][val2]

def configureAllCATIAs(myBcp):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]
    
    which = [int(i) for i in active_VFEs.split(',')]
    
    for vfe_num in which:
        for catia_num in range(1,6):
            configureCATIA(myBcp, vfe_num, catia_num)

def fromVFEnumToMap(vfenumber):
    assert vfenumber in range(1,6), "Invalid VFE number. Accepted from 1 to 5"

    slpgbt_add = 114
    slpgbt_i2c_num = 1
    if vfenumber == 1:
      slpgbt_add = 114
      slpgbt_i2c_num = 1
    if vfenumber == 2:
      slpgbt_add = 114
      slpgbt_i2c_num = 2
    if vfenumber == 3:
      slpgbt_add = 115
      slpgbt_i2c_num = 1
    if vfenumber == 4:
      slpgbt_add = 115
      slpgbt_i2c_num = 2
    if vfenumber == 5:
      slpgbt_add = 116
      slpgbt_i2c_num = 1

    return slpgbt_add, slpgbt_i2c_num


def ReadScaTemp(myBcp):

    return ReadScaAdcChannel(myBcp, 0)

def ReadScaAdcChannel(myBcp, channel_no):
    
    WriteSca(myBcp, 0x14, 0x04, 0x60, 0x0) 
    assert channel_no >=0 and channel_no <=31, "Channel no. should be between 0 to 31"
    value = ReadScaRegister(myBcp, 0x00, 0x07)
    value = value | 0x10000000
    WriteSca(myBcp, 0x00, 0x01, 0x06, value)
    WriteSca(myBcp, 0x14, 0x04, 0x50, channel_no)
    return WriteSca(myBcp, 0x14, 0x04, 0x02, 0x00000001)
    
def ReadScaData(myBcp):
    bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt

    return bcp_lpgbt_obj.tx_data_.get()

def EnableScaI2C(myBcp, master_ch = 0):

    print(f"Enabling I2C channel {master_ch} of SCA chip")
    print("++++++++++++ We are enabling a channel without disabling the others... is it what you want? ++++++++++++++++++++")
    # Reading SCA reg b,c,d
    
    reg_b = ReadScaRegister(myBcp, 0x00, 0x03)
    reg_c = ReadScaRegister(myBcp, 0x00, 0x05)
    reg_d = ReadScaRegister(myBcp, 0x00, 0x07)
   
    print(hex(reg_b), hex(reg_c), hex(reg_d))
 
    # Enabling 16 SCA I2C channels
    mask = 1 << (3 + master_ch)
    reg_b = reg_b | ((mask & 0xFF) << 24)
    reg_c = reg_c | (((mask >> 8) & 0xFF) << 24)
    reg_d = reg_d | (((mask >> 16) & 0xFF) << 24)

    WriteSca(myBcp, 0x00, 0x04, 0x02, reg_b)
    WriteSca(myBcp, 0x00, 0x04, 0x04, reg_c)
    WriteSca(myBcp, 0x00, 0x04, 0x06, reg_d)

    reg_b = ReadScaRegister(myBcp, 0x00, 0x03)
    reg_c = ReadScaRegister(myBcp, 0x00, 0x05)
    reg_d = ReadScaRegister(myBcp, 0x00, 0x07) 
  
    print(hex(reg_b), hex(reg_c), hex(reg_d))

#TODO
def halfSampleMode(myBcp, vfe_num, vfechannel, halfSample=True):
# Set 80MHz or 160MHz mode for LiTE-DTU
    vfe_device_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1)
    data = ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x01, 1, nb_bytes=8)
    if halfSample:
      data |= 0x20
    else:
      data &= 0xDF
    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x01, 1, data)

#TODO
def bc0_scan(bcpObj):
  for ch_num in range(1, 26):
    for i in range(150, 301):
      bcpObj.BCP.BCP_TCDS.shift_sel_bc0.set(i)
      myreg = getattr(myBcp.BCP.BCP_Lpgbt, "ch_locked_decomp"+str(ch_num))
      bc0 = myreg.get()
      if bc0 == 3:
        print(f"Channel {ch_num} bc0 received {i} clocks after global BC0 --> @BX +{i/4.}")
        continue
      print(f"No Channel BC0 found on channel {ch_num}")

def writeMultiScaI2C(myBcp, i2c_master, device_add, reg_add, data, freq=3):
  '''
  I2C multile bytes write operation procedure
  (see SCA-GBT documentation)
    - Write the starting register in BYTE0 of the data buffer
      and the *consecutives* values to write in BYTE1-15
    - Start a multibyte write operation using cmd 0xda
  '''
  ## freq 0: 100kHz, 1: 200kHz, 2: 400kHz, 3: 1MHz

  data_list = [reg_add]
  if isinstance(data, list):
    data_list.extend(data)
  else:
    data_list.append(data)

  assert len(data_list)<=16, "Data list too long, you can insert max 15 vals"

  ## Preparing the I2C master CTRL register value
  nbytes = len(data_list)
  sclmode = 0 # check the manual
  ctrl_val = ((sclmode & 0x1) << 7) + ((nbytes & 0x1F) << 2) + (freq & 0x3)
  WriteSca(myBcp, i2c_master+3, 0x04, 0x30, ctrl_val << 24)

  ## Then we prepare the reg add we want to read with I2C_W_DATA0
  sublist_of_data_list = [data_list[i*4: i*4+4] for i in range((int(len(data_list)/4)+1))] 
  for index, sublist in enumerate(sublist_of_data_list):
    data_bytes = 0
    command = index*16 + 0x40 # 0x40, 0x50, 0x60 or 0x70
    for num, single_byte in enumerate(sublist):
      data_bytes += single_byte << 8*(3-num%4) # shift of 24, 16, 8 or 0 bits
    WriteSca(myBcp, i2c_master+3, 0x04, command, data_bytes)
    
#  for index, single_byte in enumerate(data_list):
#    if index%4 == 0:
#      data_bytes = 0
#    command = int(index/4)*16+0x40 # 0x40, 0x50, 0x60 or 0x70
#    data_bytes += single_byte << 8*(3-index%4) # shift of 24, 16, 8 or 0 bits
#    WriteSca(myBcp, i2c_master+3, 0x04, command, data_bytes)
  
  ## Start the I2C multibyte write operation indicating **only** the slve address
  reg_bytes = (device_add & 0x7f) << 24
  response = WriteSca(myBcp, i2c_master+3, 0x04, 0xDA, reg_bytes)

def readMultiScaI2C(myBcp, i2c_master, device_add, reg_add, nbytes=1, freq=3):
  '''
  I2C multile bytes read operation procedure:
    - Set the pointer with **single** byte read operation (cmd: 0x82)
    - Start a multi byte read operation, the values are put in the
      data buffer, starting from BYTE0 to BYTE15 (see doc)
    - Read the data buffer (cmd 0x71, 0x061, 0x51, 0x41)
      **note** that the order is inverted w.r.t. the write operation
  '''

  assert nbytes>0, "nbytes too small, at least 1"
  assert nbytes<=16, "nbytes too high, max 16"

  ## Reset the pointer using a single byte write operation
  ## indicating the slave address and the register to point to
  WriteSca(myBcp, i2c_master+3, 0x04, 0x82, (device_add<<24)+(reg_add<<16))

  ## Preparing the I2C master CTRL register value
  sclmode = 0 # check the manual
  data = ((sclmode & 0x1) << 7) + ((nbytes & 0x1F) << 2) + (freq & 0x3)
  WriteSca(myBcp, i2c_master+3, 0x04, 0x30, data << 24)
  data = (device_add & 0x7f)
  response = WriteSca(myBcp, i2c_master+3, 0x04, 0xDE, data << 24)
  
  # Read DATA register
  bytes_list = []
  for num_answer in range(int((nbytes-1)/4)+1):
      command = 0x71-num_answer*16 # 0x71, 0x61, 0x51 or 0x41
      response = WriteSca(myBcp, i2c_master+3, 0x04, command, 0x0)
      for i in range(4):
        bytes_list.append((response >> (8*(3-i))) & 0xFF)
  bytes_list = bytes_list[0:nbytes]
  if len(bytes_list) == 1:
    bytes_list = bytes_list[0]
  return bytes_list

# Class ccamands to read sca registers
def ReadScaRegister(myBcp, channel, cmd):
    bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt
  
    tx_data = WriteSca(myBcp, channel, 4, cmd, 0)

    return tx_data

def WriteSca(myBcp, channel, length, cmd, data):
    bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt
  
    general_info = (cmd << 24) + (length << 16) + (channel << 8) + 1
    start_time = time.time()

    reply = 0
    bcp_lpgbt_obj.tx_transID_.set(general_info)
    bcp_lpgbt_obj.tx_data_.set(data)
    while reply != 1:
        reply = bcp_lpgbt_obj.rx_reply_received_s_.get()
        diff_time = time.time() - start_time
        if diff_time > 5:
            raise TimeoutError("Write SCA timeout!")
    return bcp_lpgbt_obj.tx_data_.get()

def GetAllCurrentPhase(myBcp):
  allPhase = {}
  for lpgbt_device_add in [113, 114, 115, 116]:
    allPhase[lpgbt_device_add] = {}
    for group in range(7):
      channel0_1 = 0x153+group*3
      channel2_3 = 0x154+group*3
      phase0_1 = ReadLpgbt(myBcp, lpgbt_device_add, channel0_1, number_of_bytes=1)
      phase2_3 = ReadLpgbt(myBcp, lpgbt_device_add, channel2_3, number_of_bytes=1)
      phase0 = (phase0_1 & 0x0F)
      phase1 = (phase0_1 & 0xF0) >> 4
      phase2 = (phase2_3 & 0x0F)
      phase3 = (phase2_3 & 0xF0) >> 4
      allPhase[lpgbt_device_add][group] = [phase0,phase1,phase2,phase3]
  return allPhase

def GetAllCurrentPLLLock(myBcp):
  allLocks = {}
  for lpgbt_device_add in [113, 114, 115, 116]:
    allLocks[lpgbt_device_add] = {}
    for group in range(7):
      channel0_3 = 0x152+group*3
      lock0_3 = ReadLpgbt(myBcp, lpgbt_device_add, channel0_3, number_of_bytes=1)
      allLocks[lpgbt_device_add][group] = [((lock0_3 >> i) & 0x1) for i in range(4,8)]
  return allLocks


def ReinitLpgbtTrain(myBcp):
    for device in [113,114,115,116]:
      WriteLpgbt(myBcp, device, 0x115, [0x11,0x11,0x11,0x11])
      WriteLpgbt(myBcp, device, 0x115, [0x00,0x00,0x00,0x00])

def setFixLpgbtPhase(myBcp, phase_val=0):
  assert phase_val < 16, "Phase value must be between 0 and 15"
  for device in [113,114,115,116]:
    WriteLpgbt(myBcp, device, 0x0c8, [0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C])
    for reg_add in [0x0d0, 0x0d4, 0x0d8, 0x0dc, 0x0e0, 0x0e4, 0x0e8]:
      reg_val = 0x00 | (phase_val<<4)
      WriteLpgbt(myBcp, device, reg_add, reg_val)

def ReadLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, number_of_bytes=1):
    if lpgbt_device_add == 113:
        return ReadMasterLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, number_of_bytes=number_of_bytes)
    elif lpgbt_device_add in [114, 115, 116]:
        return ReadSlaveLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, nb_bytes=number_of_bytes)
    raise Exception("Address provided not LpGBT")
    return 0

def WriteLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, data):
    if lpgbt_device_add == 113:
        return WriteMasterLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, data)
    elif lpgbt_device_add in [114, 115, 116]:
        return WriteSlaveLpgbt(myBcp, lpgbt_device_add, lpgbt_reg_add, data)
    raise Exception("Address provided not LpGBT")
    return 0

# Class commands to read and write on lpgbt registers
def ReadMasterLpgbt(myBcp, mlpgbt_device_add, mlpgbt_reg_add, number_of_bytes=1):
            bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt
            point_and_bytes_val = number_of_bytes << 16
            point_and_bytes_val = point_and_bytes_val | mlpgbt_reg_add # ex: 0x00101c5, read 1 byte at 0x1c5
            #bcp_lpgbt_obj.Point_and_bytes.set(point_and_bytes_val)
            bcp_lpgbt_obj.mem_addr_.set(mlpgbt_reg_add)
            bcp_lpgbt_obj.Number_of_consecutive_reads_.set(number_of_bytes)
            read_command_val = 1 << 8
            read_command_val = read_command_val | mlpgbt_device_add
#            bcp_lpgbt_obj.RW_command.set(read_command_val) # ex: 0x171, read command with lpgbt 0x71
#            bcp_lpgbt_obj.lpGBT_addr_.set(mlpgbt_device_add)
            bcp_lpgbt_obj.lpGBT_addr_.set(read_command_val)
#            bcp_lpgbt_obj.RW_cmd_.set(1)

            full = True
            start_time = time.time()
            while full:
              if time.time() - start_time > 0.1: 
                raise Exception("Read Master Timeout")
              #full = bcp_lpgbt_obj.IC_status.get() & 0x00000001
              full = bcp_lpgbt_obj.ic_empty_.get() & 0x00000001

            empty = False
            byte_list = []
            start_time = time.time()
            while not empty:
                #empty = bcp_lpgbt_obj.IC_status.get() & 0x00000001
                empty = bcp_lpgbt_obj.ic_empty_.get() & 0x00000001
                #mem_val = bcp_lpgbt_obj.Read_result.get() & 0x000000FF
                mem_val = bcp_lpgbt_obj.mem_val_.get() & 0x000000FF
                byte_list.append(mem_val)
                if time.time() - start_time > 0.1:
                  raise Exception("Read Master Timeout")
            if number_of_bytes == 1:
                return byte_list[6]
            elif number_of_bytes > 1:
                return byte_list[6:6+number_of_bytes]
            return None


def WriteMasterLpgbt(myBcp, mlpgbt_device_add, mlpgbt_reg_add, data):
            bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt
            #print("Writing a register of the master lpgbt")
            data_list = []
            if isinstance(data, list):
                data_list = data
            else:
                data_list = [data]
            for single_byte in data_list:
                #print(single_byte)
                #bcp_lpgbt_obj.IC_status.set(single_byte)
                bcp_lpgbt_obj.ic_empty_.set(single_byte)
            #bcp_lpgbt_obj.Point_and_bytes.set(mlpgbt_reg_add)
            bcp_lpgbt_obj.mem_addr_.set(mlpgbt_reg_add)
            write_command_val = 2 << 8
            write_command_val = write_command_val | mlpgbt_device_add
#            bcp_lpgbt_obj.RW_command.set(write_command_val) # ex: 0x171, read command with lpgbt 0x71
#            bcp_lpgbt_obj.lpGBT_addr_.set(mlpgbt_device_add)
            bcp_lpgbt_obj.lpGBT_addr_.set(write_command_val)
#            bcp_lpgbt_obj.RW_cmd_.set(2)
      

#def FastWriteMasterLpgbt(myBcp, mlpgbt_device_add, mlpgbt_reg_add, data):
#            bcp_lpgbt_obj = myBcp.BCP.BCP_Lpgbt
#            #print("Writing a register of the master lpgbt")
#            data_list = []
#            if isinstance(data, list):
#                data_list = data
#            else:
#                data_list = [data]
#            for single_byte in data_list:
#                #print(single_byte)
#                #bcp_lpgbt_obj.IC_status.set(single_byte)
#                bcp_lpgbt_obj.ic_empty_.set(single_byte)
#            write_command_val = 2 << 8
#            write_command_val = write_command_val | mlpgbt_device_add
#            #bcp_lpgbt_obj.RW_command.set(write_command_val) # ex: 0x171, read command with lpgbt 0x71
#            bcp_lpgbt_obj.lpGBT_addr_.set(mlpgbt_device_add)
#            bcp_lpgbt_obj.RW_cmd_.set(2)

# Taken from _i2c_master_get_reg_offsets
def _i2c_master_get_reg_offsets(master_id):
        assert master_id in range(3), "Invalid I2C master ID"

        offset_wr = master_id * (lpgbtConstants.I2CM1CMD.address - lpgbtConstants.I2CM0CMD.address)
        offset_rd = master_id * (lpgbtConstants.I2CM1STATUS.address - lpgbtConstants.I2CM0STATUS.address)

        return offset_wr, offset_rd

def _i2c_master_set_slave_address(myBcp, mlpgbt_device_add, master_id, slave_address):

        offset_wr, _ = _i2c_master_get_reg_offsets(master_id)

        address_low = slave_address & 0x7F
        address_high = (slave_address >> 7) & 0x07
        WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0ADDRESS.address + offset_wr, address_low)

def _i2c_master_set_slave_address_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, VFE_device_add):

        offset_wr, _ = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)

        address_low = VFE_device_add & 0x7F
        address_high = (VFE_device_add >> 7) & 0x07
        #WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0ADDRESS.address + offset_wr, address_low)
        WriteSlaveLpgbt(
            myBcp,
            slpgbt_device_add,
            lpgbtConstants.I2CM0ADDRESS.address + offset_wr,
            address_low,
        )

def _i2c_master_issue_command(myBcp, mlpgbt_device_add, master_id, command):
        assert master_id in range(3), "Invalid I2C master ID"

        offset_wr, _ = _i2c_master_get_reg_offsets(master_id)
        WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0CMD.address + offset_wr, command)

def _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, command):
        assert slpgbt_i2c_ch in range(3), "Invalid I2C master ID"

        offset_wr, _ = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)

        #WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0CMD.address + offset_wr, command)
        WriteSlaveLpgbt(
            myBcp,
            slpgbt_device_add,
            lpgbtConstants.I2CM0CMD.address + offset_wr,
            command
        )

def _i2c_master_set_nbytes(myBcp, mlpgbt_device_add, master_id, nbytes, freq=3):
        assert master_id in range(3), "Invalid I2C master ID"
        assert nbytes in range(16), "Invalid transaction length"

        offset_wr, offset_rd = _i2c_master_get_reg_offsets(master_id)

        control_reg_val = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0CTRL.address + offset_rd)
        control_reg_val &= (~LpgbtEnumsV1.I2cmConfigReg.NBYTES.bit_mask)
        control_reg_val &= (~LpgbtEnumsV1.I2cmConfigReg.FREQ.bit_mask)        

        control_reg_val |= nbytes << LpgbtEnumsV1.I2cmConfigReg.NBYTES.offset
        control_reg_val |= freq << LpgbtEnumsV1.I2cmConfigReg.FREQ.offset
        
        WriteMasterLpgbt(
            myBcp, mlpgbt_device_add, 
            lpgbtConstants.I2CM0DATA0.address + offset_wr,
            control_reg_val
        )
        _i2c_master_issue_command(myBcp, mlpgbt_device_add, master_id, LpgbtEnumsV1.I2cmCommand.WRITE_CRA)

def _i2c_master_set_nbytes_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, nbytes, freq=3):
        assert slpgbt_i2c_ch in range(3), "Invalid I2C master ID"
        assert nbytes in range(16), "Invalid transaction length"

        offset_wr, offset_rd = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)

        #control_reg_val = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.I2CM0CTRL.address + offset_rd)
       
        control_reg_val = 0
        control_reg_val &= (~LpgbtEnumsV1.I2cmConfigReg.NBYTES.bit_mask)
        control_reg_val &= (~LpgbtEnumsV1.I2cmConfigReg.FREQ.bit_mask)

        control_reg_val |= nbytes << LpgbtEnumsV1.I2cmConfigReg.NBYTES.offset
        control_reg_val |= freq << LpgbtEnumsV1.I2cmConfigReg.FREQ.offset
        WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.I2CM0DATA0.address + offset_wr, control_reg_val)
        
        _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_CRA)

def _i2c_master_await_completion(myBcp, mlpgbt_device_add, master_id, timeout=0.1):
        _, offset_rd = _i2c_master_get_reg_offsets(master_id)

        timeout_time = time.time() + timeout
        while True:
            status = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0STATUS.address + offset_rd, 1)

            if status & LpgbtEnumsV1.I2cmStatusReg.SUCC.bit_mask:
                break

            if status & LpgbtEnumsV1.I2cmStatusReg.LEVEERR.bit_mask:
                #raise LpgbtI2CMasterBusError("The SDA line is pulled low before initiating a transaction.")
                raise Exception("The SDA line is pulled low before initiating a transaction.")

            if status & LpgbtEnumsV1.I2cmStatusReg.NOACK.bit_mask:
                #raise LpgbtI2CMasterTransactionError("The last transaction was not acknowledged by the I2C slave")
                raise Exception("The last transaction was not acknowledged by the I2C slave")

            if time.time() > timeout_time:
                #raise LpgbtTimeoutError(f"Timeout while waiting for I2C master to finish (status:0x{status:02x})")
                raise Exception(f"Timeout while waiting for I2C master to finish (status:0x{status:02x})")
                

def _i2c_master_await_completion_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, timeout=0.1):
        
        _, offset_rd = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)

        timeout_time = time.time() + timeout
        while True:
            #status = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0STATUS.address + offset_rd, 1)
            status = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.I2CM0STATUS.address + offset_rd)
            if status & LpgbtEnumsV1.I2cmStatusReg.SUCC.bit_mask:
                break

            if status & LpgbtEnumsV1.I2cmStatusReg.LEVEERR.bit_mask:
                #raise LpgbtI2CMasterBusError("The SDA line is pulled low before initiating a transaction.")
                raise Exception("Slave LpGBT: The SDA line is pulled low before initiating a transaction.")

            if status & LpgbtEnumsV1.I2cmStatusReg.NOACK.bit_mask:
                #raise LpgbtI2CMasterTransactionError("The last transaction was not acknowledged by the I2C slave")
                raise Exception("Slave LpGBT: The last transaction was not acknowledged by the I2C slave")

            if time.time() > timeout_time:
                #raise LpgbtTimeoutError(f"Timeout while waiting for I2C master to finish (status:0x{status:02x})")
                raise Exception(f"Slave LpGBT: Timeout while waiting for I2C master to finish (status:0x{status:02x})")

# Taken from i2c_master_read
def ReadSlaveLpgbt(
    myBcp, 
    slpgbt_device_add, 
    slpgbt_reg_add, 
    nb_bytes=1, 
    mlpgbt_i2c_ch=2, 
    mlpgbt_device_add=113, 
    slpgbt_reg_width=2,
    clk_freq=0, 
    timeout=0.1
):

            offset_wr, offset_rd = _i2c_master_get_reg_offsets(mlpgbt_i2c_ch)

            _i2c_master_set_slave_address(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, slpgbt_device_add)
            for i in range(slpgbt_reg_width):
                WriteMasterLpgbt(
                    myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0DATA0.address + offset_wr + i, (slpgbt_reg_add >> (8 * i)) & 0xFF
                )

            _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.W_MULTI_4BYTE0)
            _i2c_master_set_nbytes(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, slpgbt_reg_width, freq=clk_freq)
            _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_MULTI)
            _i2c_master_await_completion(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, timeout)

            _i2c_master_set_nbytes(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, nb_bytes, freq=clk_freq)
            _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.READ_MULTI)
            _i2c_master_await_completion(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, timeout)

            byte_list = []
            for i in range(0, nb_bytes):
                result_address = lpgbtConstants.I2CM0READ15.address + offset_rd - i
                read_value = ReadMasterLpgbt(myBcp, mlpgbt_device_add, result_address, 1)
                if nb_bytes == 1:
                    return read_value
                byte_list.append(read_value)
            return byte_list

# Taken from i2c_master_write
def WriteSlaveLpgbt(
    myBcp, 
    slpgbt_device_add, 
    slpgbt_reg_add, 
    data, 
    mlpgbt_i2c_ch=2, 
    mlpgbt_device_add=113, 
    slpgbt_reg_width=2, 
    clk_freq=0, 
    timeout=0.1
):

    offset_wr, _ = _i2c_master_get_reg_offsets(mlpgbt_i2c_ch)
    address_and_data = []
    for i in range(slpgbt_reg_width):
        address_and_data.append((slpgbt_reg_add >> (8 * i)) & 0xFF)
    try:
        data_list = list(data)
    except TypeError:
        data_list = list((data,))
    address_and_data.extend(data_list)
    assert len(address_and_data) in range(16), "Unsupported I2C write length"

    _i2c_master_set_nbytes(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, len(address_and_data), freq=clk_freq)

    for i, data_byte in enumerate(address_and_data):
        WriteMasterLpgbt(
            myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0DATA0.address + offset_wr + (i % 4), data_byte
        )
        if i % 4 == 3 or i == len(address_and_data) - 1:
            _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.W_MULTI_4BYTE0 + (i // 4))

    _i2c_master_set_slave_address(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, slpgbt_device_add)
    _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_MULTI)

    _i2c_master_await_completion(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, timeout)

# Taken from i2c_master_write
def FastWriteSlaveLpgbt(
    myBcp,
    slpgbt_device_add,
    slpgbt_reg_add,
    data,
    mlpgbt_i2c_ch=2,
    mlpgbt_device_add=113,
    slpgbt_reg_width=2,
    clk_freq=0,
    timeout=0.1
):

    offset_wr, _ = _i2c_master_get_reg_offsets(mlpgbt_i2c_ch)
    address_and_data = []
    for i in range(slpgbt_reg_width):
        address_and_data.append((slpgbt_reg_add >> (8 * i)) & 0xFF)
    try:
        data_list = list(data)
    except TypeError:
        data_list = list((data,))
    address_and_data.extend(data_list)
    assert len(address_and_data) in range(16), "Unsupported I2C write length"

    for i, data_byte in enumerate(address_and_data):
        WriteMasterLpgbt(
            myBcp, mlpgbt_device_add, lpgbtConstants.I2CM0DATA0.address + offset_wr + (i % 4), data_byte
        )
        if i % 4 == 3 or i == len(address_and_data) - 1:
            _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.W_MULTI_4BYTE0 + (i // 4))

    _i2c_master_issue_command(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_MULTI)

    _i2c_master_await_completion(myBcp, mlpgbt_device_add, mlpgbt_i2c_ch, timeout)


def ReadVFEDevice(
    myBcp,
    VFE_num,
    VFE_device_add,
    VFE_reg_add,
    VFE_reg_width,
    nb_bytes=1,
    freq=0,
    viaSCA=viaSca
):

  if viaSCA:
    master_num = 0
    if VFE_num == 1:
      master_num = 3
    elif VFE_num == 2:
      master_num = 1
    elif VFE_num == 3:
      master_num = 5
    elif VFE_num == 4:
      master_num = 2
    elif VFE_num == 5:
      master_num = 7

    return readMultiScaI2C(myBcp, master_num, VFE_device_add, VFE_reg_add, nbytes=nb_bytes)
  else:
    return ReadVFEDeviceViaLpgbt(myBcp, VFE_num, VFE_device_add, VFE_reg_add, VFE_reg_width, nb_bytes=nb_bytes, freq=freq)



def ReadVFEDeviceViaLpgbt(
    myBcp,
    VFE_num,
    VFE_device_add,
    VFE_reg_add,
    VFE_reg_width,
    nb_bytes=1,
    freq=0,
):
   
    slpgbt_device_add, slpgbt_i2c_ch = fromVFEnumToMap(VFE_num)
 
    offset_wr_slpgbt, offset_rd_slpgbt = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)

    _i2c_master_set_slave_address_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, VFE_device_add)
    for i in range(VFE_reg_width):
        WriteSlaveLpgbt(
            myBcp,
            slpgbt_device_add,
            lpgbtConstants.I2CM0DATA0.address + offset_wr_slpgbt + i,
            (VFE_reg_add >> (8 * i)) & 0xFF
        )
    
    _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.W_MULTI_4BYTE0)
    _i2c_master_set_nbytes_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, VFE_reg_width)
    _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_MULTI)
    _i2c_master_await_completion_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch)

    _i2c_master_set_nbytes_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, nb_bytes)
    _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.READ_MULTI)
    _i2c_master_await_completion_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch)

    byte_list = []
    for i in range(0, nb_bytes):
        result_address = lpgbtConstants.I2CM0READ15.address + offset_rd_slpgbt - i
        read_value = ReadSlaveLpgbt(myBcp, slpgbt_device_add, result_address)
        if nb_bytes == 1:
            return read_value
        byte_list.append(read_value)
    return byte_list

def WriteVFEDeviceWithRetry(myBcp, VFE_num, VFE_device_add, VFE_reg_add, VFE_reg_width, data, viaSca=viaSca):
  if viaSca:
    master_num = 0
    if VFE_num == 1:
      master_num = 3 
    elif VFE_num == 2:
      master_num = 1
    elif VFE_num == 3:
      master_num = 5
    elif VFE_num == 4:
      master_num = 2
    elif VFE_num == 5:
      master_num = 7
    writeMultiScaI2C(myBcp, master_num, VFE_device_add, VFE_reg_add, data)
  else:
    WriteVFEDeviceViaLpgbtWithRetry(myBcp, VFE_num, VFE_device_add, VFE_reg_add, VFE_reg_width, data)


def WriteVFEDeviceViaLpgbtWithRetry(
    myBcp,
    VFE_num,
    VFE_device_add,
    VFE_reg_add,
    VFE_reg_width,
    data
):
    counter = 0
    while True:
        try:
            WriteVFEDeviceViaLpgbt(
                myBcp,
                VFE_num,
                VFE_device_add,
                VFE_reg_add,
                VFE_reg_width,
                data
            )
            return
        except Exception as inst:
            if counter == 10:
                print(inst)
                return
            counter += 1

def WriteVFEDeviceViaLpgbt(
    myBcp,
    VFE_num,
    VFE_device_add,
    VFE_reg_add,
    VFE_reg_width,
    data
):

    slpgbt_device_add, slpgbt_i2c_ch = fromVFEnumToMap(VFE_num)

    offset_wr_slpgbt, _ = _i2c_master_get_reg_offsets(slpgbt_i2c_ch)
    address_and_data = []
    for i in range(VFE_reg_width):
        address_and_data.append((VFE_reg_add >> (8 * i)) & 0xFF)
    try:
        data_list = list(data)
    except TypeError:
        data_list = list((data,))
    address_and_data.extend(data_list)
    assert len(address_and_data) in range(16), "Unsupported I2C write length"

    _i2c_master_set_nbytes_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, len(address_and_data))
   
    for i, data_byte in enumerate(address_and_data):
        
        WriteSlaveLpgbt(
            myBcp,
            slpgbt_device_add,
            lpgbtConstants.I2CM0DATA0.address + offset_wr_slpgbt + (i % 4),
            data_byte
        )
        if i % 4 == 3 or i == len(address_and_data) - 1:
            _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.W_MULTI_4BYTE0 + (i // 4))   

    _i2c_master_set_slave_address_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, VFE_device_add)
    _i2c_master_issue_command_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch, LpgbtEnumsV1.I2cmCommand.WRITE_MULTI)
    _i2c_master_await_completion_in_slpgbt(myBcp, slpgbt_device_add, slpgbt_i2c_ch)


def fast_reset_VFE(bcpObj):
    for i in range(1,6):
        #reg_name = "Fast_command_" + str(i)
        #reg_name = "fcmd_ctrl"+str(i-1)+"30_"
        reg_name = "fcmd_ctrl"+str(i-1)+"_"
        myreg = getattr(bcpObj.BCP.BCP_Lpgbt, reg_name)
        #myreg.set(0)
        #myreg.set(0x2)
        #myreg.set(0)
        #myreg.set(0x3)
        #myreg.set(0)
        #myreg.set(0x4)
        #myreg.set(0)
        #myreg.set(0x8)
        #myreg.set(0)
        #myreg.set(0xa)
        #myreg.set(0)
        myreg.set(0xa8432)

        print("VFE {}: Fast reset sequence 2 > 3 > 4 > 8 > a".format(str(i)))

    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(1)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)


def fast_cmd_VFE(bcpObj, min_=1, max_=6, mode=0):
    for i in range(min_,max_):
        reg_name = "Fast_command_" + str(i)
        #reg_name = "fcmd_ctrl"+str(i-1)+"30_"
        reg_name = "fcmd_ctrl"+str(i-1)+"_"
        myreg = getattr(bcpObj.BCP.BCP_Lpgbt, reg_name)
        myreg.set(0)
        myreg.set(mode)
        print(f"VFE {str(i)}: Fast cmd {mode}")

    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(1)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)


def fast_cmd_single_VFE(bcpObj, mode=0, vfe_num=1):
    '''
    Function to send fast cmd to a single VFE.
    Created for debug puposes.
    '''
    reg_name = "Fast_command_" + str(vfe_num)
    #reg_name = "fcmd_ctrl"+str(vfe_num-1)+"30_"
    reg_name = "fcmd_ctrl"+str(vfe_num-1)+"_"
    myreg = getattr(bcpObj.BCP.BCP_Lpgbt, reg_name)
    myreg.set(0)
    myreg.set(mode)
    print(f"VFE {str(vfe_num)}: Fast cmd {mode}")

    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(1)
    bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
 

def enableSyncMode(bcpObj, enable):
    if enable:
      fast_cmd_VFE(bcpObj, mode=5) # Sync mode
      bcpObj.SwRx.syncMode = True
      bcpObj.SwRx.outputType = 2
    else:
      fast_cmd_VFE(bcpObj, mode=6) # Normal Mode
      bcpObj.SwRx.syncMode = False
 
#def startADCCalibration(myBcp):
#    print("Check again this function startADCCalibration... Probably the code is not correct")
#    return
#    for i in range(1,6):
#        #reg_name = "Fast_command_" + str(i)
#        reg_name = "fcmd_ctrl"+str(i-1)+"30"
#        myreg = getattr(myBcp, reg_name)
#        myreg.set(0)
#        myreg.set(0xd)
#        myreg.set(0)
#    print("ADC Calibration fast command sent")

def enableTestMode(bcpObj, enable=True):
    slpgbt_device_add = 115
    old_value = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address)
    new_value = old_value
    
    if enable:
        new_value = old_value | 0x08
        #Setting test mode also in SwRx for checking channel alignment with pattern 0x3zzz9zzz
        bcpObj.SwRx.TestMode = True
        bcpObj.SwRx.outputType = 2
    else:
        new_value = old_value & 0xF7
        bcpObj.SwRx.TestMode = False
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, new_value)

def remove_all_shifts(myBcp):
    #register_string = "Bit_slip_"
    register_string = 'up_bitslip_conf_sync'
    for cnt in range(0,25):
        print('poke {} {}'.format(hex(0x60121100+cnt*4), 0))
        #register_string_num = register_string + str(cnt)
        register_string_num = register_string + str(cnt)+'40_uplink'+str(cnt)
        myreg = getattr(myBcp.BCP.BCP_Lpgbt,register_string_num)
        myreg.set(0)

def get_uplink_alignment(myBcp):
      
    config = getConfig()
    mlpgbt_device_add = config["mlpgbt_add"]
    slpgbt_device_add_list = config["slpgbts_add"]
 
    channels_map = {}
    channels_map[5] = {"lock_add": 0x158, "lock": 0, "phase_add": 0x159, "phase": 0, "group": 2, "lpgbt": 113}
    channels_map[6] = {"lock_add": 0x15b, "lock": 0, "phase_add": 0x15c, "phase": 0, "group": 3, "lpgbt": 113}
    channels_map[7] = {"lock_add": 0x15e, "lock": 0, "phase_add": 0x15f, "phase": 0, "group": 4, "lpgbt": 113}
    channels_map[10] = {"lock_add": 0x164, "lock": 0, "phase_add": 0x165, "phase": 0, "group": 6, "lpgbt": 113}

    channels_map[0] = {"lock_add": 0x15b, "lock": 0, "phase_add": 0x15c, "phase": 0, "group": 3, "lpgbt": 114}
    channels_map[1] = {"lock_add": 0x15e, "lock": 0, "phase_add": 0x15f, "phase": 0, "group": 4, "lpgbt": 114}
    channels_map[2] = {"lock_add": 0x158, "lock": 0, "phase_add": 0x159, "phase": 0, "group": 2, "lpgbt": 114}
    channels_map[3] = {"lock_add": 0x155, "lock": 0, "phase_add": 0x156, "phase": 0, "group": 1, "lpgbt": 114}
    channels_map[4] = {"lock_add": 0x152, "lock": 0, "phase_add": 0x153, "phase": 0, "group": 0, "lpgbt": 114}
    channels_map[8] = {"lock_add": 0x164, "lock": 0, "phase_add": 0x165, "phase": 0, "group": 6, "lpgbt": 114}
    channels_map[9] = {"lock_add": 0x161, "lock": 0, "phase_add": 0x162, "phase": 0, "group": 5, "lpgbt": 114}

    channels_map[18] = {"lock_add": 0x15b, "lock": 0, "phase_add": 0x15c, "phase": 0, "group": 3, "lpgbt": 115}
    channels_map[19] = {"lock_add": 0x15e, "lock": 0, "phase_add": 0x15f, "phase": 0, "group": 4, "lpgbt": 115}
    channels_map[11] = {"lock_add": 0x158, "lock": 0, "phase_add": 0x159, "phase": 0, "group": 2, "lpgbt": 115}
    channels_map[17] = {"lock_add": 0x155, "lock": 0, "phase_add": 0x156, "phase": 0, "group": 1, "lpgbt": 115}
    channels_map[22] = {"lock_add": 0x152, "lock": 0, "phase_add": 0x153, "phase": 0, "group": 0, "lpgbt": 115}
    channels_map[21] = {"lock_add": 0x164, "lock": 0, "phase_add": 0x165, "phase": 0, "group": 6, "lpgbt": 115}
    channels_map[20] = {"lock_add": 0x161, "lock": 0, "phase_add": 0x162, "phase": 0, "group": 5, "lpgbt": 115}

    channels_map[13] = {"lock_add": 0x15b, "lock": 0, "phase_add": 0x15c, "phase": 0, "group": 3, "lpgbt": 116}
    channels_map[16] = {"lock_add": 0x15e, "lock": 0, "phase_add": 0x15f, "phase": 0, "group": 4, "lpgbt": 116}
    channels_map[14] = {"lock_add": 0x158, "lock": 0, "phase_add": 0x159, "phase": 0, "group": 2, "lpgbt": 116}
    channels_map[15] = {"lock_add": 0x155, "lock": 0, "phase_add": 0x156, "phase": 0, "group": 1, "lpgbt": 116}
    channels_map[24] = {"lock_add": 0x152, "lock": 0, "phase_add": 0x153, "phase": 0, "group": 0, "lpgbt": 116}
    channels_map[23] = {"lock_add": 0x164, "lock": 0, "phase_add": 0x165, "phase": 0, "group": 6, "lpgbt": 116}
    channels_map[12] = {"lock_add": 0x161, "lock": 0, "phase_add": 0x162, "phase": 0, "group": 5, "lpgbt": 116}


    
    for key in channels_map:
      if channels_map[key]["lpgbt"] == 113:
        value_master = ReadMasterLpgbt(myBcp, mlpgbt_device_add, channels_map[key]["lock_add"], 1)
        channels_map[key]["lock"] = (value_master >> 4) & 0x1
        value_master_phase = ReadMasterLpgbt(myBcp, mlpgbt_device_add, channels_map[key]["phase_add"], 1)
        channels_map[key]["phase"] = (value_master_phase) & 0xf
      else:
        value_slave = ReadSlaveLpgbt(myBcp, channels_map[key]["lpgbt"], channels_map[key]["lock_add"], 1)
        channels_map[key]["lock"] = (value_slave >> 4) & 0x1
        value_slave_phase = ReadSlaveLpgbt(myBcp, channels_map[key]["lpgbt"], channels_map[key]["phase_add"], 1)
        channels_map[key]["phase"] = (value_slave_phase) & 0xf 
        
    
    return channels_map
    
 
 
def get_lpgbts_status(myBcp, hr=False, mlpgbt_user_add=None, slpgbt_user_add_list=None):

    config = getConfig()
    mlpgbt_device_add = mlpgbt_user_add
    slpgbt_device_add_list = slpgbt_user_add_list
    if mlpgbt_device_add == None:
      mlpgbt_device_add = config["mlpgbt_add"]
    if slpgbt_device_add_list == None:
      slpgbt_device_add_list = config["slpgbts_add"]

    value_list = {}
    value_master = ReadMasterLpgbt(myBcp, mlpgbt_device_add, 0x1d9, 1)
    if hr:
      value_list[mlpgbt_device_add] = getLpgbtStateString(value_master)
    else:
      value_list[mlpgbt_device_add] = value_master
    for slpgbt_device_add in slpgbt_device_add_list:
      value_slave = ReadSlaveLpgbt(myBcp, slpgbt_device_add, 0x1d9, 1)
      if hr:
        value_list[slpgbt_device_add] = getLpgbtStateString(value_slave)
      else:
        value_list[slpgbt_device_add] = value_slave
    return(value_list)

def get_tclink_status(myBcp, hr=False, master_only=False):

  max_range = 2 if master_only else 5

  readable_map = {
    8: "Unlock",
    9: "Unlock",
    10: "Unlock",
    15: "Lock"
  }

  status_list = []
  for cnt in range(1,max_range):
    #reg_name = "Status_tclink_" + str(cnt)
    #reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
    #status = reg.get()
    lpgbt = "master" if cnt==1 else "slave"+str(cnt-1)

    reg_name = "rx_frame_locked_"+lpgbt
    reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
    status1 = reg.get()

    reg_name = "rx_data_not_idle_"+lpgbt
    reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
    status2 = reg.get()

    reg_name = "rx_user_data_ready_"+lpgbt
    reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
    status3 = reg.get()

    reg_name = "tx_user_data_ready_"+lpgbt
    reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
    status4 = reg.get()
    print([str(status1),str(status2),str(status3),str(status4)])    
    status = int("".join([str(status1),str(status2),str(status3),str(status4)]),2)

    if hr:
      if status in readable_map:
        status_list.append(readable_map[status])
      else:
        status_list.append('Unknown')
    else:
      status_list.append(status)
  return status_list

def reset_tclinks(myBcp, master_only=False):
    
    max_range = 2 if master_only else 5

    status_list = get_tclink_status(myBcp, hr=False, master_only=master_only)

    print("Status list: ", status_list)   


    print("\nReset TClinks\n") 
    for cnt in range(1, max_range):
        lpgbt = "master" if cnt==1 else "slave"+str(cnt-1)
        #reg_name = "Reset_tclink_" + str(cnt)
        reg_name = "mgt_reset_rx_datapath_"+lpgbt
        reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
        reg.set(1)

        reg_name = "mgt_reset_all_"+lpgbt
        reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
        reg.set(0)

        reg_name = "mgt_reset_tx_pll_and_datapath_"+lpgbt
        reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
        reg.set(0)

        reg_name = "mgt_reset_tx_datapath_"+lpgbt
        reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
        reg.set(0)

    time.sleep(1)

    for cnt in range(1, max_range):
        lpgbt = "master" if cnt==1 else "slave"+str(cnt-1)
        #reg_name = "Reset_tclink_" + str(cnt)
        reg_name = "mgt_reset_rx_datapath_"+lpgbt
        reg = getattr(myBcp.BCP.BCP_Lpgbt, reg_name)
        reg.set(0)
    
    time.sleep(2)
    
    status_list = get_tclink_status(myBcp, hr=False, master_only=master_only)
    print("Status list: ", status_list)

    if all([status == 0x0000000F for status in status_list]):
      return True
     

    return False 

def reset_slave_lpgbts(myBcp, mlpgbt_device_add=113):
    print('Resetting the slave LpGBTs setting to low the RSTB pin')
    print('To do that we need to set to low the GPIO0 of the master lpGBT')

    gpiodirl = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIODIRL.address)
    print("Reading if GPIOs are input or output:", gpiodirl)
    gpiooutl = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address)
    print("Reading output values of GPIOs:", gpiooutl)
    
    print("Setting GPIO0 as output and with value low for 0.01 sec")
    gpiodirl_new = gpiodirl | 0x1
    gpiooutl_new_high = gpiooutl | 0x1
    gpiooutl_new_low  = gpiooutl & 0xFE
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIODIRL.address, gpiodirl_new)
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiooutl_new_high)
    time.sleep(1)
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiooutl_new_low)
    time.sleep(0.01)
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiooutl_new_high)
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIODIRL.address, gpiodirl)
    print("... and back to original value")

def reset_sca_by_gpio(myBcp, mlpgbt_device_add=113):
  print('Resetting the sca setting to low the GPIO 2 of Master LpGBT')
  val = input("This function only works for FE v2.3. Do you want to launch it? [y/any other key]")
  if val != "y":
    return
  print("Reading if GPIOs are input or output:")
  gpiodirh = ReadLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIODIRH.address)
  gpiodirl = ReadLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIODIRL.address)
  print(hex(gpiodirh<<8 | gpiodirl)) 

  if (gpiodirh<<8 | gpiodirl) != 0x5:
    print("Wrong configuration for the Master LpGBT")
    return
  gpiol_new = ReadLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address)
  gpiol_up = gpiol_new | 0x4
  print(f"Setting first 8 GPIOs output to {gpiol_up}")
  WriteLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiol_up)
  time.sleep(0.1)
  gpiol_down = gpiol_up & 0xb
  print(f"Setting first 8 GPIOs output to {gpiol_down}")
  WriteLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiol_down)
  time.sleep(0.1)
  print(f"Setting first 8 GPIOs output to {gpiol_up}")
  WriteLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiol_up)
  


def slpgbt_config_done(myBcp, slpgbt_device_add, pll_config_done=True, dll_config_done=True):
    reg_val = 0
    if pll_config_done:
        reg_val |= lpgbtConstants.POWERUP2.PLLCONFIGDONE.bit_mask
    if dll_config_done:
        reg_val |= lpgbtConstants.POWERUP2.DLLCONFIGDONE.bit_mask

    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.POWERUP2.address, reg_val)

def mlpgbt_config_done(myBcp, mlpgbt_device_add, pll_config_done=True, dll_config_done=True):
    reg_val = 0
    if pll_config_done:
        reg_val |= lpgbtConstants.POWERUP2.PLLCONFIGDONE.bit_mask
    if dll_config_done:
        reg_val |= lpgbtConstants.POWERUP2.DLLCONFIGDONE.bit_mask
    WriteMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.POWERUP2.address, reg_val)
#    readvalue = ReadMasterLpgbt(myBcp, mlpgbt_device_add, lpgbtConstants.POWERUP2.address)
#    print(f"Wrote {reg_val}; Read {readvalue}")
    

def find_slave_lpgbts(myBcp):
    register_address = 0x00
    slave_lpgbt_list = []
    print("Scan over master lpGBT to find the slave lpGBTs")
    for slave_device in range(1,128):
        try:
            ReadSlaveLpgbt(myBcp, slave_device, register_address)
            print("    Slave lpGBTD found by master lpGBT: ", hex(slave_device), '-->', slave_device)
            slave_lpgbt_list.append(slave_device)
        except: # No device found at this address, simply skip
            pass

    return slave_lpgbt_list

def align_channels(bcpObj):
    print("Shift list", bcpObj.swRx.shiftList)
    if len(bcpObj.swRx.shiftList) != 25:
        print("Shift list contains {} instead of 25 vaues... something is wrong. Stopping the function.".format(len(bcpObj.swRx.shiftList)))
        return
    #register_string = "Bit_slip_"
    register_string = 'up_bitslip_conf_sync'
    for cnt, shift in enumerate(bcpObj.swRx.shiftList):
        if shift >= int(0):
            print('poke {} {}'.format(hex(0x60121100+cnt*4), shift))
            #register_string_num = register_string + str(cnt)
            register_string_num = register_string + str(cnt)+'40_uplink'+str(cnt)
            myreg = getattr(bcpObj.BCP.BCP_Lpgbt,register_string_num)
            myreg.set(shift)


def configure_mlpgbt_from_file(myBcp, mlpgbt_device_add, file_path):
    print("Write configuration from file {}".format(file_path))
    with open(file_path, 'r') as f:
        for line in f:
            l = line.strip()
            if l.startswith("#"):
               continue
            ll = l.split()
            if len(ll) >= 2:
                addr = int(ll[0], 0)
                val = int(ll[1], 0)
                WriteMasterLpgbt(myBcp, mlpgbt_device_add, addr, val)

def configure_slpgbt_from_file(myBcp, slpgbt_device_add, file_path):
    print("Write configuration from file {}".format(file_path))
    with open(file_path, 'r') as f: 
        for line in f:
            l = line.strip()
            if l.startswith("#"):
               continue
            ll = l.split()
            if len(ll) >= 2:
                addr = int(ll[0], 0)
                val = int(ll[1], 0)
                WriteSlaveLpgbt(myBcp, slpgbt_device_add, addr, val)

def set_special_slpgbt_registers(myBcp):
    val = input("This function only works for FE v3.0 because of different GPIO config wrt FEv2.3. Do you want to launch it? [y/any other key] ")
    if val != "y":
      return

    # GPIO direction Slave1: 
    # GPIO0,1,2,7 set as output --> Enable/disable LVR regulators
    WriteSlaveLpgbt(myBcp, 114, 0x053, 0x00)
    WriteSlaveLpgbt(myBcp, 114, 0x054, 0x87)

    # GPIO direction Slave2:
    # GPIO11 set as output --> Enable/disable Test Mode
    WriteSlaveLpgbt(myBcp, 115, 0x053, 0x08)
    WriteSlaveLpgbt(myBcp, 115, 0x054, 0x00)

    #TODO: set output pins as high/low

def gpio_reset_VFE(myBcp, slpgbt_device_add):
    val = input("This function only works for FE v2.3. Do you want to launch it? [y/any other key]")
    if val != "y":
      return
    print("Reading if GPIOs are input or output:")
    gpiodirh = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRH.address)
    gpiodirl = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRL.address)
    print(hex(gpiodirh<<8 | gpiodirl))

    gpiodirh_new = gpiodirh | 0xFF
    gpiodirl_new = gpiodirl | 0xFF
    print("Setting all GPIO as output")
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRL.address, gpiodirl_new)
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRH.address, gpiodirh_new)

    gpiol_new = 0xFF
    gpioh_new = 0x00
    print("Setting GPIO output to 0x00FF")
    print(hex(gpioh_new<<8 | gpiol_new))
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, gpioh_new)
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiol_new)

    print("Reading GPIO values in slave lpgbt:")
    gpioh_new = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address)
    gpiol_new = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTL.address)
    print(hex(gpioh_new<<8 | gpiol_new))

    print("Setting GPIO output to 0x0000")
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, 0x00)
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTL.address, 0x00)
    time.sleep(0.01)

    print("Setting GPIO output to 0x00FF")
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, gpioh_new)
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTL.address, gpiol_new)

def gpio_test_pulse(myBcp, slpgbt_device_add):
    print("Reading if GPIOs are input or output:")
    gpiodirh = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRH.address)
    gpiodirl = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIODIRL.address)
    print(hex(gpiodirh<<8 | gpiodirl))
    
    if (gpiodirh<<8 | gpiodirl) != 0xFFFF:
        print("GPIOs are not output, leaving the function...")
        return
    
    print("Reading GPIO values in slave lpgbt:")
    gpioh_new = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address)
    gpiol_new = ReadSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTL.address)
    print(hex(gpioh_new<<8 | gpiol_new))
    
    print("Setting GPIO_12 to 0")
    gpioh_new = gpioh_new & 0xEF
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, gpioh_new)
    print(hex(gpioh_new))
    print("Setting GPIO_12 to 1")
    gpioh_new = gpioh_new | 0x10
    print(hex(gpioh_new))
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, gpioh_new)
    #time.sleep(5)
    print("Setting GPIO_12 back to 0")
    gpioh_new = gpioh_new & 0xEF
    print(hex(gpioh_new))
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, lpgbtConstants.PIOOUTH.address, gpioh_new)


def setPLLphase(myBcp, vfe_num, vfe_ch, pll_conf):

    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
    if type(pll_conf) == list:
      pll_conf = pll_conf[(vfe_num-1)*5+(vfe_ch-1)]
    pll_conf_reg16 = pll_conf & 0xFF
    pll_conf_reg15 = (pll_conf >> 8) & 0x1

    #bulk_data = ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x09, 1, nb_bytes=8)
    print('Warning DTU register hardcoded !!!')
    ## [0x0, 0x3c, 0x88, 0x44, 0x55, 0x55, 0x00, 0x00] => reg 0x09 -> 0x10
    bulk_data = [0,60,136,68,85,85,0,0]
    bulk_data[-2] = (bulk_data[-2]&0xFE) | pll_conf_reg15
    bulk_data[-1] = pll_conf_reg16
    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x09, 1, bulk_data) 

def genericAllDTUs(myBcp, myfunc, which='1,2,3,4,5', **kwargs):
    which = [int(i) for i in which.split(',')]
    for vfe_num in which:
        for dtu_num in range(1,6):
            myfunc(myBcp, vfe_num, dtu_num, **kwargs)

def setPedestalsG1(myBcp, vfe_num, vfe_ch, ped_G1):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x06, 1, ped_G1&0xFF)

def setPedestalsG10(myBcp, vfe_num, vfe_ch, ped_G10):
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x05, 1, ped_G10&0xFF)

def togglePLLForce(myBcp, vfe_num, vfe_ch, enable=False):
    
    vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
  
    reg17_val = 0x34 # Auto PLL Marc value
    if enable:
        reg17_val = 0x3c # Manual PLL Marc value

    bulk_data = ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x09, 1, nb_bytes=9)
    print(bulk_data)
    bulk_data[0x11-0x09] = reg17_val

    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x09, 1, bulk_data)
    #bulk_data = ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x09, 1, nb_bytes=9)
    #print(bulk_data)

def setManualPhaseRXChannels(myBcp, value):

    slpgbt_list = []
    slpgbt_list = find_slave_lpgbts(myBcp)
    for slpgbt_device_add in slpgbt_list:
      for reg in [0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce]:
        WriteSlaveLpgbt(myBcp, slpgbt_device_add, reg, 0x1C)
      for reg in [0xd0, 0xd4, 0xd8, 0xdc, 0xe0, 0xe4, 0xe8]:
        WriteSlaveLpgbt(myBcp, slpgbt_device_add, reg, value)

def readLpgbtTemp(myBcp, lpgbt_add, measures=1):
    result = readLpgbtADC(myBcp, lpgbt_add, 14, positive=True, gain=0, measures=measures)
    return round((result - 486.2)/2.105, 1)

def readAllLpgbtTemp(myBcp,lpgbt_add_list):
    result = {}
    for device in lpgbt_add_list:
        value = readLpgbtTemp(myBcp,device)
        result[device] = value
    return result

def readADCwithCurrent(myBcp, lpgbt_add, adc_num, i_value=0x50):
    # enable current DAC (access to this register affects also voltage DAC)
    WriteLpgbt(myBcp, lpgbt_add, 0x06a, 0x40)

    # set current value: current = value of 0x06c*xx in uA.
    WriteLpgbt(myBcp, lpgbt_add, 0x06c, i_value)

    # enable current source for pin adc_num
    WriteLpgbt(myBcp, lpgbt_add, 0x06d, 1 << adc_num)

    VTRx_value = readLpgbtADC(myBcp, lpgbt_add, adc_num, positive = 1, gain = 0)

    # disable DAC current
    WriteLpgbt(myBcp, 113, 0x06a, 0x00)

    return VTRx_value

def readLpgbtADC(myBcp, lpgbt_add, adc_num, positive=True, gain=None, measures=1, ground = False):
  gain = 0 if gain is None else gain + 0x4
  #assert adc_num in range(0,8), "Invalid ADC number. Accepted from 0 to 7"
  if gain: assert gain-0x4 in range(0,4), "Invalid GAIN. Accepted from 0 to 3, 0: x2, 1: x8, 2: x16, 3: x32"

  #value = ReadLpgbt(myBcp, lpgbt_add, 0x121, 1)
  # Enable reference voltage
  vref_reg = ReadLpgbt(myBcp, lpgbt_add, 0x01c, 1)
  vref_reg = (1 << 7) | vref_reg 
  WriteLpgbt(myBcp, lpgbt_add, 0x01c, vref_reg) #OK
 
  # Enable ADC core and set gain 
  WriteLpgbt(myBcp, lpgbt_add, 0x123, 0x00+gain) #OK

  # To read external input on one of the ADCs we need to put negative input to Vref/2 
  # It means to write 0x0F in the first 4 bits of 0x121
  adc_mon = ReadLpgbt(myBcp, lpgbt_add, 0x122, 1)
  if ground:
    #disable VDD to set it to ground
    WriteLpgbt(myBcp, lpgbt_add, 0x122, adc_mon&0b101111)
    if positive:
      WriteLpgbt(myBcp, lpgbt_add, 0x121, (12<<4)+15) #OK
    else:
      WriteLpgbt(myBcp, lpgbt_add, 0x121, 12+(15<<4)) #OK
  else:
    if positive:
      WriteLpgbt(myBcp, lpgbt_add, 0x121, (adc_num<<4)+15) #OK
    else:
      WriteLpgbt(myBcp, lpgbt_add, 0x121, adc_num+(15<<4)) #OK
  
  
  # Wait for reference voltage stabilization
  time.sleep(0.01)
  

  newval = 0
  for cnt in range(measures):
    # start ADC convertion
    WriteLpgbt(myBcp, lpgbt_add, 0x123, 0x80+gain)
    #for vfechannel in range(1,6):
    #  catia_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1)
    #  value = 0x08
    #  WriteVFEDeviceWithRetry(myBcp, 5, catia_add, 0x01, 1, value) 
    
    value = [0,0]
    startTime = time.time()
    # we look for bit DONE in reg [0x1ca] ADCStatusH 
    while not ( value[0] & 0x40 ):
      value = ReadLpgbt(myBcp, lpgbt_add, 0x1ca, 2)
      if time.time() - startTime > 5:
        print("WARNING: ADC readout closed with timeout")
        break
    
    Vref_H = value[0]
    Vref_L = value[1]
    newval = newval + (Vref_L | ((Vref_H&0x03) << 8))
    #print(f"Vref_L: {hex(Vref_L)}, Vref_H: {hex(Vref_H&0x03)} newval {hex(Vref_L | ((Vref_H&0x03) << 8))}")
     
  # stop ADC conversion ## not clear
  WriteLpgbt(myBcp, lpgbt_add, 0x123, gain)
  # enable VDD again
  WriteLpgbt(myBcp, lpgbt_add, 0x122, adc_mon|(1<<4))
  return int(newval/measures)

def configure_mlpgbt_ADC(myBcp, lpgbt_add):
    if (lpgbt_add == 113):
        # select input signal for the converter
        value = ReadMasterLpgbt(myBcp, lpgbt_add, 0x121, 1)
        value = 0x3F
        #print(value)
        WriteMasterLpgbt(myBcp, lpgbt_add, 0x121, value)

        # enable ADC
        #WriteMasterLpgbt(myBcp, lpgbt_add, 0x123, 0x04)

        # enable voltage reference
        WriteMasterLpgbt(myBcp, lpgbt_add, 0x01c, 0x80)


        time.sleep(0.1)
    else:
      print("ADC for slave lpgbt not implemented yet")

def read_mlpgbt_ADC(myBcp, lpgbt_add):
    if (lpgbt_add == 113):
        # start ADC conversion
        WriteMasterLpgbt(myBcp, lpgbt_add, 0x123, 0x84)
        time.sleep(0.1)

        Vref_H = ReadMasterLpgbt(myBcp, lpgbt_add, 0x1ca, 1)
        Vref_L = ReadMasterLpgbt(myBcp, lpgbt_add, 0x1cb, 1)
        newval = Vref_L | ((Vref_H&0x03) << 8)
        print(f"Vref_L: {hex(Vref_L)}, Vref_H: {hex(Vref_H&0x03)} newval {hex(newval)}") 
        # stop ADC conversion
        WriteMasterLpgbt(myBcp, lpgbt_add, 0x123, 0x04)
        
    else:
      print("ADC for slave lpgbt not implemented yet")

def sendAllCATIA_temp(myBcp, on = 1):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]

    which = [int(i) for i in active_VFEs.split(',')]
    for vfe_num in which:
        for catia_num in range(1,6):
            send_CATIA_temp(myBcp, vfe_num, catia_num, on)

def send_CATIA_temp(myBcp, vfenumber, vfechannel, on = 1):

    # Temperature sensor to output ON
    # Reg1 bit3: 0 --> Vref_reg, 1 --> Temperature

    for catia_num in range(1,6):

      vfe_device_add = VFE_first_add + 3 + VFE_ch_step*(catia_num-1)

      ## Put all CATIAs in High-Z state (reg1->0x02, reg5->0x4000)

      WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x02)

      WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x40)
      WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x00)
   
    ## Enable temperature sensor for selected channel (reg1->0x0a) 

    vfe_device_add = VFE_first_add + 3 + VFE_ch_step*(vfechannel-1)
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x0a) 



   # WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x02) # Temperature
   # if on:
   #     WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x18) # Temperature
   #     
   #     # Bit control for the output to the TSENSOR pad to OFF
   #     WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x0F) #0x7F
   #     WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0xFF)
   #     #WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x06, 1, 0x0F-8) 
   # else:
   #     WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x00)
   #     WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x06, 1, 0x0F-8) 

def sendAllCATIA_Vref(myBcp, which='1,2,3,4,5', ref_v = 0):
    which = [int(i) for i in which.split(',')]
    for vfe_num in which:
        for catia_num in range(1,6):
            send_CATIA_Vref(myBcp, vfe_num, catia_num, ref_v)

def send_CATIA_Vref(myBcp, vfenumber, vfechannel, ref_v = 0):

    vfe_device_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1)

    # Temperature sensor to output OFF
    # Reg1 bit3: 0 --> Vref_reg, 1 --> Temperature
    
#    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x08) # Temperature
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x01, 1, 0x00) # Vref_reg
    
    # Bit control for the output to the TSENSOR pad to OFF
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0x3F)
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x05, 1, 0xFF)
    
    # Enable the power on of the regulator, set Vref
    WriteVFEDeviceWithRetry(myBcp, vfenumber, vfe_device_add, 0x06, 1, 0x0F+ref_v) # 3: 1.126, 6: 0.989

def setHGDAC(myBcp, vfenumber, vfechannel, value):
  catia_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1)

  value = value&0x3f
  value = value << 2
  value = value + 3
  WriteVFEDeviceWithRetry(myBcp, vfenumber, catia_add, 0x03, 1, 0x10)
  WriteVFEDeviceWithRetry(myBcp, vfenumber, catia_add, 0x03, 1, value)

def setLGDAC(myBcp, vfenumber, vfechannel, value, hg_dacval=-999):
  catia_add = VFE_first_add+3+VFE_ch_step*(vfechannel-1) 

  value = value&0x3f
  value = value << 8
  if hg_dacval == -999:
    value = value + 3
  else:
    value = value + (hg_dacval << 2) + 3
  WriteVFEDeviceWithRetry(myBcp, vfenumber, catia_add, 0x03, 1, value >> 8)
  WriteVFEDeviceWithRetry(myBcp, vfenumber, catia_add, 0x03, 1, value & 0xFF)

def simple_align_BCP(myBcp):
        print("Zeroing bit shifts")
        remove_all_shifts(myBcp)

        print("Enable sync mode")
        enableSyncMode(myBcp, True)

        print("Acquire data")
        acquireData(myBcp)
        #print("Acquire data")
        #acquireData(myBcp)
        #print("Acquire data")
        #acquireData(myBcp)
        #print("Acquire data")
        #acquireData(myBcp)

        print("Align channels")
        align_channels(myBcp)
        bit_shift = myBcp.swRx.shiftList
        
        print("Acquire data")
        acquireData(myBcp)

        print("Disable sync mode")
        enableSyncMode(myBcp, False)

        return bit_shift

def align_BCP_with_sync_mode(myBcp):
  conf = getConfig()
  active_VFEs = conf["active_VFEs"]

  #fast_cmd_VFE(myBcp, mode=6) # Normal mode

  print("Slpgbt 115 reset DTUs by GPIO")
  WriteSlaveLpgbt(myBcp, 115, 0x055, 0x00)
  time.sleep(1)
  WriteSlaveLpgbt(myBcp, 115, 0x055, 0x10)
  time.sleep(1)        

  fast_cmd_VFE(myBcp, mode=6) # Normal mode

  fast_reset_VFE(myBcp)

  configureAllDTUs(myBcp)

  print("After a reset CATIA are not in active mode but they are sending the reference voltage")
  print("So we are setting CATIA in normal mode now")
  enableCATIAactiveMode(myBcp,  active_VFEs)
  
  print("Enabling PLL Force")
  genericAllDTUs(myBcp, togglePLLForce, active_VFEs, enable=True)
  print(f"Setting PLL {conf['pll_conf']}")
  genericAllDTUs(myBcp, setPLLphase, active_VFEs, pll_conf=conf["pll_conf"])

  print("Reinit Initial LpGBT phase")
  ReinitLpgbtTrain(myBcp)

  bit_shift = simple_align_BCP(myBcp)

  return bit_shift

def optimize_pedestals_bisec(myBcp, lowGain=False, hg_dacval_map = {}, ped_lo = 20, ped_hi = 40):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]

    vfe_list = [int(x) for x in active_VFEs.split(",")]
    #vfe_list = [2]
    good_channels = {}
    mean_map = {}

    dac_val_lg = {}
    dac_val_hg = {}

    #Reset CATIA register 0x04 to default 0xFFFF
    for vfe_num in vfe_list:
      for ch in range(1,6):
        catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)
        WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, 0xFFFF >> 8)
        WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, 0xFFFF & 0xFF)


    if lowGain:
      genericAllDTUs(myBcp, forceLowGainDTU, which=active_VFEs)

    min_range = 0x10 # default val for GAIN 10
    max_range = 0x3F
    if lowGain:
      min_range = 0x10 # default val for GAIN 1 is 0x10 but start to far
      max_range = 0x3F

    nan_counter = [0 for x in range(25)]
   
    dac_vals = [[ round((min_range+max_range)/2) for ch in range(1,6)] for vfe in vfe_list]
    dac_vals_old = [[ -1 for ch in range(1,6)] for vfe in vfe_list]
    dac_vals_lower = [[ min_range for ch in range(1,6)] for vfe in vfe_list]
    dac_vals_upper = [[ max_range for ch in range(1,6)] for vfe in vfe_list]

    ntrial = 0

    while True:
      ntrial+=1
      if (len(good_channels) == len(vfe_list)*5) or (ntrial > round((max_range-min_range)/2)): #or (dac_vals == dac_vals_old) 
        break

      for vfe_idx, vfe in enumerate(vfe_list):
        for ch in range(1,6):
          TTch = (vfe-1)*5 + ch
          if TTch not in good_channels:
            if lowGain:
              hg_dacval = -999
              if TTch in hg_dacval_map:
                hg_dacval = hg_dacval_map[TTch]
              setLGDAC(myBcp, vfe, ch, dac_vals[vfe_idx][ch-1], hg_dacval=hg_dacval)
              dac_val_lg[TTch] = dac_vals[vfe_idx][ch-1]
              dac_val_hg[TTch] = hg_dacval
            else:
              setHGDAC(myBcp, vfe, ch, dac_vals[vfe_idx][ch-1])

      filename = f"pedestal.dat"
      open(filename, 'w').close()
      myBcp.dataWriter.DataFile.set(filename)
      myBcp.dataWriter.Open()

      for index in range(10):
        print(f"Acquisition {index+1}/10")
        acquireData(myBcp)
      print("######### SONO QUI#####")
      myBcp.dataWriter.Close()

      TTeventList = getListFromFile(filename)
      print(len(TTeventList))

      df = pd.DataFrame(TTeventList)

      for vfe_idx, vfe in enumerate(vfe_list):
          for ch in range(1,6):
              TTch = (vfe-1)*5 + ch
              if TTch in good_channels: continue
              all_vals_df = df[df.vfe.eq(vfe-1) & df.ch.eq(ch-1)]
              # find the mean value of the gaussian
              all_vals_df.fillna(0, inplace=True)
              mean_val = all_vals_df['value'].mean()
              if math.isnan(mean_val):
                print(f"Mean in nan for VFE {vfe} channel {ch}!")
                nan_counter[TTch-1] = nan_counter[TTch-1] + 1
              if nan_counter[TTch-1] == 3:
                mean_val = 0

              print(f"Mean value for VFE{vfe} channel{ch}: {mean_val:.2f}")
              dac_vals_old = copy.deepcopy(dac_vals)
              if mean_val > ped_hi:
                dac_vals_lower[vfe_idx][ch-1] = dac_vals[vfe_idx][ch-1]
                dac_vals[vfe_idx][ch-1] = round((dac_vals[vfe_idx][ch-1] + dac_vals_upper[vfe_idx][ch-1])/2)
              elif mean_val < ped_lo:
                dac_vals_upper[vfe_idx][ch-1] = dac_vals[vfe_idx][ch-1]
                dac_vals[vfe_idx][ch-1] = round((dac_vals[vfe_idx][ch-1] + dac_vals_lower[vfe_idx][ch-1])/2)
              else:
                good_channels[TTch] = dac_vals[vfe_idx][ch-1]
                mean_map[TTch] = round(mean_val)
    #no_zero_mean_list = list(filter(lambda a: a != 0, list(mean_map.values())))
    print("mean_map",mean_map)
    #print("no_zero_mean_list",no_zero_mean_list)
    #print("min no_zero_mean_list", min(no_zero_mean_list))
    diff_map ={TTch: min(mean_map[TTch]-15, 0xFF) for TTch in mean_map}
    print("diff_map", diff_map)
    for vfe in vfe_list:
        for ch in range(1,6):
          TTch = (vfe-1)*5 + ch
          if not TTch in diff_map: continue
          if diff_map[TTch] < 0:
            print("Value with zero", TTch, diff_map[TTch])
            continue
          if lowGain:
            setPedestalsG1(myBcp, vfe, ch, diff_map[TTch])
          else:
            setPedestalsG10(myBcp, vfe, ch, diff_map[TTch])

            # Correcting the saturation point of the HG = 4095 - digital subtraction val
            dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
            data = (4095-diff_map[TTch]) & 0xFFF
            #data = (4095-0xff) & 0xFFF
            WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x8, 1, data >> 8)
            WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x7, 1, data & 0xFF)

    if lowGain:
      genericAllDTUs(myBcp, setFreeGainDTU, which=active_VFEs)

    print(good_channels)
    if len(dac_val_lg) == 25 and len(dac_val_hg) == 25:
      dac_val_lg_string = "["+", ".join([str(dac_val_lg[i]) for i in range(1,26)])+"]"
      dac_val_hg_string = "["+", ".join([str(dac_val_hg[i]) for i in range(1,26)])+"]"
      print("\n\n================ Dac val lg and hg ===================")
      print('"dac_val_g1":', dac_val_lg_string)
      print('"dac_val_g10":', dac_val_hg_string)

    return mean_map, good_channels


def optimize_pedestals(myBcp, lowGain=False, hg_dacval_map = {}):
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    vref = conf["CATIA_vref"]

    vfe_list = [int(x) for x in active_VFEs.split(",")]
    good_channels = {}
    mean_map = {}

    dac_val_lg = {}
    dac_val_hg = {}

    if lowGain:
      genericAllDTUs(myBcp, forceLowGainDTU, which=active_VFEs)

    min_range = 0x25 # default val for GAIN 10
    if lowGain:
      min_range = 0x25 # default val for GAIN 1 is 0x10 but start to far
    nan_counter = [0 for x in range(25)]
    for dac_val in range(min_range,0x3F):

      if len(good_channels) == len(vfe_list)*5:
        break

      for vfe in vfe_list:
        for ch in range(1,6):
          TTch = (vfe-1)*5 + ch
          if TTch not in good_channels:
            if lowGain:
              hg_dacval = -999
              if TTch in hg_dacval_map:
                hg_dacval = hg_dacval_map[TTch]
              setLGDAC(myBcp, vfe, ch, dac_val, hg_dacval=hg_dacval)
              dac_val_lg[TTch] = dac_val
              dac_val_hg[TTch] = hg_dacval
            else:
              setHGDAC(myBcp, vfe, ch, dac_val)
     
      filename = f"pedestal.dat"
      open(filename, 'w').close()
      myBcp.dataWriter.DataFile.set(filename)
      myBcp.dataWriter.Open()

      for index in range(10):
        print(f"Acquisition {index+1}/10")
        acquireData(myBcp)
      print("######### SONO QUI#####")
      myBcp.dataWriter.Close()

      TTeventList = getListFromFile(filename)
      print(len(TTeventList))

      df = pd.DataFrame(TTeventList)

      for vfe in vfe_list:
          for ch in range(1,6):
              TTch = (vfe-1)*5 + ch
              if TTch in good_channels: continue
              all_vals_df = df[df.vfe.eq(vfe-1) & df.ch.eq(ch-1)]
              # find the mean value of the gaussian
              all_vals_df.fillna(0, inplace=True)
              mean_val = all_vals_df['value'].mean()
              if math.isnan(mean_val):
                print(f"Mean in nan for VFE {vfe} channel {ch}!")
                nan_counter[TTch-1] = nan_counter[TTch-1] + 1
              if nan_counter[TTch-1] == 3:
                mean_val = 0

              print(f"Mean value for VFE{vfe} channel{ch}: {mean_val:.2f}")
              if mean_val <= 30:
                good_channels[TTch] = dac_val
                mean_map[TTch] = int(mean_val)

    #no_zero_mean_list = list(filter(lambda a: a != 0, list(mean_map.values())))
    print("mean_map",mean_map)
    #print("no_zero_mean_list",no_zero_mean_list)
    #print("min no_zero_mean_list", min(no_zero_mean_list))
    diff_map ={TTch: min(mean_map[TTch]-15, 0xFF) for TTch in mean_map}
    print("diff_map", diff_map)
    for vfe in vfe_list:
        for ch in range(1,6):
          TTch = (vfe-1)*5 + ch
          if not TTch in diff_map: continue
          if diff_map[TTch] < 0:
            print("Value with zero", TTch,diff_map[TTch])
            continue
          if lowGain:
            setPedestalsG1(myBcp, vfe, ch, diff_map[TTch])
          else:
            setPedestalsG10(myBcp, vfe, ch, diff_map[TTch])

            # Correcting the saturation point of the HG = 4095 - digital subtraction val
            dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
            data = (4095-diff_map[TTch]) & 0xFFF
            WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x8, 1, data >> 8)
            WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x7, 1, data & 0xFF)

    if lowGain:
      genericAllDTUs(myBcp, setFreeGainDTU, which=active_VFEs)

    print(good_channels)
    if len(dac_val_lg) == 25 and len(dac_val_hg) == 25:
      dac_val_lg_string = "["+", ".join([str(dac_val_lg[i]) for i in range(1,26)])+"]"
      dac_val_hg_string = "["+", ".join([str(dac_val_hg[i]) for i in range(1,26)])+"]"
      print("\n\n================ Dac val lg and hg==================")
      print('"dac_val_g1":', dac_val_lg_string)
      print('"dac_val_g10":', dac_val_hg_string)

    return mean_map, good_channels

def extract_pedestal_config(myBcp):
  conf = getConfig()
  active_VFEs = conf["active_VFEs"]
  vfe_list = [int(x) for x in active_VFEs.split(",")]
  pedG1_map = {}
  pedG10_map = {}
  HGsaturation_map = {}
  for vfe in vfe_list:
    for ch in range(1,6):
      TTch = (vfe-1)*5 + ch
      dtu_device_add = VFE_first_add+2+VFE_ch_step*(ch-1)
      val = ReadVFEDevice(myBcp, vfe, dtu_device_add, 0x05, 1, nb_bytes=4)
      pedG1_map[TTch] = val[1]
      pedG10_map[TTch] = val[0]
      HGsaturation_map[TTch] = ((val[3] & 0xF) << 8) + val[2]
  
  pedG1_string = "["+", ".join([str(pedG1_map[i]) for i in range(1,26)])+"]"
  pedG10_string = "["+", ".join([str(pedG10_map[i]) for i in range(1,26)])+"]"
  HGsaturation_string = "["+", ".join([str(HGsaturation_map[i]) for i in range(1,26)])+"]"
  print("dig_ped_g1: ", pedG1_string)
  print("dig_ped_g10: ", pedG10_string)
  print("HG_saturation: ", HGsaturation_string)     
 
# In acquire_continuous:
# - duration is in seconds if selfTrigger=False (you need external triggers)
# - duration is number of acquisitions if selfTrigger=True (eg when you don't have ext trigger)
def acquire_continuous(myBcp, duration=100, filename="continuous.dat", selfTrigger=False):
    myBcp.BCP.BCP_Readout.ClrBusy()

    open(filename, 'w').close()
    myBcp.dataWriter.DataFile.set(filename)
    myBcp.dataWriter.Open()

    # DTU flush (the DTU output FIFO is flushed)
    #fast_cmd_VFE(myBcp, mode=7)
    # DTU normal mode
    fast_cmd_VFE(myBcp, mode=6)

    #  print(f"Acquisition {bx+1}/100")
    #  acquireData(myBcp)

    if not selfTrigger:
      ## external trigger needeed !!!
      print("Enable continuous mode")
      myBcp.App.AppTx.ContinuousMode.set(True)
      time.sleep(duration)
      print("Disable continuous mode")
      myBcp.App.AppTx.ContinuousMode.set(False)
    else:
      for i in range(duration):
        acquireData(myBcp)    

    myBcp.dataWriter.Close()


# def fuses_read_bank(myBcp, address, timeout=0.01):
#     """Read fuses bank"""
#     if address%4 != 0:
#         raise Exception("Incorrect address for burn bank!")
#     _, _, address_high, address_low = self.u32_to_bytes(address)
#     #self.write_regs(Lpgbt.FUSEBLOWADDH, address_high)
#     WriteMasterLpgbt(myBcp, address, lpgbtConstants.FUSEBLOWADDH.address, address_high)
#     #self.write_regs(Lpgbt.FUSEBLOWADDL, address_low)
#     WriteMasterLpgbt(myBcp, address, lpgbtConstants.FUSEBLOWADDL.address, address_low)

#     #self.write_regs(Lpgbt.FUSECONTROL, Lpgbt.FUSECONTROL_FUSEREAD_bm)
#     WriteMasterLpgbt(myBcp, address, lpgbtConstants.FUSECONTROL.address, lpgbtConstants.FUSECONTROL_FUSEREAD_bm)

#     timeout_time = time.time() + timeout

#     #status = self.read_regs(Lpgbt.FUSESTATUS)
#     status = ReadMasterLpgbt(myBcp, address, lpgbtConstants.FUSESTATUS.address)

#     if status & Lpgbt.FUSESTATUS_FUSEDATAVALID_bm:
#         #self.write_regs(Lpgbt.FUSECONTROL, 0)
#         WriteMasterLpgbt(myBcp, address, lpgbtConstants.FUSECONTROL.address, 0)

#     # value = self.read_regs(Lpgbt.FUSEVALUESD)<<24
#     value = ReadMasterLpgbt(myBcp, address, lpgbtConstants.FUSEVALUESD.address)<<24
#     # value |= self.read_regs(Lpgbt.FUSEVALUESC)<<16
#     value |= ReadMasterLpgbt(myBcp, address, lpgbtConstants.FUSEVALUESC.address)<<16
#     # value |= self.read_regs(Lpgbt.FUSEVALUESB)<<8
#     value |= ReadMasterLpgbt(myBcp, address, lpgbtConstants.FUSEVALUESB.address)<<8
#     # value |= self.read_regs(Lpgbt.FUSEVALUESA)
#     value |= ReadMasterLpgbt(myBcp, address, lpgbtConstants.FUSEVALUESA.address)

#     return value

def getLpgbtStateString(pusmstate):
    return LpgbtEnumsV1.PusmState(pusmstate).name


def getLpgbtCounterEnable(myBcp, lpgbt_device_add):
    value = ReadLpgbt(myBcp, lpgbt_device_add, 0x12a)
    value = value & 0x7
    if value == 2:
        return True
    return False

def setTestModeLpgbt(myBcp, lpgbt_list=[113,114,115,116], enable=True):

    for lpgbt_device_add in lpgbt_list:
      if lpgbt_device_add == 113:
        setTestModeMlpgbt(myBcp, enable=enable)
      else:
        setTestModeSlpgbt(myBcp, lpgbt_device_add, enable=enable)

def setTestModeMlpgbt(myBcp, enable=False):
  value = 0x00
  if enable:
    value = 0x12
  WriteMasterLpgbt(myBcp, 113, 0x12a, value)

  value = 0x00
  if enable:
    value = 0x2
  WriteMasterLpgbt(myBcp, 113, 0x12b, value)

  value = ReadMasterLpgbt(myBcp, 113, 0x12c)
  value = value & 0xf8
  if enable:
    value = value | 0x2
  else:
    value = value | 0x0
  WriteMasterLpgbt(myBcp, 113, 0x12c, value)

def setTestModeSlpgbt(myBcp, slpgbt_device_add, enable=False):
  for reg in [0x129, 0x12a, 0x12b, 0x12c]:
    value = ReadSlaveLpgbt(myBcp, slpgbt_device_add, reg)
    value = value & 0xc0
    if enable:
      value = value | 0x12
    else:
      value = value | 0x00
    WriteSlaveLpgbt(myBcp, slpgbt_device_add, reg, value)

def acquire_test_pulse(myBcp, latency, amplitude):

  myBcp.BCP.BCP_TCDS.Test_pulse_latency.set(latency)

  conf = getConfig()

  active_VFEs = conf["active_VFEs"]
  vref = conf["CATIA_vref"]

  which = [int(i) for i in active_VFEs.split(',')]
  for vfe_num in which:
    for ch in range(1,6):
        print("Configuring CATIA for test pulses")
        dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
        catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)

        # set the window size of 16 samples
        val_reg1 = ReadVFEDevice(myBcp, vfe_num, dtu_add, 0x01, 1, nb_bytes=1)
        WriteVFEDeviceWithRetry(myBcp, vfe_num, dtu_add, 0x01, 1, (val_reg1&0x3f)|0x80)

        # enable CATIA Vref:  
        WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x06, 1, 0x0F+(vref<<4))

        # set pulse voltage
        amplitude = int(amplitude) & 0xFFF
        data = 0x1000+amplitude
        WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, data >> 8)
        WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, data & 0xFF)

  filename = f"test_pulse_scan_V{amplitude}_D{latency}.dat"
  open(filename, 'w').close()
  myBcp.dataWriter.DataFile.set(filename)
  myBcp.dataWriter.Open()

  print("Clear busy")
  myBcp.BCP.BCP_Readout.ClrBusy()

  print("Arming test pulse")
  myBcp.BCP.BCP_TCDS.Test_pulse_arm.set(1)

  print("Send strobe")
  myBcp.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
  for i in range(100):
    myBcp.BCP.BCP_TCDS.Fast_cmd_strobe.set(1)
    myBcp.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)

  print("Disarming test pulse")
  myBcp.BCP.BCP_TCDS.Test_pulse_arm.set(0)

  print("Requesting frames")
  myBcp.App.AppTx.SendFrame.set(1)

  myBcp.dataWriter.Close()
  
  #Reset CATIA register 0x04 to default 0xFFFF
  for vfe_num in which:
    for ch in range(1,6):
      catia_add = VFE_first_add+3+VFE_ch_step*(ch-1)
      WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, 0x0000 >> 8)
      WriteVFEDeviceWithRetry(myBcp, vfe_num, catia_add, 0x04, 1, 0x0000 & 0xFF)

def FindVTRx(myBcp):
  register_address = 0x00
  print("Scan over master lpGBT to find the VTRx")
  for vtrx_device in range(1,128):
      try:
          ReadVTRx(myBcp, vtrx_device, register_address)
          print("    VTRx found by master lpGBT: ", hex(vtrx_device), '-->', vtrx_device)
          break
      except Exception as ex: # No device found at this address, simply skip
          pass

  return vtrx_device


def ReadVTRx(myBcp, vtrx_device_add, vtrx_reg_add):
  return ReadSlaveLpgbt(
    myBcp,
    vtrx_device_add,
    vtrx_reg_add,
    nb_bytes=1,
    mlpgbt_i2c_ch=1,
    mlpgbt_device_add=113,
    slpgbt_reg_width=1,
    clk_freq=0,
    timeout=0.1
    )

def WriteVTRx(myBcp, vtrx_device_add, vtrx_reg_add, data):
  return WriteSlaveLpgbt(
        myBcp, 
        vtrx_device_add, 
        vtrx_reg_add, 
        data, 
        mlpgbt_i2c_ch=1, 
        mlpgbt_device_add=113, 
        slpgbt_reg_width=1, 
        clk_freq=0, 
        timeout=0.1
    )

def getVTRxVersion(myBcp, vtrx_device=0x50):
  data = ReadVTRx(myBcp, vtrx_device, 0x15)
  revid = data & 0xF
  chipid = data >> 4
  print("VTRx revid ", revid)
  print("VTRx chipid ", chipid)
  return revid, chipid

def enableVTRxChannels(myBcp, vtrx_device=0x50):
  revid, chipid = getVTRxVersion(myBcp, vtrx_device)
  if revid == 5 and chipid == 1:
    WriteVTRx(myBcp, vtrx_device, 0x0, 0xF) 

def get_gpio_16_bit_reg(myBcp, lpgbt_device_add, first_reg_address):
    reg = ReadLpgbt(myBcp, lpgbt_device_add, first_reg_address, number_of_bytes=2)
    valueh = reg[0]
    valuel = reg[1]
    return valuel | (valueh << 8)

def set_single_gpio(myBcp, devices_selected, gpio_selected):
    if gpio_selected < 8:
        # check if pin is configured as output (1)
        direction = ReadLpgbt(myBcp, devices_selected, 0x054) & 1 << gpio_selected != 0
        if direction:
            old_set = ReadLpgbt(myBcp, devices_selected, 0x056)
            # bit flip
            new_set = old_set ^ (1 << gpio_selected)
            
            #set gpio
            WriteLpgbt(myBcp, devices_selected, 0x056, new_set)
        else:
            print('GPIO is set as input')
            return
    else:
        gpio_selected = gpio_selected - 8
        # check if pin is configured as output (1)
        direction = ReadLpgbt(myBcp, devices_selected, 0x053) & 1 << gpio_selected != 0
        if direction:
            old_set = ReadLpgbt(myBcp, devices_selected, 0x055)
            # bit flip
            new_set = old_set ^ (1 << gpio_selected)
            
            #set gpio
            WriteLpgbt(myBcp, devices_selected, 0x055, new_set)
        else:
            print('GPIO is set as input')
            return
    return

def set_gpio_16_bit_reg(myBcp, lpgbt_device_add, first_reg_address, val_16bit):
    print(first_reg_address, hex(val_16bit))
    valueh = val_16bit >> 8
    valuel = val_16bit & 0xFF
    WriteLpgbt(myBcp, lpgbt_device_add, first_reg_address, [valueh, valuel])    

def configure_default_all_lpgbts(myBcp):

    myBcp.BCP.BCP_TCDS.bc0_disabled.set(0) # 0 = enabled, 1 = disabled

    print("Reset bitshift in BCP")
    remove_all_shifts(myBcp)
  
    print("Configure lpgbt master")
    configure_mlpgbt_from_file(myBcp, 113, directory+'/master_rom_ec.cnf')
    mlpgbt_config_done(myBcp, 113, pll_config_done=1, dll_config_done=1)
    time.sleep(1)
  
    print("Reset master tclink")
    reset_tclinks(myBcp, master_only=True)
    time.sleep(1)
  
    print("Enable VTRx channels if recent version")
    enableVTRxChannels(myBcp, vtrx_device=0x50)
  
    print("Reset slave lpgbts")
    reset_slave_lpgbts(myBcp)
    time.sleep(1)
  
    slpgbt_list = find_slave_lpgbts(myBcp)
  
    for addr in slpgbt_list:
        print(f"Configure slave lpgbt {addr}")
        if addr == 114:
          configure_slpgbt_from_file(myBcp, addr, directory+'/slave_1.cnf')
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x01)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x03)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x07)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x87)
        else:
          configure_slpgbt_from_file(myBcp, addr, directory+'/slave.cnf')
        # GPIO out for slave 2 (115)
        if addr == 115:
          print("Slpgbt 115 setting output GPIOs")
          WriteSlaveLpgbt(myBcp, 115, 0x054, 0x00)
          WriteSlaveLpgbt(myBcp, 115, 0x053, 0x18)
          WriteSlaveLpgbt(myBcp, 115, 0x055, 0x10)
          WriteSlaveLpgbt(myBcp, 115, 0x056, 0x00)

        
        slpgbt_config_done(myBcp, addr)
  
    print("Reset all tclink")
    reset_tclinks(myBcp)
    time.sleep(1)
  
    print("Reset VFEs")
    #gpio_reset_VFE(myBcp, 115)
    fast_reset_VFE(myBcp)


def getGPIO_PLLLock(myBcp, lpgbt_device_adds=[113,114,115,116], repetition=10):

    pll_ordered_list = []
    try:
        pll_map = {}
        gpio_map = fromGPIOListToMap()
        for lpgbt_device_add in lpgbt_device_adds:
            for attempts in range(repetition):
                result = get_gpio_16_bit_reg(myBcp, lpgbt_device_add, lpgbtConstants.PIOINH.address)
                for i in range(16):
                    in_val = (result >> i) & 1
                    if "PLLLOCK" in gpio_map[lpgbt_device_add][i]:
                        if gpio_map[lpgbt_device_add][i] not in pll_map:
                            pll_map[gpio_map[lpgbt_device_add][i]] = in_val
                        else:
                            pll_map[gpio_map[lpgbt_device_add][i]] = in_val & pll_map[gpio_map[lpgbt_device_add][i]]
        # pll_map looks like this:
        # pll_map = {
        #     "4B_PLLLOCK": 0,
        #     "5E_PLLLOCK": 1,
        #     ...
        # }
        
        for vfe in range(1,6):
            for index, ch_val in enumerate(['A','B','C','D','E']):
                name = str(vfe)+str(ch_val)+"_PLLLOCK"
                pll_ordered_list.append(pll_map[name])
    except Exception as ex:
        print(traceback.format_exc())

    
    return pll_ordered_list

def getAllCatiaTemps(myBcp, el):

    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) for i in active_VFEs.split(',')]
    if int(el[2][0]) in which:
        value = ','.join([str(read_CATIA_temp(myBcp, el[0], el[1], int(el[2][0]), ch)) for ch in range(1,6)])
          
        if len(el) == 4:
          el[3] = value
        else:
          el.append(value)

def getAllVTRxTemps(myBcp, el):

    value = read_VTRx_temp(myBcp, el[0], el[1])
    if len(el) == 4:
        el[3] = value
    else:
        el.append(value)

def getAllVFETemps(myBcp, el):

    conf = getConfig()
    active_VFEs = conf["active_VFEs"]

    if el[2][0] in active_VFEs:
        value = read_temp_pt100(myBcp, el[0], el[1])
        if len(el) == 4:
            el[3] = value
        else:
            el.append(value)

def getAllADCValues(myBcp):
    adc_list = getADCList()
    for el in adc_list:
        if "TEMP_CATIA" in el[2]:
            getAllCatiaTemps(myBcp,el)
        elif "TH1 (VTRx)" in el[2]:
            getAllVTRxTemps(myBcp,el)
        elif "TEMP_VFE" in el[2]:
            getAllVFETemps(myBcp,el)

    return adc_list

def getVTRxValue(myBcp):
    adc_list = getADCList()
    for el in adc_list:
        if "TH1 (VTRx)" in el[2]:
            getAllVTRxTemps(myBcp,el)
    if len(adc_list[6]) == 4:
        return adc_list[6][3]
    return 0

def getOnlyADCValues(myBcp):
    adc_values = []
    val = getAllADCValues(myBcp)
    
    for el in val:
        if len(el) == 4:
            adc_values.append(el[3])
        else:
            adc_values.append('-')
    print(adc_values)
    return adc_values    

def adc_to_voltage_conversion(myBcp, lpgbt_add, value_adc):

    gain, offset = calibrate_lpgbt_sigle_ended(myBcp, lpgbt_add)
    #print(gain, offset)

    Vref = (offset - offset*(1-(gain/2)))/(512*gain)
    print(f"Vref is {Vref*2}")
    value_V = Vref*2*(((value_adc - offset*(1-(gain/2)))/(512*gain)))

    return value_V
               
def calibrate_lpgbt_sigle_ended(myBcp, lpgbt_add):

    # measuring the offset by setting both inNegative and inPositive as VrefHalf
    offset = readLpgbtADC(myBcp, lpgbt_add, 15, positive = 1, gain = 0, ground=0, measures=1)

    # measuring the gain throught the VDD (with bit5 grounded)
    adc_measurement = readLpgbtADC(myBcp, lpgbt_add, 12, positive = 1, gain = 0, ground=1, measures=1)
    gain = 2*(1-adc_measurement/offset)

    return gain, offset

# Read temperature resistor on VFE 
def read_temp_pt100(myBcp, lpgbt_add, adc_num, i_value=0x80):

    # read the temperature in [adc_ch]
    temp_adc_ch = readADCwithCurrent(myBcp, lpgbt_add, adc_num, i_value)
    print(temp_adc_ch)       

    # convert the temperature from adc_ch to Volt through the LpGBT-ADC calibration
    temp_V = adc_to_voltage_conversion(myBcp, lpgbt_add, temp_adc_ch)
    #print(temp_V)
    
    # calculate Ibias [0, 0.9mA]. from Vt_to_xI.csv we get I_bias = 1.1mA
    ibias = (i_value/0xFF)*0.000925
    #ibias = 0.00483*i_value+0.0048

    # convert from Volt to Celsius # put 1000 Ohm. is it pt1000?
    temp_C = ((temp_V)/(1000*ibias)-1)/0.003925
    
    return round(temp_C,1)

def read_VTRx_temp(myBcp, lpgbt_add, adc_num, i_value=0x50):
    # read the temperature in [adc_ch]
    temp_adc_ch = readADCwithCurrent(myBcp, lpgbt_add, adc_num, i_value)
    #print(temp_adc_ch)

    #Must use this equation for VTRx
    temp_C = math.e**(((1/temp_adc_ch)+0.007119)/0.003306) 

    return round(temp_C,1)

def read_CATIA_temp(myBcp, lpgbt_add, adc_num, vfe_num=2, cat_num=1):

    conf = getConfig()
    active_VFEs = conf["active_VFEs"]
    
    vfe_device_add = VFE_first_add+3+VFE_ch_step*(cat_num-1)

    #Need to first tell CATIA to send voltage
    send_CATIA_temp(myBcp,vfe_num,cat_num)

    #sendAllCATIA_temp(myBcp)
    #Read the ADC count
    adc_count = readLpgbtADC(myBcp,lpgbt_add,adc_num,positive = False,gain = 1)
    #Apply linear calibration equation found by plotting data
    #val = (adc_count-448.91801)/1.13689
    #print('Setting CATIAs to normal mode')
    #enableCATIAactiveMode(myBcp, active_VFEs)
    #time.sleep(0.5)
    #send_CATIA_temp(myBcp,vfe_num,cat_num,on = 0)

    ## Put the CATIA back to High-Z stat

    WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x01, 1, 0x02)

    return round((adc_count-676.39)/3.01,1)

def decomp_stat(myBcp):
  print("vfe1                s1              s1              s1              s1              s1")
  decomp_dict = {'1':[], '2':[], '3':[], '4':[], '5':[] }
  for ch in range(5): 
    for reg_to_read in decomp_dict:
#        myreg1 = getattr(myBcp.BCP.BCP_Lpgbt, reg_to_read+str(ch))
        decomp_dict[reg_to_read].append(1)
  for decomp_key in decomp_dict:
    print(f"{decomp_key}: " + "    ".join([f"{i}" for i in decomp_dict[decomp_key]] ))
  
def check_ch_errors(myBcp, ch_num=0):
  print("check_ch_errors function, ch_num from 0 to 24")
  
  ch_num += 1

  reg_list = {"ch_locked":0, "bc0_c":0, "wr_data_count_max":0, "packet_counter_min":0, "packet_counter_max":0, "rd_data_count_min":0, "sample_error_counter":0, "frame_error_counter":0, "crc_error_counter":0, "parity_error_counter":0}

  for index, reg_name in enumerate(reg_list):
    reg_full_name = reg_name+"_decomp"+str(ch_num)
    myreg = getattr(myBcp.BCP.BCP_Lpgbt, reg_full_name)
    reg_value = myreg.get() 
    reg_list[reg_name] = reg_value
  return reg_list

def start_error_counters(bcpObj):
  print("Reset counters and start them")
  bcpObj.BCP.BCP_TCDS.fifo_occup.set(7)
  bcpObj.BCP.BCP_TCDS.resync_init.set(1)
  bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(1)
  bcpObj.BCP.BCP_TCDS.Fast_cmd_strobe.set(0)
  bcpObj.BCP.BCP_TCDS.resync_init.set(0)
  time.sleep(2)
  bcpObj.BCP.BCP_Lpgbt.clear_cnt_.set(1)
  bcpObj.BCP.BCP_Lpgbt.clear_cnt_.set(0)


def set_single_gpio(myBcp, devices_selected, gpio_selected):
    if gpio_selected < 8:
        # check if pin is configured as output (1)
        direction = ReadLpgbt(myBcp, devices_selected, 0x054) & 1 << gpio_selected != 0
        if direction:
            old_set = ReadLpgbt(myBcp, devices_selected, 0x056)
            # bit flip
            new_set = old_set ^ (1 << gpio_selected)

            #set gpio
            WriteLpgbt(myBcp, devices_selected, 0x056, new_set)
        else:
            print('GPIO is set as input')
            return
    else:
        gpio_selected = gpio_selected - 8
        # check if pin is configured as output (1)
        direction = ReadLpgbt(myBcp, devices_selected, 0x053) & 1 << gpio_selected != 0
        if direction:
            old_set = ReadLpgbt(myBcp, devices_selected, 0x055)
            # bit flip
            new_set = old_set ^ (1 << gpio_selected)

            #set gpio
            WriteLpgbt(myBcp, devices_selected, 0x055, new_set)
        else:
            print('GPIO is set as input')
            return
    return

def test_full_config(myBcp):
    myBcp.BCP.BCP_TCDS.bc0_disabled.set(0) # 0 = enabled, 1 = disabled

    print("Configure lpgbt master")
    configure_mlpgbt_from_file(myBcp, 113, directory+'/master_rom_ec.cnf')
    mlpgbt_config_done(myBcp, 113, pll_config_done=1, dll_config_done=1)
    time.sleep(0.5)

    print("Reset master tclink")
    reset_tclinks(myBcp, master_only=True)
    time.sleep(0.5)

    print("Enable VTRx channels if recent version")
    enableVTRxChannels(myBcp, vtrx_device=0x50)

    print("Reset slave lpgbts")
    reset_slave_lpgbts(myBcp)
    time.sleep(1)

    slpgbt_list = find_slave_lpgbts(myBcp) 

    for addr in slpgbt_list:
        print(f"Configure slave lpgbt {addr}")
        if addr == 114:
          configure_slpgbt_from_file(myBcp, addr, directory+'/slave_1.cnf')
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x01)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x03)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x07)
          time.sleep(0.5)
          WriteSlaveLpgbt(myBcp, addr, 0x056, 0x87)
        else:
          configure_slpgbt_from_file(myBcp, addr, directory+'/slave.cnf')
        # GPIO out for slave 2 (115)
        if addr == 115:
          print("Slpgbt 115 setting output GPIOs")
          WriteSlaveLpgbt(myBcp, 115, 0x054, 0x00)
          WriteSlaveLpgbt(myBcp, 115, 0x053, 0x18)
          WriteSlaveLpgbt(myBcp, 115, 0x055, 0x10)
          WriteSlaveLpgbt(myBcp, 115, 0x056, 0x00)


        slpgbt_config_done(myBcp, addr)

    print("Reset all tclink")
    reset_tclinks(myBcp)
    time.sleep(1)

    ################################ align BCP ##########################################
    conf = getConfig()
    active_VFEs = conf["active_VFEs"]

    print("Slpgbt 115 reset DTUs by GPIO")
    WriteSlaveLpgbt(myBcp, 115, 0x055, 0x00)
    time.sleep(2)
    WriteSlaveLpgbt(myBcp, 115, 0x055, 0x10)
    time.sleep(1)

    print("Fast reset VFEs")
    fast_cmd_VFE(myBcp, mode=6) # Normal mod
    fast_reset_VFE(myBcp)

    configureAllDTUs(myBcp)

    print("After a reset CATIA are not in active mode but they are sending the reference voltage")
    print("So we are setting CATIA in normal mode now")
    enableCATIAactiveMode(myBcp,  active_VFEs)

    print("Enabling PLL Force")
    genericAllDTUs(myBcp, togglePLLForce, active_VFEs, enable=True)
    print(f"Setting PLL {conf['pll_conf']}")
    genericAllDTUs(myBcp, setPLLphase, active_VFEs, pll_conf=conf["pll_conf"])

    print("Reinit Initial LpGBT phase")
    ReinitLpgbtTrain(myBcp)

    bit_shift = simple_align_BCP(myBcp)

    ############################################## optimize pedestal hg and lg ###################################
    if 1:
      dac_val_g1 = conf["dac_val_g1"]
      dac_val_g10 = conf["dac_val_g10"]
      dig_ped_g1 = conf["dig_ped_g1"]
      dig_ped_g10 = conf["dig_ped_g10"]
      HG_saturation = conf["HG_saturation"]

      which = [int(i) for i in active_VFEs.split(',')]
      for vfe in which:
        for ch in range(1,6): 
          print(f"Set pedestals for VFE {vfe}, ch {ch}")
          TTch = ((vfe-1)*5 + ch) - 1
          setLGDAC(myBcp, vfe, ch, dac_val_g1[TTch], hg_dacval=dac_val_g10[TTch])
          setPedestalsG1(myBcp, vfe, ch, dig_ped_g1[TTch])
          setPedestalsG10(myBcp, vfe, ch, dig_ped_g10[TTch])
        
          dtu_add = VFE_first_add+2+VFE_ch_step*(ch-1)
          WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x8, 1, HG_saturation[TTch] >> 8)
          WriteVFEDeviceWithRetry(myBcp, vfe, dtu_add, 0x7, 1, HG_saturation[TTch] & 0xFF)

    if 1:
      configureAllADCs(myBcp)
