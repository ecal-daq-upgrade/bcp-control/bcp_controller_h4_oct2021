import argparse
from data_functions import *
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import lpgbt_functions as lpgbt_functions

def main(args):

    global ax
    global fig
    global plot1

   # TTeventList = getListFromFile(args.inputfile)
   # TTeventList = getListFromFile("test_pulse_scan1.dat")
   # print("hello")
   # av_list= []
   # np_array_list = []
   # amplitude = 355
    measures = int(args.measures)

   # df = pd.DataFrame(TTeventList)
  
    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]
    which = [int(i) - 1 for i in active_VFEs.split(',')]
    
    for vfe in which:
        fig = plt.figure(1)
        fig.canvas.manager.set_window_title('VFE {}'.format(str(vfe+1)))
        for ch in range(5):
          multi_histo = []
          multi_histo_x = []
          for measure in range(1,measures+1):
            TTeventList = getListFromFile(f"{args.inputfile}{measure}.dat")

            av_list= []
            np_array_list = []

            df = pd.DataFrame(TTeventList)

            single_ch_df = df[df.vfe.eq(vfe) & df.ch.eq(ch)]
            single_ch_df.index = range(len(single_ch_df.index))
            single_ch_hg_df_index = single_ch_df.index[single_ch_df.hg.eq(1)]
            single_ch_lg_df_index = single_ch_df.index[single_ch_df.hg.eq(0)]
 
            single_ch_hg_df = single_ch_df[single_ch_df.hg.eq(1)]
            single_ch_lg_df = single_ch_df[single_ch_df.hg.eq(0)]

            multi_histo.append(single_ch_lg_df['value'].max())
            multi_histo_x.append(ch)

          #ax[ch].scatter(ch, multi_histo)
          #plt.scatter(multi_histo_x,multi_histo)
          average = sum(multi_histo)/len(multi_histo)
          #plt.scatter(ch,average)
          plt.errorbar(ch,average,[[max(multi_histo)-average],[average-min(multi_histo)]], markersize=2, marker='o', capsize=2, elinewidth=1, markeredgewidth=1)

 

#   Showing results

    plt.show()
        
if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="test_pulse_scan")
    #parser.add_argument("-of", "--outputfile", help="Output csv filename path", default="my_archive.csv")
    #parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true') #type=bool, default=False)
    parser.add_argument("-m", "--measures", help="Number of files to go through", default=2)     

    args = parser.parse_args()

    print("TODO https://matplotlib.org/stable/gallery/lines_bars_and_markers/scatter_hist.html")

    main(args) 
