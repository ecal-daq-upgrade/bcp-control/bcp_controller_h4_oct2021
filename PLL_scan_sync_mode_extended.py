import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
from operator import add


def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    lpgbt_map = pd.read_csv("lpgbt_map.csv",index_col=0)
    
    
    try:
      for test_num in range(0,6):
        for phase_val in range(0,16):
          lpgbt_functions.setFixLpgbtPhase(myBcp, phase_val)
          myfile = open(f"pll_scan_phase_{phase_val}_{test_num}.txt", "w")

          #print("Send reset fast command to VFEs")
          #lpgbt_functions.fast_reset_VFE(myBcp)
          #time.sleep(2)
  
          print("Configure all DTUs")
          lpgbt_functions.configureAllDTUs(myBcp)
          time.sleep(1)
  
          print("Set all DTUs to PLL manual")
          lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.togglePLLForce, active_VFEs, enable=True)
  
          time.sleep(0.1)
  
          print("Enable sync mode")
          lpgbt_functions.enableSyncMode(myBcp, True)
  
          pll_conf1_list = [0x0, 0x1, 0x3, 0x7, 0xF, 0x1F, 0x3F]
          pll_conf2_list = range(0,8)
          #pll_conf1_list = [0x7]
          #pll_conf2_list = range(3,7)
   
          for index1, pll_conf1 in enumerate(pll_conf1_list):
              for index2, pll_conf2 in enumerate(pll_conf2_list):
  
                  pll_conf = (pll_conf1<<3)|pll_conf2
                 
                  myfile.write(f"{str(pll_conf).zfill(3)} {format(pll_conf, '#011b')}: ")
                  #print(f'pll_conf1 {pll_conf1}, pll_conf2 {pll_conf2}, pll_conf {pll_conf}')
                  
                  lpgbt_functions.genericAllDTUs(myBcp, lpgbt_functions.setPLLphase, active_VFEs, pll_conf=pll_conf)
                  
                  phase_list = []
                  range_val = 1
                  for attempt in range(range_val):
                    lpgbt_functions.ReinitLpgbtTrain(myBcp)
                    time.sleep(0.5)    
  
                    phase_conf = lpgbt_functions.GetAllCurrentPhase(myBcp)
                    if len(phase_list) == 0:
                      phase_list = [phase_conf[lpgbt_map.loc[i]['lpgbt_addr']][lpgbt_map.loc[i]['group']][0] for i in range(25)]
                    else: 
                      phase_list = list(map(add, phase_list, [phase_conf[lpgbt_map.loc[i]['lpgbt_addr']][lpgbt_map.loc[i]['group']][0] for i in range(25)]))
                  print(phase_list) 
                  #TODO: why divide by range_val?
                  phase_list = [int(x/range_val) for x in phase_list]
                  print(phase_list)
                  print("Zeroing bit shifts")
                  lpgbt_functions.remove_all_shifts(myBcp)
          
                  time.sleep(0.2)
                  print("Acquire data")
                  lpgbt_functions.acquireData(myBcp)
          
                  print("Acquire data. Why do we need to acquire data a second time?")
                  lpgbt_functions.acquireData(myBcp)
          
                  time.sleep(0.2) # Don't go below 0.01 seconds, 0.1 it even safer
                  print("Align channels")
                  lpgbt_functions.align_channels(myBcp)
                  myfile.write(', '.join(str(x).zfill(2) for x in myBcp.swRx.shiftList))                
                  myfile.write("\n")
                  
                  myfile.write('Phase            '+', '.join(str(x).zfill(2) for x in phase_list))
                  myfile.write("\n")
                  
                  myfile.flush()
   
                  print("Acquire data")
                  lpgbt_functions.acquireData(myBcp)
          
                  print("Acquire data")
                  # lpgbt_functions.acquireData(myBcp)
                   
          print("Disable sync mode")
          lpgbt_functions.enableSyncMode(myBcp, False)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
