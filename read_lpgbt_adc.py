import traceback
import sys
import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
from operator import add


def main():

    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)

    conf = lpgbt_functions.getConfig()
    active_VFEs = conf["active_VFEs"]

    counter = 0
    try:
      startTime = time.time()
      print('Setting CATIAs to send temperature voltage to LpGBTs')
      #lpgbt_functions.sendAllCATIA_temp(myBcp,0)      
      #lpgbt_functions.send_CATIA_temp(myBcp,2,1)
      #lpgbt_functions.send_CATIA_temp(myBcp,2,2)
      #lpgbt_functions.send_CATIA_temp(myBcp,2,3)
      #lpgbt_functions.send_CATIA_temp(myBcp,2,4)
      #lpgbt_functions.send_CATIA_temp(myBcp,2,5)
      

      while(True):
         #lpgbt_functions.sendAllCATIA_temp(myBcp)  
         #vfe_temp_measured = lpgbt_functions.adc_to_voltage_conversion(myBcp, 115, 572)      
         #vfe_temp = lpgbt_functions.read_temp_pt100(myBcp, 115, 0, i_value=0xF1)
         #vfe_temp = lpgbt_functions.read_VTRx_temp(myBcp, 113, 6, i_value=0x50)
         #print("VFE_TEMP1 [V]", vfe_temp) #, "VFE_TEMP1 measured [V]", vfe_temp_measured)
         
         #print('Reading LpGBT ADC')
         #val = lpgbt_functions.readLpgbtADC(myBcp,115,15,positive = True,gain = 0) # 5TEMP_CATIA
         #print(lpgbt_functions.adc_to_voltage_conversion(myBcp,115,val)*2)
         
         #adc_conv = lpgbt_functions.adc_to_voltage_conversion(myBcp, 116, val)
         print(lpgbt_functions.read_CATIA_temp(myBcp,113,3,vfe_num=2,cat_num=1))
         
         #val = lpgbt_functions.readLpgbtADC(myBcp,113,3,positive = 0,gain = 1)
         #voltage = lpgbt_functions.adc_to_voltage_conversion(myBcp,113,val)
         #print(val,voltage)

        #catia = lpgbt_functions.getAllCatiaTemps(myBcp)
        #vtrx = lpgbt_functions.getAllVTRxTemps(myBcp) 
        #print(catia[0][3], vtrx[0][3])
                         

             #print(lpgbt_functions.readCatiaTemp(myBcp,113,3))
         #    print(lpgbt_functions.getAllCatiaTemps(myBcp))
             #print(lpgbt_functions.readAllLpgbtTemp(myBcp,[113,114,115,116]))
         #   print('Setting CATIAs to normal mode')
         #   lpgbt_functions.enableCATIAactiveMode(myBcp, active_VFEs)
        
        #print('loop time:', time.time() - startTime)
         time.sleep(2)

    except Exception as ex:
        print(traceback.format_exc())
        # or
        print(sys.exc_info()[2])
    finally:
        myBcp.stop()

if __name__ == "__main__":
   
    main() 
