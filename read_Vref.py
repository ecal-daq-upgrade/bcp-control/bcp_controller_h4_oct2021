import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    mlpgbt_device_add       = 113
    slpgbt_device_add_list  = [114, 115, 116]

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    ref_v = [8, 12, 4, 10, 14, 6, 13, 9, 15, 11, 2, 5, 7, 3] #9=15, 5=7, 
    for v in sorted(ref_v):
      print(v)
      lpgbt_functions.sendAllCATIA_Vref(myBcp, '2', ref_v = (v << 4))
      lpgbt_functions.configure_mlpgbt_ADC(myBcp, mlpgbt_device_add)
      for i in range(5):
        lpgbt_functions.read_mlpgbt_ADC(myBcp, mlpgbt_device_add)

    myBcp.stop()

if __name__ == "__main__":
   
    main() 
