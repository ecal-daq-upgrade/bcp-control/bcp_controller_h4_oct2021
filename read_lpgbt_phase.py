import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    mlpgbt_device_add       = 113
    slpgbt_device_add_list  = [114, 115, 116]
    eprx_control_list       = [0x0c8, 0x0c9, 0x0ca, 0x0cb, 0x0cc, 0x0cd, 0x0ce]

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try:
        value_old = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, eprx_control_list[0], 7)
        print("Values read: ", [hex(x) for x in value_old])

        for key, value in enumerate(value_old):
            if value != 0:
                phase_value = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, 0x153+3*key, 1)
                print(f"Phase value for group {key}: ", hex(phase_value & 0xF))

                phase_value = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, 0x152+3*key, 1)
                print(f"Locked status for group {key}: ", hex(phase_value >> 4))

        slpgbt_device_add_list = lpgbt_functions.find_slave_lpgbts(myBcp)
        print("Slave lpGBTs list:", slpgbt_device_add_list)

        for key, slpgbt_device_add in enumerate(slpgbt_device_add_list):
            print(f"\n Slave {key}")
            value_old = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, eprx_control_list[0], 7)
            print("Values read: ", [hex(x) for x in value_old])

            for key, value in enumerate(value_old):
                if value != 0:
                    phase_value = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, 0x153+3*key, 1)
                    print(f"Phase value for group {key}: ", hex(phase_value & 0xF))

                    phase_value = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, 0x152+3*key, 1)
                    print(f"Locked status for group {key}: ", hex(phase_value >> 4))


    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()


if __name__ == "__main__":
   
    main()
