import sys
import argparse
import time

import lpgbt_functions as lpgbt_functions

if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-rm", "--ResetMode", help="Type of VFE reset: 1 gpio reset, 2 fast command reset", type=int, default=1)

    args = parser.parse_args()
    
    myBcp = lpgbt_functions.bcpConfigureAndStart()
    
    if args.ResetMode == 1:
        print("GPIO reset VFEs")
        lpgbt_functions.gpio_reset_VFE(myBcp, 115)
    if args.ResetMode == 2:
        print("Fast command reset VFEs")
        lpgbt_functions.fast_reset_VFE(myBcp)

    myBcp.stop()
