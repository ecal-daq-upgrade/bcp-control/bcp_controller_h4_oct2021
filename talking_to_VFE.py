import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the VFE devices via LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'


    VFE_first_add = 8
    VFE_ch_step = 8
    vfe_ch = 3

    VFE_num                 = 2
    vfe_device_add          = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
    register_add            = 0x0
    data                    = 0x01 

    myBcp = lpgbt_functions.bcpConfigureAndStart()
   
    try: 
      vfe_num = 2
      for vfe_ch in range(1,6):
        vfe_device_add = VFE_first_add+2+VFE_ch_step*(vfe_ch-1)
        lowgain = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x06, 1)
        highgain = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x05, 1)
        print(f'Reading vfe {vfe_num} ch {vfe_ch}, low gain:', lowgain)
        print(f'Reading vfe {vfe_num} ch {vfe_ch}, high gain:', highgain)
      #  lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x06, 1, 0xFF)#ped_G1&0xFF)
      #  lpgbt_functions.WriteVFEDeviceWithRetry(myBcp, vfe_num, vfe_device_add, 0x05, 1, 0xFF)#ped_G10&0xFF)

      #  lowgain = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x06, 1)
      #  highgain = lpgbt_functions.ReadVFEDevice(myBcp, vfe_num, vfe_device_add, 0x05, 1)
      #  print(f'Reading vfe {vfe_num} ch {vfe_ch}, low gain:', lowgain)
      #  print(f'Reading vfe {vfe_num} ch {vfe_ch}, high gain:', highgain)

    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()


if __name__ == "__main__":
   
    main() 
