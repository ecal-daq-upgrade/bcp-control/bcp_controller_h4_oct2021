import lpgbt_functions as lpgbt_functions

# Script to test the slow communication between the BCP and the LpGBTs on the FE


def main():

    BOLD = '\033[1m'
    END = '\033[0m'

    mlpgbt_device_add       = 113
    slpgbt_device_add_list  = [114, 115, 116]
    register_add            = 0x004
    data                    = [0x11,0x12]

    myBcp = lpgbt_functions.bcpConfigureAndStart()

    try: 
      #print(lpgbt_functions.GetAllCurrentPhase(myBcp))
      print("Checking if we can multi read/write on master lpgbt\n")
      print(f"Reading reg {hex(register_add)} and {hex(register_add+1)}")
      value_old = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, register_add, 2)
      print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")
      print(f"Writing {data} on reg {hex(register_add)} and {hex(register_add+1)}")
      lpgbt_functions.WriteMasterLpgbt(myBcp, mlpgbt_device_add, register_add, data)
      print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
      value_new = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, register_add, 2)
      print(f"{BOLD}Reading reg {register_add} and {hex(register_add+1)}: {value_new}{END}")    
      print(f"Writing original value on reg {hex(register_add)} and and {hex(register_add+1)}")
      lpgbt_functions.WriteMasterLpgbt(myBcp, mlpgbt_device_add, register_add, value_old)
      print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
      value_old = lpgbt_functions.ReadMasterLpgbt(myBcp, mlpgbt_device_add, register_add, 2)
      print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")

      slpgbt_device_add_list = lpgbt_functions.find_slave_lpgbts(myBcp)
      print("Slave lpGBTs list:", slpgbt_device_add_list)

      for key, slpgbt_device_add in enumerate(slpgbt_device_add_list):
          print(f"\n\nChecking if we can multi read/write on slave lpgbt #{key+1}\n")
          print(f"Reading reg {hex(register_add)} and {hex(register_add+1)}")
          value_old = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, register_add, nb_bytes=2)
          print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")
          print(f"Writing {data} on reg {hex(register_add)} and {hex(register_add+1)}")
          lpgbt_functions.WriteSlaveLpgbt(myBcp, slpgbt_device_add, register_add, data) 
          print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
          value_new = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, register_add, nb_bytes=2)
          print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_new}{END}")
          print(f"Writing original value on reg {hex(register_add)} and {hex(register_add+1)}")
          lpgbt_functions.WriteSlaveLpgbt(myBcp, slpgbt_device_add, register_add, value_old)
          print(f"Reading again reg {hex(register_add)} and {hex(register_add+1)}")
          value_old = lpgbt_functions.ReadSlaveLpgbt(myBcp, slpgbt_device_add, register_add, nb_bytes=2)
          print(f"{BOLD}Reading reg {hex(register_add)} and {hex(register_add+1)}: {value_old}{END}")
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()


if __name__ == "__main__":
   
    main() 
