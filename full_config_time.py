import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
import matplotlib.pyplot as plt
import random
def main():

    startTime = time.time()
    myBcp = lpgbt_functions.bcpConfigureAndStart(NumReadoutWords = 10)
    try:
      lpgbt_functions.test_full_config(myBcp)
    except Exception as ex:
        print(ex)
    finally:
        myBcp.stop()

    endTime = time.time()
    print(f"Total time {endTime - startTime}")

if __name__ == "__main__":
   
    main() 
