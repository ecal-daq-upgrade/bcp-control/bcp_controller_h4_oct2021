import time
import lpgbt_functions as lpgbt_functions
import pandas as pd
import matplotlib.pyplot as plt

def main():

  myBcp = lpgbt_functions.openBCPConnection()
  my_data= []
  for i in range(1024):
    my_data.append(0xFFFFFFFF)
  #print(my_data)
  try:
    #print('First read:', myBcp.App.AppMem.DataBlock.get())
    start = time.time()
    for i in range(1):
      myBcp.App.AppMem.DataBlock.set(my_data)
    print(time.time() - start)


    start = time.time()
    for i in range(1):
      myBcp.App.AppMem.DataBlock.get()
    print(time.time() - start)
  except Exception as ex:
    print(ex)
  finally:
    myBcp.stop()

if __name__ == "__main__":
   
    main() 
