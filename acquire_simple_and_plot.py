import sys
import argparse
import time
import pyrogue as pr
import pyrogue.pydm
import rogue.utilities.fileio
import pandas as pd
import numpy as np
import argparse
import matplotlib.pyplot as plt
import os
from read_data_and_plot import *
import lpgbt_functions as lpgbt_functions

def main(args):

    if args.plotresults:
        rec = Receiver()
        fread = pyrogue.utilities.fileio.FileReader(files='continuous.dat', configChan = None)
        #fread = pyrogue.utilities.fileio.FileReader(files=args.inputfile, configChan = None)

        for header, data in fread.records():
            arr = np.array(data, dtype=np.uint8)
            fframe = fakeFrame(header, arr)
            rec.process(fframe)

        av_list = []
        np_array_list = []
        for ch_num, ch_list in enumerate(rec.TTeventList):
            average = 0
            lg_list = ch_list['lg']
            hg_list = ch_list['hg']
            print(f'lg list len: {len(lg_list)}, hg list len {len(hg_list)}')
            if len(lg_list) == 0 and len(hg_list) == 0:
                print('Skipping')
                continue
            for value in lg_list:
                average = average + value
            for value in hg_list:
                average = average + value
            average = average/(len(lg_list)+len(hg_list))
            av_list.append(int(average))
            print('Ch {}, average value {}'.format(ch_num, average))
            np_array_list.append(np.array(lg_list+hg_list))

        for i in range(5):
            fig, ax = plt.subplots(5)
            bins = np.arange(4096+1)
            for j in range(5):
                ch = i*5+j
                print(len(np_array_list))
                x = np.arange(len(np_array_list[ch]))
                print(len(x), len(np_array_list[ch]))
                ax[j].plot(x, np_array_list[ch])
        plt.show()

if __name__ == "__main__":
    
    # Defining option list
    parser = argparse.ArgumentParser(description='Command line options parser')
    parser.add_argument("-nrw", "--NumReadoutWords", help="Number of subframes", type=int, default=63)
    parser.add_argument("-ot", "--OutputType", help="Type FE channels logging. Options 0,1,2", type=int, default=2)
    parser.add_argument("-tm", "--TestMode", help="Look for DTU Test Mode pattern for bit shifting", type=bool, default=False)
    parser.add_argument("-plt", "--plotresults", help="if flag is present, results are plotted", action='store_true')
    parser.add_argument("-if", "--inputfile", help="Input filename path", default="single_event.dat")
    args = parser.parse_args()

    main(args)
